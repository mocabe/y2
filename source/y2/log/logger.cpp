// SPDX-License-Identifier: MIT

#include <y2/log/logger.hpp>

#include <spdlog/spdlog.h>
#include <spdlog/sinks/sink.h>
#include <spdlog/sinks/stdout_sinks.h>

namespace y2::log {

  namespace {

    auto cvt_level(level lvl)
    {
      switch (lvl) {
        case level::info:
          return spdlog::level::info;
        case level::warning:
          return spdlog::level::warn;
        case level::error:
          return spdlog::level::err;
      }
      Y2_UNREACHABLE();
    }
  }

  logger::logger(badge<logger>) {}

  auto logger::get_for_stderr(std::string_view sv) -> std::shared_ptr<logger>
  {
    auto ret = std::make_shared<logger>(badge<logger>());

    if (auto logger = spdlog::get(std::string(sv))) {
      ret->m_logger = logger;
    } else {
      ret->m_logger = spdlog::stdout_logger_mt(std::string(sv));
    }

    ret->m_level = level::info;
    ret->m_logger->set_level(spdlog::level::info);

    return ret;
  }

  void logger::log(level lvl, std::string_view msg)
  {
    assert(m_logger);
    m_logger->log(cvt_level(lvl), msg);
  }

  void logger::set_level(level lvl)
  {
    m_logger->set_level(cvt_level(lvl));
    m_level = lvl;
  }

  void logger::disable() { m_logger->set_level(spdlog::level::off); }
  void logger::enable() { m_logger->set_level(cvt_level(m_level)); }

} // namespace y2::log