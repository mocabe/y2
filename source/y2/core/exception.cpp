// SPDX-License-Identifier: MIT

#include <y2/core/exception.hpp>

#include <fmt/core.h>
#include <fmt/format.h>

namespace y2 {

  namespace {

    ///
    /// \brief Format exception info into readable string.
    ///
    /// This format works as hyperlink in VS Code terminal.
    ///
    auto format_exception_string(const char* str, const std::source_location& loc)
    {
      return fmt::format(
        "{}({},{}): in {}: {}",
        loc.file_name(),
        loc.line(),
        loc.column(),
        loc.function_name(),
        str);
    }

  } // namespace

  ///
  /// \brief Initialize exception.
  ///
  /// \param str Ascii string which represents exception message.
  /// \param loc Source location (provided by default argument).
  ///
  exception::exception(const char* str, std::source_location loc)
    : runtime_error(format_exception_string(str, loc))
    , m_loc {loc}
  {
  }

  ///
  /// \brief Get error message as UTF-8 string.
  ///
  auto exception::u8_what() const -> const char8_t*
  {
    return reinterpret_cast<const char8_t*>(what());
  }

  ///
  /// \brief Get source location.
  ///
  auto exception::source_location() const
  {
    return m_loc;
  }

} // namespace y2