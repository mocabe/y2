// SPDX-License-Identifier: MIT

#include <y2/core/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>

namespace y2 {

  namespace {

    auto to_boost(const uuid& id) -> boost::uuids::uuid
    {
      auto ret = boost::uuids::uuid();
      std::copy_n(id.data().data(), 16, ret.data);
      return ret;
    }

  } // namespace

  auto to_string(const uuid& id) -> std::string
  {
    return boost::uuids::to_string(to_boost(id));
  }

  auto operator<<(std::ostream& os, const uuid& id) -> std::ostream&
  {
    os << to_boost(id);
    return os;
  }

} // namespace y2