// SPDX-License-Identifier: MIT

#include <y2/core/uid.hpp>

#include <fmt/format.h>

namespace y2::uid_detail {

  auto id_to_string(u64 id) -> std::string
  {
    return fmt::format("{:0>16x}", id);
  }

} // namespace y2::uid_detail