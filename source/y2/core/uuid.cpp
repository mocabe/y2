// SPDX-License-Identifier: MIT

#include <y2/core/uuid.hpp>

#include <boost/uuid/uuid_generators.hpp>
#include <algorithm>

#include <boost/uuid/string_generator.hpp>

namespace y2 {

  namespace {

    auto to_boost(const uuid& id) -> boost::uuids::uuid
    {
      auto ret = boost::uuids::uuid();
      std::copy_n(id.data().data(), 16, ret.data);
      return ret;
    }

    auto from_boost(const boost::uuids::uuid& id) -> uuid
    {
      auto ret = uuid::make_zero();
      std::copy_n(id.data, 16, ret.data().data());
      return ret;
    }

  } // namespace

  namespace uuid_detail {

    auto make_from_string_dynamic(const char (&str)[37]) -> std::array<u8, 16>
    {
      auto gen = boost::uuids::string_generator();
      auto id = gen(str);
      std::array<u8, 16> ret;
      std::copy_n(id.data, 16, ret.data());
      return ret;
    }

  } // namespace uuid_detail

  ///
  /// \brief Create zero UUID.
  ///
  uuid uuid::make_zero()
  {
    return uuid();
  }

  ///
  /// \brief Create random UUID.
  ///
  uuid uuid::make_random()
  {
    static auto gen = boost::uuids::random_generator_mt19937();
    return from_boost(gen());
  }

  ///
  /// \brief Create UUID form string.
  ///
  uuid uuid::make_from_string(std::string_view str)
  {
    auto gen = boost::uuids::string_generator();
    return from_boost(gen(str.begin(), str.end()));
  }

  ///
  /// \brief Zero UUID?
  ///
  bool uuid::is_zero() const noexcept
  {
    return to_boost(*this).is_nil();
  }

  ///
  /// \brief Get UUID variant.
  ///
  auto uuid::variant() const noexcept -> uuid_variant
  {
    switch (to_boost(*this).variant()) {
      case boost::uuids::uuid::variant_ncs:
        return uuid_variant::ncs;
      case boost::uuids::uuid::variant_rfc_4122:
        return uuid_variant::rfc_4122;
      case boost::uuids::uuid::variant_microsoft:
        return uuid_variant::microsoft;
      case boost::uuids::uuid::variant_future:
        return uuid_variant::future;
    }
    Y2_UNREACHABLE();
  };

  ///
  /// \brief Get UUID version.
  ///
  auto uuid::version() const noexcept -> uuid_version
  {
    switch (to_boost(*this).version()) {
      case boost::uuids::uuid::version_unknown:
        return uuid_version::unknown;
      case boost::uuids::uuid::version_time_based:
        return uuid_version::time_based;
      case boost::uuids::uuid::version_dce_security:
        return uuid_version::dce_security;
      case boost::uuids::uuid::version_name_based_md5:
        return uuid_version::name_based_md5;
      case boost::uuids::uuid::version_random_number_based:
        return uuid_version::random_number_based;
      case boost::uuids::uuid::version_name_based_sha1:
        return uuid_version::name_based_sha1;
    }
    Y2_UNREACHABLE();
  }
}