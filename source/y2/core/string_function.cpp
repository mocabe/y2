// SPDX-License-Identifier: MIT

#include <y2/core/string_function.hpp>

namespace y2::string_function {

  /// \brief Split string by delimiter.
  ///
  /// \param delim delimiter.
  /// \param max_split maximum split count.
  /// \retval List of split strings (not including delimiter).
  template <class ReturnType>
  static auto split_impl(string_view str, char8_t delim, usize max_split) -> std::vector<ReturnType>
  {
    auto ret = std::vector<ReturnType>();

    auto bgn = str.begin();
    auto it  = bgn;

    auto c = usize(0);
    for (; it != str.end(); ++it) {
      if (*it == delim) {
        if (++c > max_split) {
          break;
        }
        if (bgn != it) {
          ret.emplace_back(bgn, it);
        }
        bgn = it + 1;
      }
    }
    if (c != 0 && bgn != str.end()) {
      ret.emplace_back(bgn, str.end());
    }
    return ret;
  }

  auto split_copy(string_view str, char8_t delim, usize max_split)
    -> std::vector<string>
  {
    return split_impl<string>(str, delim, max_split);
  }

  auto split_view(string_view str, char8_t delim, usize max_split)
    -> std::vector<string_view>
  {
    return split_impl<string_view>(str, delim, max_split);
  }

} // namespace y2::string_function