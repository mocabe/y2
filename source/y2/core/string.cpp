// SPDX-License-Identifier: MIT

#include <y2/core/string.hpp>

namespace y2 {

  namespace {

    auto to_std_view(string_view view) -> std::u8string_view
    {
      return {view.data(), view.length()};
    }

  } // namespace

  auto string::from_utf8_unchecked(std::string_view view) -> string
  {
    const auto ptr = reinterpret_cast<const char8_t*>(view.data());
    return string(ptr, ptr + view.length());
  }

  string::string() noexcept = default;

  string::string(char8_t c)
    : m_str {c}
  {
  }

  string::string(const char8_t* cstr)
    : m_str(cstr)
  {
  }

  string::string(std::u8string_view view)
    : m_str {view}
  {
  }

  string::string(string_view view)
    : m_str {to_std_view(view)}
  {
  }

  string::string(const allocator_type& alloc) noexcept
    : m_str {alloc}
  {
  }

  auto string::begin() const -> const_iterator
  {
    return m_str.data();
  }

  auto string::end() const -> const_iterator
  {
    return m_str.data() + m_str.size();
  }

  auto string::cbegin() const -> const_iterator
  {
    return begin();
  }

  auto string::cend() const -> const_iterator
  {
    return end();
  }

  auto string::length() const noexcept -> size_type
  {
    return m_str.length();
  }

  auto string::capacity() const noexcept -> size_type
  {
    return m_str.capacity();
  }

  bool string::empty() const noexcept
  {
    return m_str.empty();
  }

  auto string::data() const noexcept -> const char8_t*
  {
    return m_str.data();
  }

  auto string::view() const noexcept -> string_view
  {
    return string_view(data(), length());
  }

  auto string::span() const noexcept -> std::span<const char8_t>
  {
    return {m_str.data(), m_str.length()};
  }

  bool string::contains(string_view x) const noexcept //
  {
    return view().contains(x);
  }

  bool string::contains(char8_t x) const noexcept
  {
    return contains(string_view(&x, 1));
  }

  auto string::count(string_view x) const noexcept -> size_type
  {
    return view().count(x);
  }

  auto string::count(char8_t x) const noexcept -> size_type
  {
    return count(string_view(&x, 1));
  }

  bool string::starts_with(string_view x) const noexcept
  {
    return view().starts_with(x);
  }

  bool string::starts_with(char8_t x) const noexcept
  {
    return view().starts_with(string_view(&x, 1));
  }

  bool string::ends_with(string_view x) const noexcept
  {
    return view().ends_with(x);
  }

  bool string::ends_with(char8_t x) const noexcept
  {
    return view().ends_with(string_view(&x, 1));
  }

  void string::replace(string_view x, string_view y)
  {
    auto i1 = size_type(0);
    auto i2 = size_type(0);

    while (i1 < length() && i2 < x.length()) {
      if (m_str[i1++] == x[i2++]) {
        if (i2 == x.length()) {
          m_str.replace(i1 - x.length(), i2, to_std_view(y));
          i1 = i1 - x.length() + y.length();
          i2 = 0;
        }
      } else {
        i2 = 0;
      }
    }
  }

  void string::replace(char8_t x, char8_t y)
  {
    replace(string_view(&x, 1), string_view(&y, 1));
  }

  void string::replace(char8_t c, string_view view)
  {
    replace(string_view(&c, 1), view);
  }

  void string::replace(string_view x, char8_t y)
  {
    replace(x, string_view(&y, 1));
  }

  void string::append(const char8_t* x)
  {
    m_str.append(x);
  }

  void string::append(string_view x)
  {
    m_str.append(to_std_view(x));
  }

  void string::append(const string& x)
  {
    m_str.append(x.m_str);
  }

  void string::append(char8_t x)
  {
    m_str.append(to_std_view(string_view(&x, 1)));
  }

  void string::erase(const_iterator p)
  {
    m_str.erase(m_str.begin() + std::distance(cbegin(), p));
  }

  void string::erase(const_iterator first, const_iterator last)
  {
    const auto f = m_str.begin() + std::distance(cbegin(), first);
    const auto l = m_str.end() + std::distance(cend(), last);
    m_str.erase(f, l);
  }

  void string::resize(size_type length)
  {
    m_str.resize(length);
  }

  void string::reserve(size_type length)
  {
    m_str.reserve(length);
  }

  void string::clear()
  {
    m_str.clear();
  }

  /// Convert string to string_view
  string::operator string_view() const noexcept
  {
    return view();
  }

  auto string::operator[](size_type pos) const -> const_reference
  {
    assert(pos < length());
    return m_str[pos];
  }

  auto string::operator+=(const char8_t* x) -> string&
  {
    append(string_view(x));
    return *this;
  }

  auto string::operator+=(const string& x) -> string&
  {
    append(x);
    return *this;
  }

  auto string::operator+=(string_view x) -> string&
  {
    append(x);
    return *this;
  }

  auto string::operator+=(char8_t x) -> string&
  {
    append(x);
    return *this;
  }

  auto operator+(const string& lhs, const string& rhs) -> string
  {
    auto lhs_ = lhs;
    lhs_.append(rhs);
    return lhs_;
  }

  auto operator+(string&& lhs, const string& rhs) -> string
  {
    auto lhs_ = std::move(lhs);
    lhs_.append(rhs);
    return lhs_;
  }

  auto operator+(const string& lhs, string&& rhs) -> string
  {
    auto rhs_ = std::move(rhs);
    rhs_.insert(rhs_.cbegin(), lhs.cbegin(), lhs.cend());
    return rhs_;
  }

  auto operator+(string&& lhs, string&& rhs) -> string
  {
    auto lhs_ = std::move(lhs);
    auto rhs_ = std::move(rhs);
    lhs_.append(rhs_);
    return lhs_;
  }

} // namespace y2