// SPDX-License-Identifier: MIT

#include <y2/core/uid.hpp>

#include <boost/random/random_device.hpp>
#include <boost/random/mersenne_twister.hpp>

#include <random>

namespace y2::uid_detail {

  auto make_random_id() -> u64
  {
    static_assert(std::is_same_v<uint64_t, std::uint_fast64_t>);
    static boost::random::random_device seed;
    static boost::random::mt19937_64 rng(seed());
    return rng();
  }
}