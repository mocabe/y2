// SPDX-License-Identifier: MIT

#include <y2/ui/mouse_events.hpp>

namespace y2::ui::events
{
  mouse_event::mouse_event(event_phase phase, mouse_button button, vec_px pos)
    : event(phase)
    , m_button {button}
    , m_pos {pos}
  {
  }

  auto mouse_event::pos() const -> vec_px { return m_pos; }

  auto mouse_event::button() const -> mouse_button { return m_button; }

  mouse_click::mouse_click(event_phase phase, mouse_button button, vec_px pos)
    : mouse_event(phase, button, pos)
  {
  }

  mouse_double_click::mouse_double_click(
    event_phase phase,
    mouse_button button,
    vec_px pos)
    : mouse_event(phase, button, pos)
  {
  }
  mouse_press::mouse_press(event_phase phase, mouse_button button, vec_px pos)
    : mouse_event(phase, button, pos)
  {
  }
  mouse_release::mouse_release(
    event_phase phase,
    mouse_button button,
    vec_px pos)
    : mouse_event(phase, button, pos)
  {
  }

  mouse_repeat::mouse_repeat(event_phase phase, mouse_button button, vec_px pos)
    : mouse_event(phase, button, pos)
  {
  }

  mouse_move::mouse_move(event_phase phase, vec_px pos, vec_px delta)
    : mouse_event(phase, mouse_button(), pos)
    , m_delta {delta}
  {
  }

  auto mouse_move::delta() const -> vec_px { return m_delta; }

  mouse_over::mouse_over(event_phase phase, vec_px pos)
    : mouse_event(phase, mouse_button(), pos)
  {
  }

  mouse_out::mouse_out(event_phase phase, vec_px pos)
    : mouse_event(phase, mouse_button(), pos)
  {
  }

  mouse_enter::mouse_enter(event_phase phase, vec_px pos)
    : mouse_event(phase, mouse_button(), pos)
  {
  }

  mouse_leave::mouse_leave(event_phase phase, vec_px pos)
    : mouse_event(phase, mouse_button(), pos)
  {
  }

} // namespace y2::ui::events