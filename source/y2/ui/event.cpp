// SPDX-License-Identifier: MIT

#include <y2/ui/event.hpp>

namespace y2::ui {

  using namespace scoped_enum_operators;

  ///
  /// \brief Construct event object.
  /// \param phase Propagation phases to use for this event.
  ///
  event::event(event_phase phase)
    : m_phase {phase}
    , m_current_phase {event_phase::none}
  {
  }

  ///
  /// \brief Get phase of event.
  ///
  auto event::phase() const -> event_phase
  {
    return m_phase;
  }

  ///
  /// \brief Is event currently in dispatch?
  ///
  bool event::dispatching() const
  {
    return phase() != event_phase::none;
  }

  ///
  /// \brief Get current dispatch phase.
  ///
  auto event::current_phase() const -> event_phase
  {
    return m_current_phase;
  }

  ///
  /// \brief Set current dispatch phase.
  ///
  void event::set_current_phase(event_phase phase)
  {
    assert((~m_phase & phase) == event_phase::none);
    m_current_phase = phase;
  }

  ///
  /// \brief Set phase.
  ///
  void event::set_phase(event_phase p)
  {
    m_phase = p;
  }

} // namespace y2::ui