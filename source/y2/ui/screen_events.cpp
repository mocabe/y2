// SPDX-License-Identifier: MIT

#include <y2/ui/screen_events.hpp>

namespace y2::ui::events {

  screen_event::screen_event()
    : event(event_phase::target)
  {
  }

} // namespace y2::ui::events

namespace y2::ui::screen_events {

  connect::connect(const ui::screen& m)
    : m_screen {m}
  {
  }

  auto connect::screen() const -> ui::screen
  {
    return m_screen;
  }

  disconnect::disconnect(const ui::screen& m)
    : m_screen {m}
  {
  }

  auto disconnect::screen() const -> ui::screen
  {
    return m_screen;
  }

  primary_change::primary_change(const ui::screen& m)
    : m_screen {m}
  {
  }

  auto primary_change::screen() const -> ui::screen
  {
    return m_screen;
  }

} // namespace y2::ui::screen_events