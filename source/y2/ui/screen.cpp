// SPDX-License-Identifier: MIT

#include <y2/ui/screen.hpp>
#include <y2/ui/os/screen.hpp>
#include <y2/ui/os/application.hpp>
#include <y2/ui/os/screen_info.hpp>

#include <y2/core/exception.hpp>
#include <range/v3/view.hpp>

namespace y2::ui {

  namespace {

    class screen_delegate final : public os_screen_delegate
    {
      ui::screen& m_screen;

    public:
      screen_delegate(ui::screen& screen)
        : m_screen {screen}
      {
      }

      void process_event(events::os_screen_update& e) override
      {
        auto& m = m_screen;
        if (auto v = e.geometry()) {
          m.geometry_changed(*v);
        }
        if (auto v = e.work_area()) {
          m.work_area_changed(*v);
        }
        if (auto v = e.content_scale()) {
          m.content_scale_changed(*v);
        }
        if (auto v = e.logical_dpi()) {
          m.logical_dpi_changed(*v);
        }
        if (auto v = e.physical_dpi()) {
          m.physical_dpi_changed(*v);
        }
        if (auto v = e.name()) {
          m.name_changed(*v);
        }
      }

      void process_event(events::os_screen_disconnect&) override
      {
        m_screen.disconnected();
      }
    };

  } // namespace

  void screen::disconnect_signals()
  {
    disconnected          = {};
    geometry_changed      = {};
    work_area_changed     = {};
    content_scale_changed = {};
    logical_dpi_changed   = {};
    physical_dpi_changed  = {};
    name_changed          = {};
  }

  void screen::add_os_delegate()
  {
    assert(!m_delegate);
    m_delegate = make_unique<screen_delegate>(*this);
    m_os->add_delegate(*m_delegate);
  }

  void screen::remove_os()
  {
    if (m_os) {
      m_os->remove_delegate(*m_delegate);
      m_delegate = nullptr;
      m_os       = nullptr;
    }
  }

  ///
  /// Construct screen object from OS handle.
  ///
  /// \param os OS screen handle (non-null).
  ///
  screen::screen(shared<os_screen> os)
    : m_os {std::move(os)}
  {
    if (!m_os || !m_os->valid()) {
      throw exception("Invalid os_screen object");
    }
    add_os_delegate();
  }

  ///
  /// \brief Copy screen object.
  ///
  /// Connections will not be copied.
  ///
  screen::screen(const screen& other)
    : m_os {other.m_os}
  {
    add_os_delegate();
  }

  ///
  /// \brief Move screen object.
  ///
  /// Connections will be moved.
  ///
  screen::screen(screen&& other) noexcept
    : m_os {other.m_os}
    , disconnected {std::move(other.disconnected)}
    , geometry_changed {std::move(other.geometry_changed)}
    , work_area_changed {std::move(other.work_area_changed)}
    , content_scale_changed {std::move(other.content_scale_changed)}
    , logical_dpi_changed {std::move(other.logical_dpi_changed)}
    , physical_dpi_changed {std::move(other.physical_dpi_changed)}
    , name_changed {std::move(other.name_changed)}
  {
    add_os_delegate();
    other.remove_os();
  }

  ///
  /// \brief Copy assign screen object.
  ///
  /// Connections will not be copied.
  ///
  screen& screen::operator=(const screen& other)
  {
    disconnect_signals();
    m_os = other.m_os;
    return *this;
  }

  ///
  /// \brief Move assign screen object.
  ///
  /// Connections will be moved.
  ///
  screen& screen::operator=(screen&& other) noexcept
  {
    disconnect_signals();
    m_os                  = other.m_os;
    disconnected          = std::move(other.disconnected);
    geometry_changed      = std::move(other.geometry_changed);
    work_area_changed     = std::move(other.work_area_changed);
    content_scale_changed = std::move(other.content_scale_changed);
    logical_dpi_changed   = std::move(other.logical_dpi_changed);
    physical_dpi_changed  = std::move(other.physical_dpi_changed);
    name_changed          = std::move(other.name_changed);
    other.remove_os();
    return *this;
  }

  ///
  /// \brief Destruct screen handle.
  ///
  screen::~screen() noexcept
  {
    disconnect_signals();
    remove_os();
  }

  ///
  /// \brief Get primary screen.
  ///
  auto screen::primary() -> screen
  {
    return screen(os_screen_info::context()->primary());
  }

  ///
  /// \brief Enumerate screens.
  ///
  auto screen::enumerate() -> std::vector<screen>
  {
    auto screens = os_screen_info::context()->screens();

    return screens                                                        //
           | ranges::views::transform([](auto&& m) { return screen(m); }) //
           | ranges::to_vector;
  }

  ///
  /// \brief Get default logical DPI.
  ///
  auto screen::default_logical_dpi() -> unit::logical_dpi
  {
    return os_screen_info::context()->default_dpi();
  }

  ///
  /// \brief Check if screen handle is still valid.
  ///
  /// When screen is disconnected, screen handle becomes invalid.
  ///
  bool screen::valid() const
  {
    return m_os && m_os->valid();
  }

  ///
  /// \brief Get geometry of screen.
  ///
  auto screen::geometry() const -> ui::rect_px
  {
    if (m_os) {
      return m_os->geometry();
    }
    throw exception("Use after move");
  }

  ///
  /// \brief Get available area in screen.
  ///
  auto screen::work_area() const -> ui::rect_px
  {
    if (m_os) {
      return m_os->work_area();
    }
    throw exception("Use after move");
  }

  ///
  /// \brief Get display name of screen.
  ///
  auto screen::name() const -> string
  {
    if (m_os) {
      return m_os->name();
    }
    throw exception("Use after move");
  }

  ///
  /// \brief Get display scale of screen.
  ///
  auto screen::content_scale() const -> unit::content_scale
  {
    if (m_os) {
      return m_os->content_scale();
    }
    throw exception("Use after move");
  }

  ///
  /// \brief Get logical DPI of screen.
  ///
  /// Logical DPI = (default logical DPI) * (content scale of screen)
  ///
  auto screen::logical_dpi() const -> unit::logical_dpi
  {
    if (m_os) {
      return m_os->logical_dpi();
    }
    throw exception("Use after move");
  }

  ///
  /// \brief Get physical DPI of screen.
  ///
  /// This can be incorrect.
  ///
  auto screen::physical_dpi() const -> unit::physical_dpi
  {
    if (m_os) {
      return m_os->physical_dpi();
    }
    throw exception("Use after move");
  }

} // namespace y2::ui