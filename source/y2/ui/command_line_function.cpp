// SPDX-License-Identifier: MIT

#include <y2/ui/command_line_function.hpp>

#include <boost/nowide/args.hpp>

namespace y2::ui::command_line_function {

  ///
  /// \brief Parse command line arguments.
  ///
  /// \param argc `argc` argument for main()
  /// \param argv Platform specific `argv` for main()
  ///
  auto parse_args(int argc, char** argv) -> std::vector<string>
  {
    // Replaec args with UTF-8 encoded value until end of this function.
    [[maybe_unused]] auto arg = boost::nowide::args(argc, argv);

    // Build arg list.
    auto ret = std::vector<string>(argc);
    for (size_t i = 0; i < static_cast<size_t>(argc); ++i) {
      ret[i] = reinterpret_cast<const char8_t*>(argv[i]);
    }
    return ret;
  }

} // namespace y2::ui::command_line_function
