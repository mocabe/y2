// SPDX-License-Identifier: MIT

#include <y2/ui/os/event_queue_events.hpp>

namespace y2::ui::events {

  os_event_queue_event::os_event_queue_event()
    : event(event_phase::target)
  {
  }
} // namespace y2::ui::events

namespace y2::ui::os_event_queue_events {

  event_posted::event_posted()
  {
  }

  timer_timeout::timer_timeout(u32 id)
    : m_id {id}
  {
  }

  auto timer_timeout::id() const -> u32
  {
    return m_id;
  }

} // namespace y2::ui::os_event_queue_events
