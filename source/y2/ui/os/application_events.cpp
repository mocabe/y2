// SPDX-License-Identifier: MIT

#include <y2/ui/os/application_events.hpp>
#include <y2/ui/event_receiver.hpp>

namespace y2::ui::events {

  os_application_event::os_application_event()
    : event(event_phase::target)
  {
  }

  os_application_start::os_application_start(string id, std::vector<string> args)
    : m_id {id}
    , m_args {std::move(args)}
  {
  }

  auto os_application_start::id() const -> string
  {
    return m_id;
  }

  auto os_application_start::args() const -> std::vector<string>
  {
    return m_args;
  }

  os_application_exit::os_application_exit(exit_code code)
    : m_code {code}
  {
  }

  auto os_application_exit::code() const -> exit_code
  {
    return m_code;
  }

  os_application_unhandled_exception::os_application_unhandled_exception(std::exception_ptr e)
    : m_exception {std::move(e)}
  {
  }

  auto os_application_unhandled_exception::exception() const -> std::exception_ptr
  {
    return m_exception;
  }

  os_application_post_event::os_application_post_event(ui::event_receiver& r, unique<ui::event> e)
    : m_receiver {r}
    , m_event {std::move(e)}
  {
    assert(m_event);
  }

  auto os_application_post_event::receiver() const -> weak<ui::event_receiver>
  {
    return m_receiver;
  }

  auto os_application_post_event::event() const -> ui::event&
  {
    assert(m_event);
    return *m_event;
  }
}