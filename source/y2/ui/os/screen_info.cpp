// SPDX-License-Identifier: MIT

#include <y2/ui/os/screen_info.hpp>

namespace y2::ui {

  void os_screen_info::set_delegate(os_screen_info_delegate& d)
  {
    m_delegate = &d;
  }

  void os_screen_info::remove_delegate()
  {
    m_delegate = nullptr;
  }

  bool os_screen_info::has_delegate() const
  {
    return m_delegate;
  }

} // namespace y2::ui