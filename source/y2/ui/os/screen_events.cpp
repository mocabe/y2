// SPDX-License-Identifier: MIT

#include <y2/ui/os/screen_events.hpp>

namespace y2::ui::events {

  os_screen_event::os_screen_event()
    : event(event_phase::target)
  {
  }

  os_screen_connect::os_screen_connect(shared<os_screen> m)
    : m_screen {std::move(m)}
  {
  }

  auto os_screen_connect::screen() const -> shared<os_screen>
  {
    return m_screen;
  }

  os_screen_disconnect::os_screen_disconnect(shared<os_screen> m)
    : m_screen {std::move(m)}
  {
  }

  auto os_screen_disconnect::screen() const -> shared<os_screen>
  {
    return m_screen;
  }

  os_primary_screen_change::os_primary_screen_change(shared<os_screen> m)
    : m_screen {std::move(m)}
  {
  }

  auto os_primary_screen_change::screen() const -> shared<os_screen>
  {
    return m_screen;
  }

  os_screen_update::os_screen_update()
  {
  }

  auto os_screen_update::geometry() const -> optional<ui::rect_px>
  {
    return m_geometry;
  }
  auto os_screen_update::work_area() const -> optional<ui::rect_px>
  {
    return m_work_area;
  }
  auto os_screen_update::content_scale() const -> optional<unit::content_scale>
  {
    return m_content_scale;
  }
  auto os_screen_update::logical_dpi() const -> optional<unit::logical_dpi>
  {
    return m_logical_dpi;
  }
  auto os_screen_update::physical_dpi() const -> optional<unit::physical_dpi>
  {
    return m_physical_dpi;
  }
  auto os_screen_update::name() const -> optional<string>
  {
    return m_name;
  }

  void os_screen_update::set_geometry(ui::rect_px geometry)
  {
    m_geometry = geometry;
  }
  void os_screen_update::set_work_area(ui::rect_px work_area)
  {
    m_work_area = work_area;
  }
  void os_screen_update::set_content_scale(unit::content_scale scale)
  {
    m_content_scale = scale;
  }
  void os_screen_update::set_logical_dpi(unit::logical_dpi dpi)
  {
    m_logical_dpi = dpi;
  }
  void os_screen_update::set_physical_dpi(unit::physical_dpi dpi)
  {
    m_physical_dpi = dpi;
  }
  void os_screen_update::set_name(string name)
  {
    m_name = name;
  }

} // namespace y2::ui::events
