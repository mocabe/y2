// SPDX-License-Identifier: MIT

#include <y2/ui/os/screen.hpp>

namespace y2::ui {

  void os_screen::add_delegate(os_screen_delegate& d)
  {
    m_delegates.push_back(&d);
  }

  void os_screen::remove_delegate(os_screen_delegate& d)
  {
    std::erase(m_delegates, &d);
  }

} // namespace y2::ui