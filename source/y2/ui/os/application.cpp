// SPDX-License-Identifier: MIT

#include <y2/ui/os/application.hpp>
#include <y2/ui/event_receiver.hpp>

#include <regex>

namespace y2::ui {

  namespace {

    ///
    /// \brief Validate application ID.
    /// \throw exception on error.
    ///
    /// Application IDs must be reversed domain name format used in various platforms like Android.
    ///
    void validate_application_id(string_view id)
    {
      // TODO: Replace this with better regex library.
      const auto re  = std::regex(R"(^[a-zA-Z]+\w*(\.[a-zA-Z]+\w*)*$)");
      const auto bgn = reinterpret_cast<const char*>(id.data());
      const auto end = bgn + id.length();
      if (!std::regex_match(bgn, end, re)) {
        throw exception("Invalid application ID");
      }
    }

  } // namespace

  struct os_application::params
  {
    params(string id, std::vector<string> args)
      : m_id {id}
      , m_args {std::move(args)}
    {
    }

    auto& id() const
    {
      return m_id;
    }

    auto& args() const
    {
      return m_args;
    }

  private:
    string m_id;
    std::vector<string> m_args;
  };

  ///
  /// \brief Initialize os_application.
  /// \param id Application ID.
  /// \param args Command line arguments (exclude executable name).
  ///
  /// Initializes event queue for current thread.
  ///
  os_application::os_application(string id, std::vector<string> args)
  {
    validate_application_id(id);
    m_params = ui::make_shared<params>(std::move(id), std::move(args));

    os_event_queue::thread_configure();
    m_event_queue = os_event_queue::thread_local_context();
    m_event_queue->set_delegate(*this);
  }

  ///
  /// \brief Destroy application.
  ///
  os_application::~os_application() noexcept
  {
    // Application is exiting before processing all posted events.
    assert(m_post_events.empty());
    m_event_queue->remove_delegate();
  }

  ///
  /// \brief Set delegate for application.
  ///
  void os_application::set_delegate(os_application_delegate& d)
  {
    m_delegate = &d;
  }

  ///
  /// \brief Remove delegate for application.
  ///
  void os_application::remove_delegate()
  {
    m_delegate = nullptr;
  }

  ///
  /// \brief Send event to receiver.
  ///
  bool os_application::send_event(event_receiver& r, ui::event& e)
  {
    return r.send_event(e);
  }

  ///
  /// \brief Post event to receiver.
  ///
  void os_application::post_event(event_receiver& r, unique<ui::event> e)
  {
    if (!e) {
      return;
    }

    m_post_events.push(r, std::move(e));
    m_event_queue->post();
  }

  ///
  /// \brief Process posted event.
  ///
  void os_application::process_event(os_event_queue_events::event_posted&)
  {
    // Get event to send.
    if (auto e = m_post_events.pop()) {
      // Send event if receiver is alive.
      if (auto r = e->receiver().get()) {
        send_event(*r, e->event());
      }
    }
  }

  ///
  /// \brief Flush posted events.
  ///
  void os_application::process_all_posted_events()
  {
    while (!m_post_events.empty()) {
      auto event = os_event_queue_events::event_posted();
      process_event(event);
    }
  }

  ///
  /// \brief Get application ID.
  ///
  auto os_application::id() const -> string
  {
    return m_params->id();
  }

  ///
  /// \brief Get command line arguments
  ///
  auto os_application::args() const -> std::vector<string>
  {
    return m_params->args();
  }

} // namespace y2::ui