// SPDX-License-Identifier: MIT

#include <y2/ui/os/posted_event_queue.hpp>

namespace y2::ui {

  posted_event_queue::posted_event_queue() = default;

  auto posted_event_queue::lock() const -> std::unique_lock<std::mutex>
  {
    return std::unique_lock(m_mtx);
  }

  bool posted_event_queue::empty() const
  {
    auto lck = lock();
    return m_queue.empty();
  }

  void posted_event_queue::push(event_receiver& r, unique<event> e)
  {
    auto post_event = make_unique<event_type>(r, std::move(e));
    {
      auto lck = lock();
      m_queue.push_back(std::move(post_event));
    }
  }

  auto posted_event_queue::pop() -> unique<events::os_application_post_event>
  {
    auto lck = lock();
    if (!m_queue.empty()) {
      auto e = std::move(m_queue.front());
      m_queue.erase(m_queue.begin());
      return e;
    }
    return nullptr;
  }
}