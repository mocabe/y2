// SPDX-License-Identifier: MIT

#include <y2/ui/os/event_queue.hpp>

#include <cassert>
#include <range/v3/algorithm.hpp>

namespace y2::ui {

  void os_event_queue::set_delegate(os_event_queue_delegate& d)
  {
    assert(!m_queue_delegate);
    m_queue_delegate = &d;
  }

  void os_event_queue::remove_delegate()
  {
    m_queue_delegate = nullptr;
  }

  ///
  /// \brief Add new timer delegate, starting timer.
  ///
  void os_event_queue::add_timer_delegate(os_timer_delegate& d)
  {
    remove_timer_delegate(d);

    const auto id = d.id();
    m_timers.emplace(id, &d);
    set_timer(id, d.interval());
  }

  ///
  /// \brief Remove timer delegate, destroy timer currently running.
  ///
  void os_event_queue::remove_timer_delegate(os_timer_delegate& d)
  {
    const auto id = d.id();
    if (m_timers.contains(id)) {
      remove_timer(id);
      m_timers.erase(id);
    }
  }

  ///
  /// \brief Get next timer ID.
  ///
  /// Returned ID is only usable in the same thread.
  ///
  auto os_event_queue::next_timer_id() -> u32
  {
    thread_local auto next = u32(1);

    auto fetch_increment = [&] { return std::exchange(next, next + 1); };

    if (!os_event_queue::thread_has_context()) {
      return fetch_increment();
    }

    auto queue = os_event_queue::thread_local_context();

    while (true) {
      const auto id = fetch_increment();
      if (!queue->m_timers.contains(id)) {
        return id;
      }
    }
  }

  ///
  /// \brief Notify event post to delegate.
  ///
  void os_event_queue::send_posted_event()
  {
    if (m_queue_delegate) {
      auto event = os_event_queue_events::event_posted();
      m_queue_delegate->process_event(event);
    }
  }

  ///
  /// \brief Notify timer expiration to delegate.
  ///
  void os_event_queue::send_timer_expiration(u32 id)
  {
    auto it = m_timers.find(id);

    if (it == m_timers.end()) {
      return;
    }

    auto d = it->second;

    m_timers.erase(id);

    auto event = os_event_queue_events::timer_timeout(id);
    d->process_event(event);
  }

} // namespace y2::ui
