// SPDX-License-Identifier: MIT

#include <y2/ui/trackable.hpp>

namespace y2::ui {

  ///
  /// \brief Add connection to connection list.
  ///
  void trackable::add_connection(connection c)
  {
    m_connections.push_back(std::move(c));
  }

  ///
  /// \brief Disconnect all connnections.
  ///
  /// This function can be used in destructor to ensure all connections are disconnected before
  /// destructing members.
  ///
  void trackable::disconnect_all() noexcept
  {
    m_connections.clear();
  }

  ///
  /// \brief Get weak.
  ///
  auto trackable::track() const -> weak<const trackable>
  {
    return weak(std::enable_shared_from_this<trackable>::shared_from_this());
  }

  ///
  /// \brief Get weak.
  ///
  auto trackable::track() -> weak<trackable>
  {
    return weak(std::enable_shared_from_this<trackable>::shared_from_this());
  }

} // namespace y2::ui