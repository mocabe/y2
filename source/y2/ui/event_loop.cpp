// SPDX-License-Identifier: MIT

#include <y2/ui/event_loop.hpp>
#include <y2/ui/os/event_queue.hpp>
#include <y2/core/exception.hpp>

namespace y2::ui {

  namespace {

    struct thread_data
    {
    private:
      std::vector<event_loop*> stack;

    public:
      void push_stack(event_loop& e)
      {
        stack.push_back(&e);
      }

      void pop_stack()
      {
        stack.pop_back();
      }

      auto level() const
      {
        return stack.size();
      }

      auto& current()
      {
        if (stack.empty()) {
          throw exception("event_loop is not running");
        }
        return *stack.back();
      }
    };

    auto get_thread_data() -> thread_data&
    {
      thread_local auto data = thread_data();
      return data;
    }

    struct loop_scope
    {
      loop_scope(event_loop& loop)
      {
        auto& data = get_thread_data();
        data.push_stack(loop);
      }

      ~loop_scope()
      {
        auto& data = get_thread_data();
        data.pop_stack();
      }
    };

  } // namespace

  ///
  /// \brief Get current event loop level.
  ///
  /// Loop level starts from 1.
  /// Returns 0 when there's no event loop running.
  ///
  auto event_loop::level() -> u32
  {
    if (!os_event_queue::thread_has_context()) {
      return 0;
    }
    auto& data = get_thread_data();
    return static_cast<u32>(data.level());
  }

  ///
  /// \brief Initialize event loop.
  ///
  event_loop::event_loop()
  {
  }

  ///
  /// \brief Running?
  ///
  bool event_loop::running() const
  {
    return m_running;
  }

  ///
  /// \brief Run event loop.
  ///
  void event_loop::run()
  {
    if (!running()) {
      auto scope = loop_scope(*this);
      os_event_queue::thread_local_context()->run();
    }
  }

  ///
  /// \brief Stop current event loop.
  ///
  void event_loop::stop()
  {
    if (running()) {
      os_event_queue::thread_local_context()->stop();
    }
  }
}