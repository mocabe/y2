// SPDX-License-Identifier: MIT

#include <y2/ui/key_events.hpp>

namespace y2::ui::events {

  using namespace scoped_enum_operators;

  key_event::key_event(event_phase phase, ui::key key)
    : event(phase)
    , m_key {key}
  {
  }

  key_press::key_press(
    event_phase phase,
    ui::key key,
    ui::key_action action,
    ui::key_mods mods)
    : key_event(phase, key)
    , m_action {action}
    , m_mods {mods}
  {
  }

  auto key_press::key() const -> ui::key { return m_key; }

  auto key_press::modifiers() const -> ui::key_mods { return m_mods; }

  auto key_press::action() const -> ui::key_action { return m_action; }

  bool key_press::is_repeat() const { return m_action == key_action::repeat; }

  bool key_press::test_modifiers(ui::key_mods mods) const
  {
    return static_cast<bool>(m_mods & mods);
  }

  bool key_press::has_modifier() const { return static_cast<bool>(m_mods); }

  bool key_press::shift() const { return test_modifiers(key_mods::shift); }

  bool key_press::control() const { return test_modifiers(key_mods::control); }

  bool key_press::alt() const { return test_modifiers(key_mods::alt); }

  bool key_press::super() const { return test_modifiers(key_mods::super); }

  bool key_press::caps_lock() const
  {
    return test_modifiers(key_mods::caps_lock);
  }

  bool key_press::num_lock() const
  {
    return test_modifiers(key_mods::num_lock);
  }

  key_release::key_release(event_phase phase, ui::key key)
    : key_event(phase, key)
  {
  }

  auto key_release::key() const -> ui::key { return m_key; }

  key_char::key_char(event_phase phase, std::u8string str)
    : key_event(phase, {})
    , m_str {std::move(str)}
  {
  }

  auto key_char::chars() const -> std::u8string_view { return m_str; }

} // namespace y2::ui::events