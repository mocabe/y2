y2_add_library(y2-ui 
  trackable.cpp
  event_receiver.cpp
  event.cpp
  application.cpp
  application_events.cpp
  screen.cpp
  screen_events.cpp
  key_events.cpp
  mouse_events.cpp
  command_line_function.cpp
  event_phase_scope.cpp
  event_handler_list.cpp
  event_dispatcher.cpp
  timer_events.cpp
  timer.cpp
  event_loop.cpp
)

target_link_libraries(y2-ui PRIVATE y2::config)
target_link_libraries(y2-ui PRIVATE y2::core)
target_link_libraries(y2-ui PRIVATE y2::log)

target_precompile_headers(y2-ui PUBLIC
  <y2/ui/memory.hpp>
  <y2/ui/signal.hpp>
)

add_subdirectory(os)

if(WIN32)
  add_subdirectory(win32)
endif()

y2_source_group(y2-ui)
