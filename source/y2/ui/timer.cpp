// SPDX-License-Identifier: MIT

#include <y2/ui/timer.hpp>
#include <y2/ui/os/event_queue.hpp>
#include <y2/ui/timer_events.hpp>
#include <y2/core/exception.hpp>

namespace y2::ui {

  class timer::delegate final : public os_timer_delegate
  {
    timer& m_owner;
    s32 m_timer_id = os_event_queue::next_timer_id();
    std::chrono::milliseconds m_interval;

  public:
    delegate(timer& owner)
      : m_owner {owner}
    {
    }

    ~delegate() noexcept
    {
      stop();
    }

    auto id() const noexcept -> s32 override
    {
      return m_timer_id;
    }

    auto interval() const noexcept -> std::chrono::milliseconds override
    {
      return m_interval;
    }

    void process_event(os_event_queue_events::timer_timeout& e) override
    {
      auto event = timer_events::timeout(e.id());
      m_owner.m_handlers.handle_event(event);
    }

    void start()
    {
      auto queue = os_event_queue::thread_local_context();
      queue->add_timer_delegate(*this);
    }

    void stop()
    {
      auto queue = os_event_queue::thread_local_context();
      queue->remove_timer_delegate(*this);
    }

    void set_interval(std::chrono::milliseconds interval)
    {
      m_interval = interval;
    }
  };

  void timer::init()
  {
    assert(!m_delegate);
    m_delegate = make_unique<delegate>(*this);
  }

  timer::timer()
  {
    init();
  }

  timer::~timer() noexcept
  {
  }

  timer::timer(timer&& other) noexcept
    : m_handlers {std::move(other.m_handlers)}
  {
    init();
  }

  timer& timer::operator=(timer&& other) noexcept
  {
    m_handlers.clear();
    m_handlers = std::move(other.m_handlers);
    return *this;
  }

  void timer::start()
  {
    Y2_ASSERT(m_delegate);
    if (m_delegate) {
      m_delegate->start();
    }
  }

  void timer::stop()
  {
    Y2_ASSERT(m_delegate);
    if (m_delegate) {
      m_delegate->stop();
    }
  }

  auto timer::id() const -> u32
  {
    Y2_ASSERT(m_delegate);
    if (m_delegate) {
      return m_delegate->id();
    }
    return {};
  }

  auto timer::interval() const -> interval_type
  {
    Y2_ASSERT(m_delegate);
    if (m_delegate) {
      return m_delegate->interval();
    }
    return {};
  }

  void timer::set_interval(std::chrono::milliseconds interval)
  {
    Y2_ASSERT(m_delegate);
    if (m_delegate) {
      m_delegate->set_interval(interval);
    }
  }

} // namespace y2::ui