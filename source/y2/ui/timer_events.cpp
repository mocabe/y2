// SPDX-License-Identifier: MIT

#include <y2/ui/timer_events.hpp>

namespace y2::ui::events {

  timer_event::timer_event()
    : event(event_phase::target)
  {
  }
} // namespace y2::ui::events

namespace y2::ui::timer_events {

  timeout::timeout(s32 id)
    : timer_event()
    , m_id {id}
  {
  }

  auto timeout::id() const -> s32
  {
    return m_id;
  }

} // namespace y2::ui::timer_events
