// SPDX-License-Identifier: MIT

#include <y2/ui/application.hpp>
#include <y2/ui/os/application.hpp>
#include <y2/ui/os/screen_info.hpp>
#include <y2/ui/application_events.hpp>
#include <y2/ui/command_line_function.hpp>

#include <y2/core/exception.hpp>
#include <y2/ui/memory.hpp>
#include <y2/log/logger.hpp>

#include <range/v3/view.hpp>

Y2_DECL_LOCAL_LOGGER(ui::application)

namespace y2::ui {

  static application* s_app = nullptr;

  class application::impl final : os_application_delegate
  {
    application& m_self;
    y2::context<os_application> m_os;

  public:
    impl(application& self, string id, std::vector<string> args)
      : m_self {self}
    {
      if (s_app) {
        throw exception("Cannot create multiple application instance");
      }
      s_app = &self;

      os_application::configure(std::move(id), std::move(args));
      m_os = os_application::context();
      m_os->set_delegate(*this);
    }

    ~impl() noexcept
    {
      s_app = nullptr;
      m_os->remove_delegate();
    }

    auto run() -> exit_code
    {
      return m_os->run();
    }

    void process_event(events::os_application_start& e) override
    {
      auto event = application_events::start(e.id(), e.args());
      m_self.send_event(event);
    }

    void process_event(events::os_application_exit& e) override
    {
      auto event = application_events::exit(e.code());
      m_self.send_event(event);
    }

    void process_event(events::os_application_unhandled_exception& e) override
    {
      auto event = application_events::unhandled_exception(e.exception());
      m_self.send_event(event);
    }

    void process_event(events::os_screen_connect& e) override
    {
      auto event = screen_events::connect(ui::screen(e.screen()));
      m_self.send_event(event);
    }

    void process_event(events::os_screen_disconnect& e) override
    {
      auto event = screen_events::disconnect(ui::screen(e.screen()));
      m_self.send_event(event);
    }

    void process_event(events::os_primary_screen_change& e) override
    {
      auto event = screen_events::primary_change(ui::screen(e.screen()));
      m_self.send_event(event);
    }
  };

  ///
  /// \brief Get application instance.
  ///
  auto application::instance() -> application&
  {
    if (s_app) {
      return *s_app;
    }
    throw exception("No application instance");
  }

  ///
  /// \brief Send to event to receiver.
  /// \param r Receiver of event.
  /// \param e Event to send.
  ///
  bool application::send_event(ui::event_receiver& r, ui::event& e)
  {
    return os_application::context()->send_event(r, e);
  }

  ///
  /// \brief Post event to receiver.
  /// \param r Receiver of event.
  /// \param e Event to post.
  ///
  void application::post_event(ui::event_receiver& r, unique<ui::event> e)
  {
    os_application::context()->post_event(r, std::move(e));
  }

  ///
  /// \brief Exit application.
  ///
  void application::exit(exit_code code)
  {
    os_application::context()->exit(code);
  }

  ///
  /// \brief Initialize application object.
  ///
  /// \param id Application ID.
  /// \param args List of command line arguments.
  ///
  /// \throw exception for invalid arguments.
  ///
  /// Application ID must be reversed domain name like "com.example.myapp".
  /// Command line arguments should not include executable path.
  ///
  /// Application instance is global resource.
  /// You should not create multiple application instances.
  ///
  application::application(badge<application>, string id, std::vector<string> args)
    : event_receiver(nullptr)
    , m_impl {ui::make_unique<impl>(*this, std::move(id), std::move(args))}
  {
  }

  ///
  /// \brief Initialize application object.
  ///
  /// \param id Application ID.
  /// \param argc Argument passed to argc.
  /// \param argv Argument passed to argv.
  ///
  /// \throw exception for invalid arguments.
  ///
  application::application(badge<application>, string id, int argc, char** argv)
    : application({}, std::move(id), command_line_function::parse_args(argc, argv))
  {
  }

  ///
  /// \brief Destructor.
  ///
  application::~application() noexcept
  {
  }

  ///
  /// \brief Run application's event loop.
  ///
  auto application::run() -> exit_code
  {
    return m_impl->run();
  }

  ///
  /// \brief Send event to application.
  ///
  bool application::send_event(ui::event& e)
  {
    if (e.current_phase() == event_phase::none) {
      return event_dispatcher().dispatch(e);
    }
    return receive_event(e);
  }

  ///
  /// \brief Post event to application.
  /// \param e Unique event instance.
  ///
  void application::post_event(unique<ui::event> e)
  {
    post_event(*this, std::move(e));
  }

  ///
  /// \brief Receive event sent to application.
  ///
  /// This function receives all incoming events to the application before
  /// dispatching to specific event functions.
  ///
  /// \param event Received event.
  ///
  bool application::receive_event(ui::event& event)
  {
    using events::application_event;
    using events::screen_event;

    if (auto e = event.get_as<application_event>()) {
      return receive_event(*e);
    }
    if (auto e = event.get_as<screen_event>()) {
      return receive_event(*e);
    }
    return true;
  }

  ///
  /// \brief Receive application events.
  /// \param event Received event.
  ///
  bool application::receive_event(events::application_event& event)
  {
    using application_events::unhandled_exception;

    if (auto e = event.get_as<unhandled_exception>()) {
      std::rethrow_exception(e->exception());
    }
    return true;
  }

  ///
  /// \brief Receive screen events.
  /// \param event Received event.
  ///
  bool application::receive_event(events::screen_event& event)
  {
    Y2_UNUSED(event);
    return true;
  }

} // namespace y2::ui