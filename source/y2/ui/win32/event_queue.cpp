// SPDX-License-Identifier: MIT

#include <y2/ui/win32/event_queue.hpp>
#include <y2/ui/win32/util.hpp>
#include <y2/ui/win32/window_message.hpp>

#include <y2/core/exception.hpp>
#include <y2/log/logger.hpp>

#include <mutex>

Y2_DECL_LOCAL_LOGGER(win32::event_queue)

namespace y2::ui::win32 {

  namespace {

    ///
    /// \brief Message procedure.
    ///
    auto CALLBACK message_window_proc(HWND wnd, UINT msg, WPARAM wparam, LPARAM lparam) -> LRESULT
    {
      if (auto data = GetWindowLongPtrW(wnd, GWLP_USERDATA)) {
        auto _this = reinterpret_cast<win32::event_queue*>(data);
        return _this->message_window_proc(wnd, msg, wparam, lparam);
      }
      return DefWindowProcW(wnd, msg, wparam, lparam);
    }

    constexpr auto class_name = L"799e25bc-4072-46bf-9980-5bf7c2065aa4";

    ///
    /// \brief Register window class for message only window.
    ///
    auto register_message_only_window(HINSTANCE instance)
    {
      auto wndclass = make_struct<WNDCLASSEXW>();

      wndclass.lpfnWndProc   = message_window_proc;
      wndclass.hInstance     = instance;
      wndclass.lpszClassName = class_name;

      if (auto atom = RegisterClassExW(&wndclass)) {
        return atom;
      }
      throw exception("Failed to register window class");
    }

    ///
    /// \brief Unregister window class.
    ///
    void unregister_message_only_window(HINSTANCE instance)
    {
      if (!UnregisterClassW(class_name, instance)) {
        debug_log_error("UnregisterClassW failed");
      }
    }

    ///
    /// \brief Create new message-only window.
    ///
    /// \note Window class must be registered beforehand.
    ///
    auto create_message_only_window(HINSTANCE instance) -> HWND
    {
      auto hwnd = CreateWindowExW(
        0, class_name, class_name, 0, 0, 0, 0, 0, HWND_MESSAGE, NULL, instance, NULL);

      if (hwnd) {
        return hwnd;
      }
      throw exception("Failed to create message only window");
    }

    ///
    /// \brief Destroy window.
    ///
    void destroy_message_only_window(HWND wnd)
    {
      if (!DestroyWindow(wnd)) {
        debug_log_error("DestroyWindow failed");
      }
    }

    ///
    /// \brief Disable UOI_TIMERPROC_EXCEPTION_SUPPRESSION.
    ///
    void disable_timer_exception_suppression()
    {
      auto suppress = BOOL(FALSE);
      auto result   = SetUserObjectInformationW(
        GetCurrentProcess(), UOI_TIMERPROC_EXCEPTION_SUPPRESSION, &suppress, sizeof(BOOL));

      if (!result) {
        debug_log_warning("SetUserObjectInformation failed");
      }
    }

    // -- context data --

    auto s_ctx_holder = shared_context_holder<win32::event_queue_context>();

    // -- thread data --

    thread_local auto s_thread_ctx_holder = shared_context_holder<win32::event_queue>();

  } // namespace

  ///
  /// \brief Configure global context.
  ///
  void event_queue_context::configure(HINSTANCE inst)
  {
    Y2_ASSERT(inst);
    s_ctx_holder.configure(
      [=] { return std::make_shared<event_queue_context>(inst, badge<event_queue_context>()); });
  }

  ///
  /// \brief Get global context.
  ///
  auto event_queue_context::context() -> y2::context<event_queue_context>
  {
    return s_ctx_holder.get_context();
  }

  ///
  /// \brief Initialize event queue context.
  ///
  event_queue_context::event_queue_context(HINSTANCE inst, badge<event_queue_context>)
    : m_instance {inst}
  {
    m_wnd_class = register_message_only_window(m_instance);
    disable_timer_exception_suppression();
  }

  ///
  /// \brief Destroy event queue context.
  ///
  event_queue_context::~event_queue_context() noexcept
  {
    unregister_message_only_window(m_instance);
  }

  ///
  /// \brief Get window class ATOM.
  ///
  auto event_queue_context::window_class() const -> ATOM
  {
    return m_wnd_class;
  }

  ///
  /// \brief Create message only window for event queue.
  ///
  auto event_queue_context::create_window() -> HWND
  {
    return create_message_only_window(m_instance);
  }

  ///
  /// \brief Constructor.
  ///
  event_queue::event_queue(badge<event_queue>)
  {
    m_ctx = event_queue_context::context();
    m_wnd = m_ctx->create_window();
    SetWindowLongPtrW(m_wnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(this));
  }

  ///
  /// \brief Destructor.
  ///
  event_queue::~event_queue() noexcept
  {
    destroy_message_only_window(m_wnd);
  }

  ///
  /// \brief Configure thread local context.
  ///
  void event_queue::thread_configure()
  {
    s_thread_ctx_holder.configure([] {
      if constexpr (is_debug) {
        std::stringstream ss;
        ss << std::this_thread::get_id();
        debug_log_info("Initializing event queue on thread {}", ss.str());
      }
      return std::make_shared<win32::event_queue>(badge<win32::event_queue>());
    });
  }

  ///
  /// \brief Does thread have active context?
  ///
  bool event_queue::thread_has_context()
  {
    return s_thread_ctx_holder.has_context();
  }

  ///
  /// \brief Get thread local context.
  ///
  auto event_queue::thread_local_context() -> context<event_queue>
  {
    return s_thread_ctx_holder.get_context();
  }

  ///
  /// \brief Exit event loop.
  ///
  void event_queue::exit(badge<win32::application>)
  {
    PostQuitMessage(0);
  }

  ///
  /// \brief Message procedure.
  ///
  auto event_queue::message_window_proc(HWND wnd, UINT msg, WPARAM wParam, LPARAM lParam) noexcept
    -> LRESULT
  {
    if (msg == WM_TIMER) {
      try {
        if (!KillTimer(m_wnd, wParam)) {
          debug_log_warning("KillTimer(id={}) failed", wParam);
        }
        send_timer_expiration(static_cast<u32>(wParam));
      } catch (...) {
        m_exception = std::current_exception();
      }
      return 0;
    }

    if (msg == WM_Y2_POSTEVENT) {
      try {
        send_posted_event();
      } catch (...) {
        m_exception = std::current_exception();
      }
      return 0;
    }

    if (msg == WM_Y2_WAKE) {
      return 0;
    }

    return DefWindowProcW(wnd, msg, wParam, lParam);
  }

  ///
  /// \brief Process window message.
  ///
  bool event_queue::process_message(const MSG& msg)
  {
    if (msg.message == WM_QUIT) {
      PostQuitMessage(static_cast<int>(msg.wParam));
      return false;
    }

    TranslateMessage(&msg);
    DispatchMessageW(&msg);

    if (auto ex = std::exchange(m_exception, nullptr)) {
      std::rethrow_exception(ex);
    }
    return true;
  }

  ///
  /// \brief Run message loop.
  ///
  void event_queue::run()
  {
    while (true) {

      if (m_stop.exchange(false)) {
        break;
      }

      auto msg    = MSG {};
      auto result = GetMessageW(&msg, m_wnd, 0, 0);

      if (result == -1) {
        debug_log_error("GetMessageW failed. error={}", GetLastError());
        break;
      }

      // WM_QUIT or other messages.
      if (!process_message(msg)) {
        break;
      }
    }
  }

  ///
  /// \brief Wake up message loop.
  ///
  /// This causes wait() to return.
  ///
  void event_queue::wake()
  {
    PostMessageW(m_wnd, WM_Y2_WAKE, 0, 0);
  }

  ///
  /// \brief Stop event queue.
  ///
  void event_queue::stop()
  {
    m_stop.store(true);
    wake();
  }

  ///
  /// \brief Post message to queue.
  ///
  /// When posted message is dispatched, delegate will be called.
  ///
  void event_queue::post()
  {
    if (!PostMessageW(m_wnd, WM_Y2_POSTEVENT, 0, 0)) {
      throw exception("PostMessageW failed");
    }
  }

  ///
  /// \brief Set timer.
  ///
  void event_queue::set_timer(u32 id, std::chrono::milliseconds interval)
  {
    if (!SetTimer(m_wnd, id, static_cast<UINT>(interval.count()), NULL)) {
      throw exception("SetTimer failed");
    }
  }

  ///
  /// \brief Remove timer.
  ///
  void event_queue::remove_timer(u32 id)
  {
    if (!KillTimer(m_wnd, static_cast<UINT_PTR>(id))) {
      debug_log_warning("KillTimer(id={}) failed", id);
    }
  }

} // namespace y2::ui::win32