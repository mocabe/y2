// SPDX-License-Identifier: MIT

#include <y2/ui/win32/screen_info.hpp>
#include <y2/ui/win32/screen.hpp>
#include <y2/ui/screen.hpp>
#include <y2/ui/screen_events.hpp>

#include <y2/log/logger.hpp>
#include <range/v3/view.hpp>

Y2_DECL_LOCAL_LOGGER(ui::win32::screen_info)

namespace y2::ui::win32 {

  namespace {

    using screen_array = std::vector<shared<screen>>;

    struct monitor_enum_proc_data
    {
      screen_array& screens;
      screen_array unchanged;
      screen_array connected;
    };

    ///
    /// Screen update callback.
    ///
    BOOL CALLBACK
      monitor_enum_proc(HMONITOR hMonitor, HDC /*hdc*/, LPRECT /*lpRect*/, LPARAM dwData)
    {
      auto* data = reinterpret_cast<monitor_enum_proc_data*>(dwData);

      auto& screens = data->screens;

      auto it = std::find_if(
        screens.begin(), screens.end(), [&](auto&& p) { return p->native_handle() == hMonitor; });

      if (it != screens.end()) {
        data->unchanged.push_back(std::move(*it));
        screens.erase(it);
      } else {
        data->connected.push_back(make_shared<screen>(hMonitor));
      }
      return TRUE;
    }

    struct screen_update_info
    {
      screen_array unchanged;
      screen_array connected;
      screen_array disconnected;
    };

    ///
    /// Generate update info of monitors.
    ///
    auto poll_screen_updates(const screen_array& screens)
    {
      auto tmp_screens = screens;

      auto data = monitor_enum_proc_data {.screens = tmp_screens};

      EnumDisplayMonitors(
        nullptr,
        nullptr,
        static_cast<MONITORENUMPROC>(monitor_enum_proc),
        reinterpret_cast<LPARAM>(&data));

      auto update_info = screen_update_info();

      update_info.unchanged    = std::move(data.unchanged);
      update_info.connected    = std::move(data.connected);
      update_info.disconnected = std::move(tmp_screens);

      return update_info;
    }

    ///
    /// Get primary screen.
    ///
    auto get_primary_screen_handle() -> HMONITOR
    {
      const POINT p = {0, 0};
      return MonitorFromPoint(p, MONITOR_DEFAULTTOPRIMARY);
    }

  } // namespace

  ///
  /// \brief Create screen info.
  ///
  screen_info::screen_info()
  {
    refresh();
  }

  ///
  /// \brief Destroy screen info.
  ///
  screen_info::~screen_info() noexcept
  {
    remove_delegate();

    for (auto&& m : m_screens) {
      m->invalidate();
    }
  }

  ///
  /// \brief Get primary screen.
  ///
  auto screen_info::primary() -> shared<os_screen>
  {
    if (!has_delegate()) {
      refresh();
    }

    assert(!m_screens.empty());
    return m_screens.at(0);
  }

  ///
  /// \brief List screens.
  ///
  auto screen_info::screens() -> std::vector<shared<os_screen>>
  {
    if (!has_delegate()) {
      refresh();
    }

    return m_screens //
           | ranges::views::transform([](auto& m) -> shared<os_screen> { return m; })
           | ranges::to_vector;
  }

  ///
  /// \brief Get default DPI.
  ///
  auto screen_info::default_dpi() -> unit::logical_dpi
  {
    return unit::logical_dpi(USER_DEFAULT_SCREEN_DPI);
  }

  ///
  /// \brief Add connected screens.
  ///
  void screen_info::add_screens(const std::vector<shared<screen>>& ms)
  {
    m_screens.insert(m_screens.end(), ms.begin(), ms.end());

    auto old_primary = m_screens[0]->native_handle();

    // Move primary screen to front.
    const auto primary = get_primary_screen_handle();
    std::stable_partition(
      m_screens.begin(), m_screens.end(), [&](auto&& m) { return m->native_handle() == primary; });

    for (auto&& m : ms) {
      auto event = events::os_screen_connect(m);
      send_event(event);
    }

    if (old_primary != primary) {
      auto event = events::os_primary_screen_change(m_screens[0]);
      send_event(event);
    }
  }

  ///
  /// \brief Remove disconnected screens.
  ///
  void screen_info::remove_screens(const std::vector<shared<screen>>& ms)
  {
    for (auto&& m : ms) {
      std::erase(m_screens, m);
      m->invalidate();
    }

    for (auto&& m : ms) {
      auto event = events::os_screen_disconnect(m);
      send_event(event);
    }
  }

  ///
  /// \brief Refresh screen properties.
  ///
  void screen_info::refresh_screens(const std::vector<shared<screen>>& ms)
  {
    for (auto&& m : ms) {
      assert(m);
      m->refresh();
    }
  }

  ///
  /// \brief Get instance.
  ///
  auto screen_info::context() -> y2::context<screen_info>
  {
    static auto s_ctx = std::weak_ptr<screen_info>();

    auto ctx = s_ctx.lock();

    if (!ctx) {
      ctx   = std::make_shared<screen_info>();
      s_ctx = ctx;
    }
    return y2::context<screen_info>::make_by_sharing(ctx);
  }

  ///
  /// \brief Refresh screen info.
  ///
  void screen_info::refresh()
  {
    const auto update_info = poll_screen_updates(m_screens);

    /// Add new screens first.
    /// Possibly change primary screen.
    add_screens(update_info.connected);
    /// Refersh screen properties.
    refresh_screens(update_info.unchanged);
    /// Remove screens no longer exist.
    remove_screens(update_info.disconnected);
  }

  ///
  /// \brief Find screen from native handle.
  /// \param h Native handle.
  ///
  auto screen_info::find(HMONITOR h) -> shared<os_screen>
  {
    for (const auto& s : m_screens) {
      if (s->native_handle() == h) {
        return s;
      }
    }
    return nullptr;
  }

} // namespace y2::ui::win32