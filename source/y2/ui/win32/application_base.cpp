// SPDX-License-Identifier: MIT

#include <y2/ui/win32/application_base.hpp>

namespace y2::ui::win32 {

  ///
  /// \brief Initialize application.
  /// \param inst Module instance handle.
  ///
  application_base::application_base(HINSTANCE inst)
    : m_inst {inst}
  {
    event_queue_context::configure(instance_handle());
    m_event_queue = event_queue_context::context();
  }

  ///
  /// \brief Get instance handle.
  ///
  auto application_base::instance_handle() const -> HINSTANCE
  {
    return m_inst;
  }

} // namespace y2::ui::win32
