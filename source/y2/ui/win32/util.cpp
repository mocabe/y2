// SPDX-License-Identifier: MIT

#include <y2/ui/win32/util.hpp>

namespace y2::ui::win32 {

  auto to_rect(RECT rect) noexcept -> ui::rect_px
  {
    using unit::pixel;
    const auto p1 = ui::point_px(static_cast<f64>(rect.left), static_cast<f64>(rect.top));
    const auto p2 = ui::point_px(static_cast<f64>(rect.right), static_cast<f64>(rect.bottom));
    return {p1, p2};
  }

  auto to_size(SIZE size) noexcept -> ui::size_px
  {
    using unit::pixel;
    const auto x = pixel(static_cast<f64>(size.cx));
    const auto y = pixel(static_cast<f64>(size.cy));
    return {x, y};
  }

  auto wide_to_utf8(std::wstring_view sv) -> string
  {
    const auto len = WideCharToMultiByte(
      CP_UTF8, 0, sv.data(), static_cast<int>(sv.length()), NULL, 0, NULL, NULL);

    if (len == 0)
      return {};

    auto ret = std::string();
    ret.resize(len);

    WideCharToMultiByte(
      CP_UTF8,
      0,
      sv.data(),
      static_cast<int>(sv.length()),
      reinterpret_cast<char*>(ret.data()),
      static_cast<int>(ret.length()),
      NULL,
      NULL);

    return string::from_utf8_unchecked(ret);
  }

  auto utf8_to_wide(string_view sv) -> std::wstring
  {
    const auto len = MultiByteToWideChar(
      CP_UTF8, 0, reinterpret_cast<const char*>(sv.data()), static_cast<int>(sv.length()), NULL, 0);

    if (len == 0)
      return {};

    auto ret = std::wstring();
    ret.resize(len);

    MultiByteToWideChar(
      CP_UTF8,
      0,
      reinterpret_cast<const char*>(sv.data()),
      static_cast<int>(sv.length()),
      ret.data(),
      static_cast<int>(ret.length()));

    return ret;
  }

} // namespace y2::ui::win32