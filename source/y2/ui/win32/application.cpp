// SPDX-License-Identifier: MIT

#include <y2/ui/win32/application.hpp>
#include <y2/ui/win32/event_queue.hpp>
#include <y2/ui/win32/prelude.hpp>
#include <y2/ui/win32/util.hpp>
#include <y2/ui/os/screen_info.hpp>
#include <y2/ui/event_loop.hpp>
#include <y2/core/to_underlying.hpp>

#include <y2/log/logger.hpp>

Y2_DECL_LOCAL_LOGGER(win32::application)

namespace y2::ui::win32 {

  application::application(string id, std::vector<string> args)
    : application_base(GetModuleHandle(NULL))
    , os_application(std::move(id), std::move(args))
  {
    m_screen_info = screen_info::context();
    m_screen_info->set_delegate(*this);
    debug_log_info("Initialized");
  }

  application::~application() noexcept
  {
    m_screen_info->remove_delegate();
    debug_log_info("Destroyed");
  }

  auto application::run() -> exit_code
  {
    debug_log_info("Starting");

    auto start_event = events::os_application_start(id(), args());
    send_event(start_event);

    auto loop = event_loop();
    loop.run();

    if (m_exit_code) {
      return *std::exchange(m_exit_code, nullopt);
    }
    throw exception("Event loop exited without exit code");
  }

  void application::exit(exit_code code)
  {
    if (m_exit_code) {
      log_warning("Overwriting exit code");
    }
    m_exit_code = code;

    debug_log_info("Exiting");

    auto exit_event = events::os_application_exit(code);
    send_event(exit_event);

    win32::event_queue::thread_local_context()->exit({});
    process_all_posted_events();
  }

  void application::process_event(events::os_screen_connect& e)
  {
    send_event(e);
  }

  void application::process_event(events::os_screen_disconnect& e)
  {
    send_event(e);
  }

  void application::process_event(events::os_primary_screen_change& e)
  {
    send_event(e);
  }

} // namespace y2::ui::win32