// SPDX-License-Identifier: MIT

#include <y2/ui/win32/screen.hpp>
#include <y2/ui/win32/prelude.hpp>
#include <y2/ui/win32/util.hpp>

#include <ShellScalingApi.h>

namespace y2::ui::win32 {

  namespace {

    struct screen_info
    {
      ui::rect_px geometry;
      ui::rect_px work_area;
      string name;
      unit::content_scale content_scale;
      unit::logical_dpi logical_dpi;
      unit::physical_dpi physical_dpi;
    };

    auto get_screen_info(HMONITOR hMonitor)
    {
      auto info = make_struct<MONITORINFOEXW>();
      GetMonitorInfoW(hMonitor, &info);

      auto ret      = screen_info();
      ret.geometry  = to_rect(info.rcMonitor);
      ret.work_area = to_rect(info.rcWork);
      ret.name      = wide_to_utf8(info.szDevice);

      auto dpiX = UINT(USER_DEFAULT_SCREEN_DPI);
      auto dpiY = UINT();
      GetDpiForMonitor(hMonitor, MDT_EFFECTIVE_DPI, &dpiX, &dpiY);
      ret.content_scale = unit::content_scale(dpiX / static_cast<f64>(USER_DEFAULT_SCREEN_DPI));
      ret.logical_dpi   = unit::logical_dpi(dpiX);

      dpiX = USER_DEFAULT_SCREEN_DPI;
      GetDpiForMonitor(hMonitor, MDT_RAW_DPI, &dpiX, &dpiY);
      ret.physical_dpi = unit::physical_dpi(dpiX);

      return ret;
    }

  } // namespace

  screen::screen(HMONITOR hScreen)
    : m_hScreen {hScreen}
  {
    auto info       = get_screen_info(m_hScreen);
    m_geometry      = info.geometry;
    m_work_area     = info.work_area;
    m_name          = info.name;
    m_content_scale = info.content_scale;
    m_logical_dpi   = info.logical_dpi;
    m_physical_dpi  = info.physical_dpi;
  }

  screen::~screen() noexcept
  {
    m_delegates = {};
  }

  bool screen::valid() const
  {
    return m_hScreen;
  }

  auto screen::geometry() const -> ui::rect_px
  {
    return m_geometry;
  }

  auto screen::work_area() const -> ui::rect_px
  {
    return m_work_area;
  }

  auto screen::content_scale() const -> unit::content_scale
  {
    return m_content_scale;
  }

  auto screen::logical_dpi() const -> unit::logical_dpi
  {
    return m_logical_dpi;
  }

  auto screen::physical_dpi() const -> unit::physical_dpi
  {
    return m_physical_dpi;
  }

  auto screen::name() const -> string
  {
    return m_name;
  }

  auto screen::native_handle() const -> HANDLE
  {
    return m_hScreen;
  }

  void screen::invalidate()
  {
    m_hScreen = {};
  }

  void screen::refresh()
  {
    if (!valid())
      return;

    const auto info = get_screen_info(m_hScreen);

    const auto geometry      = std::exchange(m_geometry, info.geometry);
    const auto work_area     = std::exchange(m_work_area, info.work_area);
    const auto content_scale = std::exchange(m_content_scale, info.content_scale);
    const auto logical_dpi   = std::exchange(m_logical_dpi, info.logical_dpi);
    const auto physical_dpi  = std::exchange(m_physical_dpi, info.physical_dpi);
    const auto name          = std::exchange(m_name, info.name);

    auto event = events::os_screen_update();

    if (geometry != m_geometry) {
      event.set_geometry(m_geometry);
    }
    if (work_area != m_work_area) {
      event.set_work_area(m_work_area);
    }
    if (content_scale != m_content_scale) {
      event.set_content_scale(m_content_scale);
    }
    if (logical_dpi != m_logical_dpi) {
      event.set_logical_dpi(m_logical_dpi);
    }
    if (physical_dpi != m_physical_dpi) {
      event.set_physical_dpi(m_physical_dpi);
    }
    if (name != m_name) {
      event.set_name(m_name);
    }

    for (auto&& d : m_delegates) {
      d->process_event(event);
    }
  }

} // namespace y2::ui::win32