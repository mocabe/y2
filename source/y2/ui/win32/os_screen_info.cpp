// SPDX-License-Identifier: MIT

#include <y2/ui/win32/screen_info.hpp>

namespace y2::ui {

  ///
  /// \brief Get context.
  ///
  auto os_screen_info::context() -> y2::context<os_screen_info>
  {
    return y2::context<os_screen_info>(win32::screen_info::context());
  }

} // namespace y2::ui