// SPDX-License-Identifier: MIT

#include <y2/ui/win32/event_queue.hpp>
#include <y2/ui/win32/prelude.hpp>

#include <memory>

namespace y2::ui {

  ///
  /// \brief Configure thread local context.
  ///
  void os_event_queue::thread_configure()
  {
    win32::event_queue::thread_configure();
  }

  ///
  /// \brief Does thread have active context?
  ///
  bool os_event_queue::thread_has_context()
  {
    return win32::event_queue::thread_has_context();
  }

  ///
  /// \brief Get thread local event queue.
  ///
  auto os_event_queue::thread_local_context() -> context<os_event_queue>
  {
    return context<os_event_queue>(win32::event_queue::thread_local_context());
  }

} // namespace y2::ui