// SPDX-License-Identifier: MIT

#include <y2/ui/win32/application.hpp>
#include <y2/ui/win32/prelude.hpp>
#include <y2/ui/win32/util.hpp>

#include <y2/core/exception.hpp>
#include <y2/log/logger.hpp>

Y2_DECL_LOCAL_LOGGER(ui::os_application)

namespace y2::ui {

  namespace {
    auto s_holder = shared_context_holder<os_application>();
  }

  ///
  /// \brief Set parameters for initializing application context.
  ///
  /// This function does not initialize context.
  ///
  /// \param id Application's unique ID.
  /// \param args Command line arguments.
  /// \throw y2::exception when context is already configured or initialized.
  ///
  void os_application::configure(string id, std::vector<string> args)
  {
    s_holder.configure([id = std::move(id), args = std::move(args)] {
      return std::make_shared<win32::application>(id, args);
    });
  }

  ///
  /// \brief Get application context.
  ///
  /// Throws when context was not initialized.
  ///
  /// \return Valid context.
  /// \throw y2::exception on initialization failure.
  ///
  auto os_application::context() -> y2::context<os_application>
  {
    return s_holder.get_context();
  }

} // namespace y2::ui
