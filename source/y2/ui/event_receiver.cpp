// SPDX-License-Identifier: MIT

#include <y2/ui/event_receiver.hpp>
#include <y2/core/exception.hpp>

namespace y2::ui {

  ///
  /// \brief Build event receiver.
  ///
  event_receiver::event_receiver(ui::event_receiver* parent)
    : m_parent {parent}
  {
  }

  ///
  /// \brief Build event dispatcher.
  ///
  /// \param parent Parent dispatcher.
  /// \return Non-null event dispatcher instance.
  ///
  auto event_receiver::build_event_dispatcher(ui::event_receiver* parent)
    -> unique<ui::event_dispatcher>
  {
    if (parent) {
      auto& d = parent->event_dispatcher();
      return ui::event_dispatcher::make(*this, d);
    } else {
      return ui::event_dispatcher::make(*this);
    }
  }

  ///
  /// \brief Reset event dispatcher.
  ///
  void event_receiver::reset_event_dispatcher(ui::event_receiver* parent)
  {
    m_dispatcher = nullptr;
    m_parent     = parent;
  }

  ///
  /// \brief Get event dispatcher.
  ///
  auto event_receiver::event_dispatcher() -> ui::event_dispatcher&
  {
    if (!m_dispatcher) {
      m_dispatcher = build_event_dispatcher(m_parent);
    }

    if (!m_dispatcher) {
      throw exception("Invalid event dispatcher");
    }
    return *m_dispatcher;
  }

} // namespace y2::ui