// SPDX-License-Identifier: MIT

#include <y2/ui/application_events.hpp>

namespace y2::ui::events {

  ///
  /// \brief Initialize event.
  ///
  application_event::application_event()
    : event(event_phase::target)
  {
  }

} // namespace y2::ui::events

namespace y2::ui::application_events {

  ///
  /// \brief Initialize event.
  ///
  start::start(string id, std::vector<string> args)
    : m_id {std::move(id)}
    , m_args {std::move(args)}
  {
  }

  ///
  /// \brief Get arguments given to application.
  ///
  /// Returns same value to application::args().
  ///
  auto start::args() const -> std::vector<string>
  {
    return m_args;
  }

  ///
  /// \brief Get application ID. Same to application::id().
  ///
  /// Returns same value to application::id().
  ///
  auto start::id() const -> string
  {
    return m_id;
  }

  ///
  /// \brief Initialize event.
  ///
  exit::exit(exit_code code)
    : m_code {code}
  {
  }

  ///
  /// \brief Exit code.
  ///
  auto exit::code() const -> exit_code
  {
    return m_code;
  }

  ///
  /// \brief Initialize event.
  ///
  unhandled_exception::unhandled_exception(std::exception_ptr e)
    : m_e {e}
  {
  }

  ///
  /// \brief Get exception ptr.
  ///
  auto unhandled_exception::exception() const -> std::exception_ptr
  {
    return m_e;
  }

} // namespace y2::ui::application_events