// SPDX-License-Identifier: MIT

#include <y2/ui/event_phase_scope.hpp>
#include <y2/core/exception.hpp>

namespace y2::ui {

  ///
  /// \brief Create phase scope for event.
  /// \param event Event to set phase. Must be compatible with `phase`.
  /// \param phase New phase of event.
  /// \throw exception when unable to set phase to event.
  ///
  event_phase_scope::event_phase_scope(event& event, event_phase phase)
    : m_event {event}
    , m_old_phase {event.current_phase()}
  {
    if (!contains(event.phase(), phase)) {
      Y2_ASSERT(false);
      throw exception("Invalid phase for event");
    }
    m_event.set_current_phase(phase);
  }

  ///
  /// \brief Destroy phase scope by restoring old phase.
  ///
  event_phase_scope::~event_phase_scope() noexcept
  {
    m_event.set_current_phase(m_old_phase);
  }
}