// SPDX-License-Identifier: MIT

#include <y2/ui/event_handler_list.hpp>

namespace y2::ui {

  ///
  /// \brief Clear event handlers.
  ///
  void event_handler_list::clear()
  {
    m_signal.disconnect_all();
  }

} // namespace y2::ui