// SPDX-License-Identifier: MIT

#include <y2/ui/event_dispatcher.hpp>
#include <y2/ui/event_receiver.hpp>
#include <y2/ui/event_phase_scope.hpp>
#include <y2/ui/event.hpp>

namespace y2::ui {

  using namespace scoped_enum_operators;

  /// Make instance of event_dispatcher.
  ///
  /// \param owner Owner of dispatcher object.
  auto event_dispatcher::make(event_receiver& owner) -> unique<event_dispatcher>
  {
    return ui::make_unique<event_dispatcher>(badge<event_dispatcher>(), owner);
  }

  /// Make instance of event_dispatcher.
  ///
  /// \param owner Owner of dispatcher object.
  /// \param parent Parent dispatcher.
  auto event_dispatcher::make(event_receiver& owner, event_dispatcher& parent)
    -> unique<event_dispatcher>
  {
    return ui::make_unique<event_dispatcher>(badge<event_dispatcher>(), owner, parent);
  }

  event_dispatcher::event_dispatcher(badge<event_dispatcher>, event_receiver& owner)
    : m_owner {owner}
  {
  }

  event_dispatcher::event_dispatcher(
    badge<event_dispatcher>,
    event_receiver& owner,
    event_dispatcher& parent)
    : m_owner {owner}
    , m_parent {parent}
  {
  }

  bool event_dispatcher::send_event_with_phase(ui::event& e, event_phase p) const
  {
    Y2_ASSERT(contains(e.phase(), p));
    auto scope = event_phase_scope(e, p);
    return m_owner.send_event(e);
  }

  bool event_dispatcher::dispatch_with_phase(weak<event_dispatcher> d, ui::event& e, event_phase p)
  {
    if (d.expired()) {
      return false;
    }
    Y2_ASSERT(contains(e.phase(), p));
    auto scope = event_phase_scope(e, p);
    return d.get()->dispatch(e);
  }

  bool event_dispatcher::dispatch_capture(weak<event_dispatcher> d, ui::event& e)
  {
    if (d.expired()) {
      return false;
    }

    bool result = false;

    // t is root dispatcher.
    if (d.get()->m_parent.expired()) {
      d.lock([&](auto& _d) { result = _d.send_event_with_phase(e, event_phase::capture); });
      return result;
    }

    // Dispatch parent first.
    // Extends lifetime of current dispatcher.
    d.lock([&](auto& _d) { result = dispatch_with_phase(_d.m_parent, e, event_phase::capture); });

    // Stop dispatch.
    if (result) {
      return true;
    }

    // Lifetime of dispatcher is expired during propagation.
    // TODO: Should we also stop propagating when parent dispatcher destroyed?
    if (d.expired()) {
      return false;
    }

    // Send event to owner.
    d.lock([&](auto& _d) { result = _d.send_event_with_phase(e, event_phase::capture); });
    return result;
  }

  bool event_dispatcher::dispatch_bubble(weak<event_dispatcher> d, ui::event& e)
  {
    if (d.expired()) {
      return false;
    }

    bool result = false;

    // Send event to owner.
    d.lock([&](auto& _d) { result = _d.send_event_with_phase(e, event_phase::bubble); });

    // Stop dispatch
    if (result) {
      return true;
    }

    // Lifetime of dispatcher is expired during event processsig.
    // Or this dispatcher is root.
    if (d.expired() || d.get()->m_parent.expired()) {
      return false;
    }

    // Dispatch on parent.
    d.lock([&](auto& _d) { result = dispatch_with_phase(_d.m_parent, e, event_phase::bubble); });
    return result;
  }

  bool event_dispatcher::dispatch_target(weak<event_dispatcher> t, ui::event& event)
  {
    if (t.expired()) {
      return false;
    }
    bool result = false;
    t.lock([&](auto& d) { result = d.send_event_with_phase(event, event_phase::target); });
    return result;
  }

  bool event_dispatcher::dispatch_internal(ui::event& event)
  {
    const auto phase = event.phase();
    const auto self  = weak(*this);

    // Parent should be valid if exists, but actually not.
    if (!m_parent.is_null() && m_parent.expired()) {
      assert(false);
      return false;
    }

    // Capture phase.
    if (contains(phase, event_phase::capture)) {
      if (dispatch_with_phase(m_parent, event, event_phase::capture)) {
        return true;
      }
      if (self.expired()) {
        return false;
      }
    }

    // Target phase.
    if (contains(phase, event_phase::target)) {
      if (dispatch_with_phase(self, event, event_phase::target)) {
        return true;
      }
      if (self.expired()) {
        return false;
      }
    }

    // Bubble phase.
    if (contains(phase, event_phase::bubble)) {
      return dispatch_with_phase(m_parent, event, event_phase::bubble);
    }

    return false;
  }

  bool event_dispatcher::dispatch(ui::event& event)
  {
    const auto current_phase = event.current_phase();

    if (current_phase == event_phase::capture) {
      return dispatch_capture(weak(*this), event);
    }
    if (current_phase == event_phase::target) {
      return dispatch_target(weak(*this), event);
    }
    if (current_phase == event_phase::bubble) {
      return dispatch_bubble(weak(*this), event);
    }

    if (current_phase == event_phase::none) {
      return dispatch_internal(event);
    }

    return false;
  }

} // namespace y2::ui