cmake_minimum_required(VERSION 3.20)
cmake_policy(VERSION 3.20)

# ------------------------------------------
# Directories

set(Y2_DIR ${CMAKE_CURRENT_SOURCE_DIR})
set(Y2_INCLUDE_DIR ${Y2_DIR}/include)
set(Y2_SOURCE_DIR ${Y2_DIR}/source)
set(Y2_EXTERNAL_DIR ${Y2_DIR}/external)
set(Y2_TEST_DIR ${Y2_DIR}/test)
set(Y2_TOOL_DIR ${Y2_DIR}/tool)

# ------------------------------------------
# Build type

set(BUILD_SHARED_LIBS OFF)
message(STATUS "Build type: ${CMAKE_BUILD_TYPE}")

# ------------------------------------------
# Windows SDK version

if(WIN32) 
  set(CMAKE_SYSTEM_VERSION 10.0.22000)
endif()

# ------------------------------------------
# Git

find_package(Git REQUIRED)

# ------------------------------------------
# Package manager

execute_process(COMMAND ${GIT_EXECUTABLE} submodule update --init ${VCPKG_SOURCE_DIR})
set(VCPKG_SOURCE_DIR ${Y2_EXTERNAL_DIR}/vcpkg)

# select appropriate triplet and toolchain
if(WIN32)
  # dynamic CRT, static libs
  set(VCPKG_TARGET_TRIPLET x64-windows-static-md CACHE STRING "")
endif()

# set toolchain
set(CMAKE_TOOLCHAIN_FILE ${VCPKG_SOURCE_DIR}/scripts/buildsystems/vcpkg.cmake CACHE STRING "")

# ------------------------------------------
# Project

project(y2 CXX)

if((NOT (CMAKE_CXX_COMPILER_ID MATCHES MSVC)) AND (NOT (CMAKE_BUILD_TYPE MATCHES Debug|Release|RelWithDebInfo|MinSizeRel)))
  message(FATAL_ERROR "Invalid build type!")
endif()

# ------------------------------------------
# Options

option(Y2_BUILD_TESTS "Build tests" ON)
option(Y2_ADDRESS_SANITIZER "Enable address sanitizer" OFF)
option(Y2_THREAD_SANITIZER "Enable thread sanitizer" OFF)
option(Y2_UB_SANITIZER "Enable undefined behaviour sanitizer" OFF)
option(Y2_MEMORY_SANITIZER "Enable meomry sanitizer" OFF)

# ------------------------------------------
# Compilers

message(STATUS "Compiler: ${CMAKE_CXX_COMPILER_ID}, Version: ${CMAKE_CXX_COMPILER_VERSION}")

if(NOT (CMAKE_CXX_COMPILER_ID MATCHES MSVC|Clang|GNU))
  message(FATAL_ERROR "Unsupported compiler!")
endif()

# ------------------------------------------
# CMake global settings

set_property(GLOBAL PROPERTY USE_FOLDERS ON)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

# ------------------------------------------
# Default C++ standard

set(CMAKE_CXX_STANDARD 20)

# ------------------------------------------
# External

include(${Y2_EXTERNAL_DIR}/external.cmake)

# ------------------------------------------
# Compiler Flags

if(CMAKE_CXX_COMPILER_ID MATCHES MSVC)
  # Remove /RTC1 from default set of compile flags.
  STRING (REGEX REPLACE "/RTC1" "" CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG}")
endif()

if(CMAKE_CXX_COMPILER_ID MATCHES Clang|GNU)
  # Use -O2
  STRING (REGEX REPLACE "-O3" "-O2" CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE}")
endif()

# Common compiler flags
set(Y2_COMPILE_FLAGS
  $<$<CXX_COMPILER_ID:MSVC>:/W4 /WX /Zi /EHsc /permissive- /wd4250 /d2FH4 /Zc:__cplusplus /Zc:preprocessor /MP>
  $<$<CXX_COMPILER_ID:Clang>:-Wall -Wextra -Werror -g -pedantic -Wshadow>
  $<$<CXX_COMPILER_ID:GNU>:-Wall -Wextra -Werror -g -pedantic -Wshadow-compatible-local>
)

# Common linker flags
set(Y2_LINK_FLAGS
  $<$<CXX_COMPILER_ID:Clang>:-latomic>
)

# Sanitizer options
if(Y2_ADDRESS_SANITIZER)
  set(Y2_SANITIZER_STRING "address")
elseif(Y2_THREAD_SANITIZER)
  set(Y2_SANITIZER_STRING "thread")
elseif(Y2_UB_SANITIZER)
  set(Y2_SANITIZER_STRING "undefined")
elseif(Y2_MEMORY_SANITIZER)
  set(Y2_SANITIZER_STRING "memory")
endif()

# Apply sanitizer options
if(Y2_SANITIZER_STRING AND (CMAKE_CXX_COMPILER_ID MATCHES Clang|GNU))
  set(Y2_COMPILE_FLAGS ${Y2_COMPILE_FLAGS} -fsanitize=${Y2_SANITIZER_STRING} -fno-omit-frame-pointer)
  set(Y2_LINK_FLAGS ${Y2_LINK_FLAGS} -fsanitize=${Y2_SANITIZER_STRING})
endif()

# for Ninja
if(${CMAKE_MAKE_PROGRAM} MATCHES "ninja" )
  set(Y2_COMPILE_FLAGS ${Y2_COMPILE_FLAGS} 
    $<$<CXX_COMPILER_ID:Clang>:-fcolor-diagnostics> 
    $<$<CXX_COMPILER_ID:GNU>:-fdiagnostics-color=always>)
endif()

# ------------------------------------------
# Configuration

add_library(y2-config INTERFACE)
add_library(y2::config ALIAS y2-config)
target_include_directories(y2-config INTERFACE ${Y2_INCLUDE_DIR})
target_compile_options(y2-config INTERFACE ${Y2_COMPILE_FLAGS})
target_link_options(y2-config INTERFACE ${Y2_LINK_FLAGS})
target_link_libraries(y2-config INTERFACE Boost::boost)
target_link_libraries(y2-config INTERFACE Boost::disable_autolinking)
target_link_libraries(y2-config INTERFACE range-v3)
target_link_libraries(y2-config INTERFACE fmt::fmt)

if(WIN32)
  target_compile_definitions(y2-config INTERFACE -DUNICODE -D_UNICODE)
endif()

# ------------------------------------------
# Subdirectories

add_subdirectory(${Y2_SOURCE_DIR})
#add_subdirectory(${Y2_TOOL_DIR})

if(Y2_BUILD_TESTS)
  include (CTest)
  enable_testing()
  add_subdirectory(${Y2_TEST_DIR})
endif()