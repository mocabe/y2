// SPDX-License-Identifier: MIT

#include <y2/ui/memory.hpp>
#include <y2/ui/trackable.hpp>
#include <catch2/catch.hpp>

using namespace y2;
using namespace y2::ui;

TEST_CASE("unique")
{
  SECTION("null")
  {
    auto p = unique<int>(nullptr);
    REQUIRE(!p);
    REQUIRE(!p.get());

    auto q = unique<const int>(std::move(p));
    REQUIRE(!p);
    REQUIRE(!q);

    auto r = std::move(q);
    REQUIRE(!p);
    REQUIRE(!q);
    REQUIRE(!r);
  }

  SECTION("ptr")
  {
    auto p = make_unique<int>(42);
    REQUIRE(p);
    REQUIRE(p.get());
    REQUIRE(*p == 42);
    REQUIRE(*p.get() == 42);

    auto q = std::move(p);
    REQUIRE(!p);
    REQUIRE(q);
    REQUIRE(*q == 42);

    *q = 24;
    REQUIRE(*q == 24);

    auto r = unique<const int>(std::move(q));
    REQUIRE(!q);
    REQUIRE(r);
    REQUIRE(*r == 24);
  }

  SECTION("assign")
  {
    auto p = unique<int>();
    auto q = make_unique<int>(42);

    p = std::move(q);
    REQUIRE(p);
    REQUIRE(!q);
    REQUIRE(*p == 42);
  }

  SECTION("share")
  {
    auto p = make_unique<int>(42);
    auto q = shared(std::move(p));

    REQUIRE(!p);
    REQUIRE(q);
    REQUIRE(*q == 42);
  }

  SECTION("cast static")
  {
    auto p = make_unique<int>(42);
    auto q = std::move(p).cast_static<int>();

    REQUIRE(!p);
    REQUIRE(q);

    REQUIRE(!std::move(p).cast_static<int>());
    REQUIRE(std::move(q).cast_static<int>());
  }

  SECTION("cast dynamic")
  {
    struct Base
    {
      virtual ~Base() = default;
    };

    struct Derived : Base
    {
    };

    auto p = make_unique<Derived>();
    auto q = std::move(p).cast_dynamic<Base>();

    REQUIRE(!p);
    REQUIRE(q);
  }
}

TEST_CASE("shared")
{
  SECTION("null")
  {
    auto p = shared<int>(nullptr);
    REQUIRE(!p);
    REQUIRE(!p.get());

    auto q = shared<const int>(std::move(p));
    REQUIRE(!p);
    REQUIRE(!q);

    auto r = q;
    REQUIRE(!p);
    REQUIRE(!q);
    REQUIRE(!r);
  }

  SECTION("ptr")
  {
    auto p = make_shared<int>(42);
    REQUIRE(p);
    REQUIRE(p.get());
    REQUIRE(*p == 42);
    REQUIRE(*p.get() == 42);

    auto q = p;
    REQUIRE(p);
    REQUIRE(q);
    REQUIRE(*p == 42);
    REQUIRE(*q == 42);

    *q = 24;
    REQUIRE(*p == 24);
    REQUIRE(*q == 24);

    auto r = shared<const int>(std::move(q));
    REQUIRE(p);
    REQUIRE(!q);
    REQUIRE(r);
    REQUIRE(*p == 24);
    REQUIRE(*r == 24);
  }

  SECTION("assign")
  {
    auto p = shared<int>();
    auto q = make_shared<int>(42);

    p = q;
    REQUIRE(p);
    REQUIRE(q);
    REQUIRE(*p == 42);
    REQUIRE(*q == 42);

    q = std::move(p);
    REQUIRE(!p);
    REQUIRE(q);
  }

  SECTION("cast static")
  {
    auto p = make_shared<int>(42);
    auto q = std::move(p).cast_static<int>();

    REQUIRE(!p);
    REQUIRE(q);

    REQUIRE(!std::move(p).cast_static<int>());
    REQUIRE(std::move(q).cast_static<int>());
  }

  SECTION("cast dynamic")
  {
    struct Base
    {
      virtual ~Base() = default;
    };

    struct Derived : Base
    {
    };

    auto p = make_shared<Derived>();
    auto q = p.cast_dynamic<Base>();

    REQUIRE(p);
    REQUIRE(q);

    q = std::move(p).cast_dynamic<Base>();
    REQUIRE(!p);
    REQUIRE(q);
  }
}

TEST_CASE("weak")
{
  struct Foo : trackable
  {
  };

  auto p = make_unique<Foo>();
  auto t = p->track();

  weak ref(*p);
  REQUIRE(!ref.expired());
  REQUIRE(ref.get());

  SECTION("deduction")
  {
    REQUIRE(weak(*p));
    REQUIRE(weak(p));
    REQUIRE(weak(shared(std::move(p))));
  }

  SECTION("apply")
  {
    bool test = false;
    ref.lock([&](auto&) { test = true; });
    REQUIRE(test);

    p = nullptr;
    ref.lock([&](auto&) { test = false; });
    REQUIRE(test);
    REQUIRE(ref.expired());
    REQUIRE(!ref.get());
  }

  SECTION("copy")
  {
    auto ref2 = ref;
    REQUIRE(!ref.expired());
    REQUIRE(!ref2.expired());
    REQUIRE(ref.get());
    REQUIRE(ref2.get());

    p = nullptr;
    REQUIRE(ref.expired());
    REQUIRE(ref2.expired());
    REQUIRE(!ref.get());
    REQUIRE(!ref2.get());
  }

  SECTION("move")
  {
    auto ref2 = std::move(ref);
    REQUIRE(ref.expired());
    REQUIRE(!ref.get());
    REQUIRE(!ref2.expired());
    REQUIRE(ref2.get());
  }

  SECTION("monadic")
  {
    bool test = false;

    if (!ref.lock([&](auto& q) {
          test = true;
          REQUIRE(p.get() == &q);
        }))
      test = false;

    REQUIRE(test);

    if (!ref.lock([&](auto&) { p = nullptr; })) {
      test = false;
    }

    REQUIRE(test);

    ref.lock([&](auto&) { test = false; });

    REQUIRE(test);
  }
}