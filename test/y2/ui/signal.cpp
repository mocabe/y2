// SPDX-License-Identifier: MIT

#include <y2/ui/signal.hpp>
#include <catch2/catch.hpp>

using namespace y2;
using namespace y2::ui;

TEST_CASE("signal")
{
  SECTION("basic")
  {
    signal<void(int)> sig;
    int i = 0;
    connect(sig, [&](auto x) { i = x; });
    sig(42);
    REQUIRE(i == 42);
  }

  SECTION("trackable")
  {
    struct foo : trackable
    {
    };

    auto tracker = make_unique<foo>();

    signal<void(int)> sig;
    int i = 0;
    connect(sig, *tracker, [&](auto x) { i += x; });
    sig(42);
    tracker = nullptr;
    sig(24);
    REQUIRE(i == 42);
  }

  SECTION("trackable")
  {
    struct T1
    {
      signal<void(int)> sig;

      void foo()
      {
        sig(42);
      }
    };

    struct T2 : trackable
    {
      int* i = nullptr;

      void foo(int)
      {
      }

      void init(int& p, T1& t1)
      {
        i = &p;
        connect(t1.sig, *this, [&](auto x) { *i += x; });
        connect(t1.sig, *this, &T2::foo);
      }
    };

    auto t1 = make_unique<T1>();
    auto t2 = make_unique<T2>();

    int i = 0;
    t2->init(i, *t1);

    t1->foo();
    REQUIRE(i == 42);

    t2 = nullptr;
    t1->foo();
    REQUIRE(i == 42);
  }

  SECTION("lock_with")
  {
    struct Foo : trackable
    {
    };

    auto p = make_unique<Foo>();
    auto t = p->track();

    REQUIRE(!t.expired());

    SECTION("")
    {
      bool test = false;
      t.lock([&](auto&) { test = true; });
      REQUIRE(test);

      test = false;
      p    = nullptr;
      t.lock([&](auto&) { test = true; });
      REQUIRE(!test);
    }
  }

  SECTION("combiner")
  {
    SECTION("last_optional")
    {
      struct Foo : trackable
      {
        signal<int(int)> sig;
        int i = 0;

        int foo(int v)
        {
          i += v;
          return i;
        }

        int bar(int v)
        {
          i += v;
          return i;
        }
      };

      auto foo = make_shared<Foo>();
      connect(foo->sig, *foo, &Foo::foo);
      connect(foo->sig, *foo, &Foo::bar);

      auto r1 = foo->sig(2);
      REQUIRE(r1);
      REQUIRE(*r1 == 4);

      foo->sig.disconnect_all();
      REQUIRE(!foo->sig(1));
    }

    SECTION("any_of")
    {
      struct Foo : trackable
      {
        signal<bool(), signal_combiner_any_of> sig;
        int count = 0;

        bool foo()
        {
          ++count;
          return true;
        }

        bool bar()
        {
          ++count;
          return false;
        }
      };

      auto foo = make_shared<Foo>();
      connect(foo->sig, *foo, &Foo::foo);
      connect(foo->sig, *foo, &Foo::bar);

      REQUIRE(foo->sig());
      REQUIRE(foo->count == 1);

      foo->sig.disconnect_all();
      REQUIRE(!foo->sig());

      connect(foo->sig, *foo, &Foo::bar);
      REQUIRE(!foo->sig());
    }
  }
}