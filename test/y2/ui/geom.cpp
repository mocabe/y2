// SPDX-License-Identifier: MIT

#include <y2/ui/vec.hpp>
#include <y2/ui/point.hpp>
#include <y2/ui/size.hpp>
#include <y2/ui/rect.hpp>

#include <catch2/catch.hpp>

using namespace y2;
using namespace y2::ui;

TEST_CASE("vec")
{
  SECTION("")
  {
    vec_px v;
    REQUIRE(v.x == 0);
    REQUIRE(v.x == 0);
  }

  SECTION("+")
  {
    using namespace unit;
    auto v = vec_px(1_px, 2_px);
    auto w = v + 2_px;
    REQUIRE(w.x == 3_px);
    REQUIRE(w.y == 4_px);

    w += unit::pixel(3);
    REQUIRE(w.x == 6);
    REQUIRE(w.y == 7);
  }

  SECTION("-")
  {
    auto v = vec_px(1, 2);
    auto w = v - unit::pixel(1);
    REQUIRE(w.x == 0);
    REQUIRE(w.y == 1);

    w -= unit::pixel(3);
    REQUIRE(w.x == -3);
    REQUIRE(w.y == -2);
  }

  SECTION("/")
  {
    auto v = vec_px(2, 3);
    auto w = v / 2;
    REQUIRE(w.x == 1);
    REQUIRE(w.y == 1.5);

    w /= 0.5;
    REQUIRE(w.x == 2);
    REQUIRE(w.y == 3);
  }

  SECTION("*")
  {
    auto v = vec_px(1, 2);
    auto w = v * 3;
    REQUIRE(w.x == 3);
    REQUIRE(w.y == 6);

    w *= 2;
    REQUIRE(w.x == 6);
    REQUIRE(w.y == 12);
  }
}

TEST_CASE("point_px")
{
  SECTION("")
  {
    point_px p;
    REQUIRE(p.x == 0);
    REQUIRE(p.y == 0);
    REQUIRE(p.vec() == vec_px());
  }

  SECTION("ops")
  {
    auto p = point_px(1, 2);
    REQUIRE(p.vec() == vec_px(1, 2));
    REQUIRE((p + vec_px(2, 3)) == point_px(3, 5));
    REQUIRE((p - vec_px(2, 3)) == point_px(-1, -1));
    REQUIRE((p - point_px(1, 2)) == vec_px());

    REQUIRE(p <= point_px(2, 3));
    REQUIRE(!(p <= point_px(0, 3)));
  }
}

TEST_CASE("size_px")
{
  SECTION("")
  {
    size_px s;
    REQUIRE(s.w == 0);
    REQUIRE(s.h == 0);
  }
}

TEST_CASE("rect_px")
{
  SECTION("")
  {
    rect_px r;
    REQUIRE(r.pos() == point_px());
    REQUIRE(r.size() == size_px());
    REQUIRE(r.valid());
    REQUIRE(r.empty());
  }

  SECTION("")
  {
    auto r = rect_px(point_px(), vec_px(-1, -1));
    REQUIRE(!r.valid());
    REQUIRE(!r.empty());
  }

  SECTION("")
  {
    auto r = rect_px();
    REQUIRE(intersect(r, r).empty());
    REQUIRE(unite(r, r).empty());
  }

  SECTION("")
  {
    auto r1 = rect_px(point_px(), point_px(10, 20));
    auto r2 = rect_px(point_px(5, 5), point_px(20, 10));

    REQUIRE(intersect(r1, r2) == rect_px(point_px(5, 5), point_px(10, 10)));
    REQUIRE(unite(r1, r2) == rect_px(point_px(0, 0), point_px(20, 20)));

    using namespace rect_operators;
    REQUIRE((r1 & r1) == r1);
    REQUIRE((r1 | r1) == r1);
  }

  SECTION("")
  {
    auto r1 = rect_px(point_px(), point_px(10, 20));

    REQUIRE(r1.valid());
    REQUIRE(r1.exists());
    REQUIRE(r1.contains(rect_px {point_px(), point_px(5, 10)}));
    REQUIRE(!r1.contains(rect_px {point_px(), point_px(11, 10)}));
    REQUIRE(!r1.contains(rect_px {point_px(), point_px(10, 21)}));

    REQUIRE(r1.contains(point_px(5, 10)));
    REQUIRE(!r1.contains(point_px(20, 30)));
    REQUIRE(!r1.contains(point_px(-10, 20)));
  }
}