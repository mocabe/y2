// SPDX-License-Identifier: MIT

#include <y2/ui/dynamic_cast.hpp>
#include <catch2/catch.hpp>

using namespace y2;
using namespace y2::ui;

TEST_CASE("enable_dynamic_cast")
{
  struct Base : enable_dynamic_cast<Base>
  {
    virtual ~Base() = default;
  };

  struct Derived : Base
  {
    int i = 42;

    auto Get() const
    {
      return i;
    }
  };

  struct Derived2 : Base
  {
  };

  Derived d {};

  Base& r = d;

  REQUIRE(r.get_as<Derived>());
  REQUIRE(r.as<Derived>().i == 42);
  REQUIRE(r.as<Derived>([](auto& x) { return x.i; }) == 42);

  r.as<Derived>([](auto& x) -> auto& { return x.i; }) = 24;
  REQUIRE(r.as<Derived>([](auto& x) { return x.i; }) == 24);
  REQUIRE(r.as<Derived>(&Derived::Get) == 24);

  REQUIRE(!r.get_as<Derived2>());
  REQUIRE_THROWS(r.as<Derived2>());
  REQUIRE_THROWS(r.as<Derived2>([](auto&) {}));
}