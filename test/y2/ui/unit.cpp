// SPDX-License-Identifier: MIT

#include <y2/ui/unit.hpp>
#include <catch2/catch.hpp>

using namespace y2;
using namespace y2::ui;

TEST_CASE("operators")
{
  using namespace unit;

  SECTION("additive")
  {
    auto x = 1_px;

    static_assert(std::is_same_v<decltype(x + x), unit::pixel>);
    static_assert(std::is_same_v<decltype(x - x), unit::pixel>);
    REQUIRE(static_cast<f64>(x + x) == (static_cast<f64>(x) * 2));
    REQUIRE(static_cast<f64>(x - x) == 0.0);
  }

  SECTION("multiplicative")
  {
    auto x = 1_px;

    static_assert(std::is_same_v<decltype(x * x), decltype(x)>);
    static_assert(std::is_same_v<decltype(x * 1.0), decltype(x)>);
    static_assert(std::is_same_v<decltype(1.0 * x), decltype(x)>);
    static_assert(std::is_same_v<decltype(x / x), decltype(x)>);
    static_assert(std::is_same_v<decltype(x / 1.0), decltype(x)>);

    REQUIRE(static_cast<f64>(x * 3) == (static_cast<f64>(x) * 3));
    REQUIRE(static_cast<f64>(x / 3) == (static_cast<f64>(x) / 3));
    REQUIRE(static_cast<f64>(3 * x) == (3 * static_cast<f64>(x)));
    REQUIRE(x * (2 * x) == (static_cast<f64>(x) * (2 * static_cast<f64>(x))));
  }

  SECTION("partially_ordered")
  {
    auto x = 1_px;
    auto y = 3.14_px;

    REQUIRE(x == x);
    REQUIRE(x == static_cast<f64>(x));
    REQUIRE(static_cast<f64>(x) == x);

    REQUIRE(x < y);
    REQUIRE(static_cast<f64>(x) < y);
    REQUIRE(x < static_cast<f64>(y));
  }

  SECTION("misc")
  {
    auto x = 1_px;
    ++x;
    x--;
    --x;
    x++;
  }
}