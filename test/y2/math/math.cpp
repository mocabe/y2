// SPDX-License-Identifier: MIT

#include <y2/math/math.hpp>
#include <catch2/catch.hpp>

using namespace y2::math;

constexpr auto nan_c = std::numeric_limits<y2::f64>::quiet_NaN();
constexpr auto inf_c = std::numeric_limits<y2::f64>::infinity();

TEST_CASE("trunc")
{
  using y2::math::trunc;
  REQUIRE(trunc(-1.5) == -1.0);
  REQUIRE(trunc(-1.0) == -1.0);
  REQUIRE(trunc(-0.5) == -0.0);
  REQUIRE(trunc(-0.0) == 0.0);
  REQUIRE(trunc(0.0) == 0.0);
  REQUIRE(trunc(0.5) == 0.0);
  REQUIRE(trunc(1.0) == 1.0);
  REQUIRE(trunc(1.5) == 1.0);
  REQUIRE(std::isnan(trunc(nan_c)));
  REQUIRE(trunc(inf_c) == inf_c);
  REQUIRE(trunc(-inf_c) == -inf_c);
}

TEST_CASE("floor")
{
  using y2::math::floor;
  REQUIRE(floor(-1.5) == -2.0);
  REQUIRE(floor(-1.0) == -1.0);
  REQUIRE(floor(-0.5) == -1.0);
  REQUIRE(floor(-0.0) == -0.0);
  REQUIRE(floor(0.0) == 0.0);
  REQUIRE(floor(0.5) == 0.0);
  REQUIRE(floor(1.0) == 1.0);
  REQUIRE(floor(1.5) == 1.0);
  REQUIRE(std::isnan(floor(nan_c)));
  REQUIRE(floor(inf_c) == inf_c);
  REQUIRE(floor(-inf_c) == -inf_c);
}

TEST_CASE("ceil")
{
  using y2::math::ceil;
  REQUIRE(ceil(-1.5) == -1.0);
  REQUIRE(ceil(-1.0) == -1.0);
  REQUIRE(ceil(-0.5) == 0.0);
  REQUIRE(ceil(-0.0) == 0.0);
  REQUIRE(ceil(0.0) == 0.0);
  REQUIRE(ceil(0.5) == 1.0);
  REQUIRE(ceil(1.0) == 1.0);
  REQUIRE(ceil(1.5) == 2.0);
  REQUIRE(std::isnan(floor(nan_c)));
  REQUIRE(floor(inf_c) == inf_c);
  REQUIRE(floor(-inf_c) == -inf_c);
}

TEST_CASE("round_half_up")
{
  REQUIRE(round_half_up(-1.5) == -1.0);
  REQUIRE(round_half_up(-1.25) == -1.0);
  REQUIRE(round_half_up(-1.0) == -1.0);
  REQUIRE(round_half_up(-0.75) == -1.0);
  REQUIRE(round_half_up(-0.5) == 0.0);
  REQUIRE(round_half_up(-0.49999999999999994) == 0.0);
  REQUIRE(round_half_up(-0.25) == 0.0);
  REQUIRE(round_half_up(0.0) == 0.0);
  REQUIRE(round_half_up(-0.0) == 0.0);
  REQUIRE(round_half_up(1.0) == 1.0);
  REQUIRE(round_half_up(0.25) == 0.0);
  REQUIRE(round_half_up(0.49999999999999994) == 0);
  REQUIRE(round_half_up(0.5) == 1.0);
  REQUIRE(round_half_up(0.75) == 1.0);
}