// SPDX-License-Identifier: MIT

#include <y2/core/string.hpp>
#include <y2/core/string_function.hpp>

#include <catch2/catch.hpp>

using namespace y2;

TEST_CASE("string")
{
  SECTION("concepts")
  {
    static_assert(std::contiguous_iterator<string::iterator>);
    static_assert(std::ranges::contiguous_range<string>);
    static_assert(std::ranges::sized_range<string>);
  }

  SECTION("string()")
  {
    auto s = string();
    REQUIRE(s.empty());
    REQUIRE(s.length() == 0);
    REQUIRE(s.begin() == s.end());
    REQUIRE(std::u8string(s.data(), s.length()).empty());
    REQUIRE(s.view().empty());
    REQUIRE(s.span().empty());
    REQUIRE(!s.contains('a'));
    REQUIRE(s.count('a') == 0);
  }

  SECTION("contains")
  {
    auto s = string(u8"abcdefg");
    REQUIRE(s.contains('a'));
    REQUIRE(s.contains('g'));
    REQUIRE(s.contains(string(u8"cde")));
    REQUIRE(s.contains(string_view(u8"defg")));
    REQUIRE(s.contains(string_view(u8"abcdefg")));
    REQUIRE(!s.contains(string_view(u8"xxx")));
    REQUIRE(!s.contains(string_view(u8"abcdefge")));
  }

  SECTION("count")
  {
    auto s = string(u8"aabbccddeeeefff");
    REQUIRE(s.count('a') == 2);
    REQUIRE(s.count('f') == 3);
    REQUIRE(s.count('x') == 0);
    REQUIRE(s.count(string(u8"bbccdd")) == 1);
    REQUIRE(s.count(string_view(u8"ff")) == 1);
    REQUIRE(s.count(string_view(u8"ee")) == 2);
  }

  SECTION("replace")
  {
    auto s = string(u8"a");

    s.replace('a', 'b');
    REQUIRE(s == u8"b");

    s.replace('a', 'x');
    s.replace(string(), 'x');
    s.replace(string(u8"cdefglkjf;saldkfa"), 'x');
    REQUIRE(s == u8"b");

    s.replace(string_view(u8"b"), string(u8"xxx"));
    REQUIRE(s == u8"xxx");
    s.replace(string_view(u8"x"), string(u8"abc"));
    REQUIRE(s == u8"abcabcabc");
  }

  SECTION("split")
  {
    using namespace y2::string_function;

    SECTION("")
    {
      auto s = string(u8"a");
      REQUIRE(split_copy(s, 'a').empty());
      REQUIRE(split_copy(s, 'b').empty());
      REQUIRE(split_copy(s, 'a', 0).size() == 1);
      REQUIRE(split_copy(s, 'a', 0)[0] == u8"a");
    }

    SECTION("")
    {
      auto s = string(u8"abc");
      {
        auto v = split_copy(s, 'a');
        REQUIRE(v.size() == 1);
        REQUIRE(v[0] == u8"bc");
      }
      {
        auto v = split_copy(s, 'b');
        REQUIRE(v.size() == 2);
        REQUIRE(v[0] == u8"a");
        REQUIRE(v[1] == u8'c');
      }
      {
        auto v = split_copy(s, 'c');
        REQUIRE(v.size() == 1);
        REQUIRE(v[0] == u8"ab");
      }
    }

    SECTION("")
    {
      auto s = string(u8"aabb");
      {
        auto v = split_copy(s, 'a');
        REQUIRE(v.size() == 1);
        REQUIRE(v[0] == u8"bb");
      }
      {
        auto v = split_copy(s, 'b');
        REQUIRE(v.size() == 1);
        REQUIRE(v[0] == u8"aa");
      }
      {
        auto v = split_copy(s, 'a', 0);
        REQUIRE(v.size() == 1);
        REQUIRE(v[0] == u8"aabb");
      }
      {
        auto v = split_copy(s, 'a', 1);
        REQUIRE(v.size() == 1);
        REQUIRE(v[0] == u8"abb");
      }
      {
        auto v = split_copy(s, 'b', 0);
        REQUIRE(v.size() == 1);
        REQUIRE(v[0] == u8"aabb");
      }
      {
        auto v = split_copy(s, 'b', 1);
        REQUIRE(v.size() == 2);
        REQUIRE(v[0] == u8"aa");
        REQUIRE(v[1] == u8"b");
      }
    }
  }

  SECTION("append")
  {
    {
      auto s = string(u8"abc");
      s += u8"def";
      REQUIRE(s == u8"abcdef");
    }
    {
      auto s = string(u8"abc");
      s += string_view(u8"def");
      REQUIRE(s == u8"abcdef");
    }
    {
      auto s = string(u8"abc");
      s += string(u8"def");
      REQUIRE(s == u8"abcdef");
    }
    {
      REQUIRE(string(u8"abc") + u8"def" == u8"abcdef");
      REQUIRE(string(u8"abc") + string_view(u8"def") == u8"abcdef");
      REQUIRE(string(u8"abc") + string(u8"def") == u8"abcdef");
      REQUIRE(u8"abc" + string(u8"def") == u8"abcdef");
      REQUIRE(string_view(u8"abc") + string(u8"def") == u8"abcdef");
      REQUIRE(string(u8"abc") + string(u8"def") == u8"abcdef");
    }
  }
}