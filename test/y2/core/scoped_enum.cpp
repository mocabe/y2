// SPDX-License-Identifier: MIT

#include <y2/core/enum_flag.hpp>

#include <catch2/catch.hpp>

namespace y2::test {

  enum class test_flag
  {
    one = 1 << 0,
    two = 1 << 1,
  };

}

Y2_DECL_ENUM_FLAG(y2::test::test_flag);

TEST_CASE("scoped_enum")
{
  using namespace y2::test;
  using namespace y2::scoped_enum_operators;

  SECTION("")
  {
    constexpr auto x = test_flag::one & test_flag::two;
    REQUIRE((x == static_cast<test_flag>(0)));
  }

  SECTION("")
  {
    const auto x = test_flag::one | test_flag::two;
    REQUIRE((x & test_flag::one));
    REQUIRE((x & test_flag::two));
  }

  SECTION("")
  {
    const auto x = ~test_flag::one;
    REQUIRE(!(x & test_flag::one));
    REQUIRE((x & test_flag::two));
  }
}
