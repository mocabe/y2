// SPDX-License-Identifier: MIT

#include <y2/core/variant.hpp>

#include <catch2/catch.hpp>

using namespace y2;

TEST_CASE("variant")
{
  struct Test : variant<int, double>
  {
    void foo()
    {
      visit([](auto& x) { x *= 2; });
      as_variant();
    }
  };

  auto test = Test();
  test.foo();
}