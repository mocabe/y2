# ------------------------------------------
# Testing

set(Y2_TEST_COMPILE_FLAGS ${Y2_COMPILE_FLAGS})
set(Y2_TEST_LINK_FLAGS ${Y2_LINK_FLAGS})

# Catch2 lib
add_library(y2-Catch2 catch.cpp)
target_link_libraries(y2-Catch2 PUBLIC Catch2::Catch2)
target_compile_options(y2-Catch2 PRIVATE ${Y2_TEST_COMPILE_FLAGS})
target_link_options(y2-Catch2 PRIVATE ${Y2_TEST_LINK_FLAGS})
set_target_properties(y2-Catch2 PROPERTIES FOLDER "test")

# add test
function (y2_add_test NAME LABEL)
  set(TARGET test-${LABEL}-${NAME})
  add_executable(${TARGET} ${NAME}.cpp)
  add_test(NAME ${TARGET} COMMAND ${TARGET})
  set_tests_properties(${TARGET} PROPERTIES LABELS ${LABEL})
  target_include_directories(${TARGET} PRIVATE "${Y2_INCLUDE_DIR}")
  target_compile_options(${TARGET} PRIVATE ${Y2_TEST_COMPILE_FLAGS})
  target_link_options(${TARGET} PRIVATE ${Y2_TEST_LINK_FLAGS})
  target_link_libraries(${TARGET} PRIVATE y2-Catch2)
  target_link_libraries(${TARGET} PRIVATE y2::config)
  target_link_libraries(${TARGET} PRIVATE y2::core)
  foreach(lib ${ARGN})
    target_link_libraries(${TARGET} PRIVATE ${lib})
  endforeach()
  string(REPLACE "-" "/" _test_folder "test-${LABEL}")
  set_target_properties(${TARGET} PROPERTIES FOLDER ${_test_folder})
endfunction()

# ------------------------------------------
# Tests

add_subdirectory(y2)