# boost
set(Y2_BOOST_COMPONENTS random)
find_package(Boost COMPONENTS ${Y2_BOOST_COMPONENTS} REQUIRED)

# tl::optional
find_package(tl-optional CONFIG REQUIRED)

# tl::expected
find_package(tl-expected CONFIG REQUIRED)

# fmt
find_package(fmt CONFIG REQUIRED)

# spdlog
find_package(spdlog CONFIG REQUIRED)

# range-v3
find_package(range-v3 CONFIG REQUIRED)

# Catch2
find_package(Catch2 CONFIG REQUIRED)

# submodule packages
#...