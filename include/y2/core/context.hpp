// SPDX-License-Identifier: MIT

#pragma once

#include <y2/core/prelude.hpp>  
#include <y2/core/exception.hpp>
#include <memory>
#include <functional>
#include <mutex>

namespace y2 {

  ///
  /// \brief context wrapper.
  ///
  /// This class is used to manage initialize-terminate lifecycle automatically.
  /// Moved-after state of context is null. Do not call members after move.
  ///
  /// \param T Context type.
  ///
  template <class T>
  class context
  {
    template <class U>
    friend class context;

    std::shared_ptr<T> m_ctx;

    explicit context(std::shared_ptr<T> ctx)
      : m_ctx {std::move(ctx)}
    {
    }

  public:
    ///
    /// \brief Make singleton context from shared context reference.
    ///
    /// \param ctx Shared context pointer.
    ///
    static auto make_by_sharing(std::shared_ptr<T> ctx) -> context
    {
      if (!ctx) {
        throw exception("Invalid pointer to create context");
      }
      return context(std::move(ctx));
    }

    ///
    /// \brief Make singleton context by sharing internal context with reference.
    ///
    /// \param ctx Shared pointer to context managing object.
    /// \param ref Reference to context type.
    ///
    static auto make_by_aliasing(std::shared_ptr<void> ctx, T& ref) -> context
    {
      if (!ctx) {
        throw exception("Invalid pointer to create context");
      }
      return context(std::shared_ptr<T>(ctx, &ref));
    }

  public:
    context()                   = default;
    context(const context&)     = default;
    context(context&&) noexcept = default;
    context& operator=(const context&) = default;
    context& operator=(context&&) noexcept = default;

    ///
    /// \brief Construct from other context.
    ///
    template <class U>
    explicit context(context<U> other)
      : m_ctx {std::move(other.m_ctx)}
    {
    }

    ///
    /// \brief Access context.
    ///
    auto& operator*() const
    {
      if (m_ctx) {
        return *m_ctx;
      }
      throw exception("Invalid context ref");
    }

    ///
    /// \brief Access context.
    ///
    auto operator->() const
    {
      if (m_ctx) {
        return m_ctx.get();
      }
      throw exception("Invalid context ref");
    }
  };

  ///
  /// \brief Helper template class for shared context management.
  /// \param T Context type.
  ///
  template <class T>
  class shared_context_holder
  {
    std::function<std::shared_ptr<T>()> m_cfg;
    std::weak_ptr<T> m_ctx;
    mutable std::mutex m_mtx;

  public:
    ///
    /// \brief Configure context parameters.
    /// \param func Initialization of shared context.
    ///
    template <std::invocable<> Func>
    void configure(Func&& func)
    {
      Y2_ASSERT(!m_cfg);
      auto lck = std::unique_lock(m_mtx);
      m_cfg = std::function([func = std::forward<Func>(func)] {
        return static_cast<std::shared_ptr<T>>(std::invoke(func));
      });
    }

    ///
    /// \brief Check if context is currently active.
    ///
    [[nodiscard]] bool has_context() const noexcept
    {
      auto lck = std::unique_lock(m_mtx);
      return !m_ctx.expired();
    }

    ///
    /// \brief Initialize or get shared context.
    /// \throw y2::exception on missing configure() call.
    /// \throw Any exception raised from initializer of context.
    ///
    [[nodiscard]] auto get_context() -> context<T>
    {
      auto lck = std::unique_lock(m_mtx);
      auto ctx = m_ctx.lock();

      if (!ctx && !m_cfg) {
        throw exception("Missing configuration to initialize context");
      }

      if (!ctx) {
        ctx   = m_cfg();
        m_ctx = ctx;
        m_cfg = nullptr;
      }
      return y2::context<T>::make_by_sharing(ctx);
    }
  };

} // namespace y2