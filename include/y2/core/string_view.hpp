// SPDX-License-Identifier: MIT

#pragma once

#include <y2/core/prelude.hpp>

#include <string_view>

namespace y2 {

  ///
  /// \brief String view class.
  ///
  class string_view
  {
  public:
    using value_type      = char8_t;
    using size_type       = usize;
    using pointer         = value_type*;
    using const_pointer   = const value_type*;
    using reference       = value_type&;
    using const_reference = const value_type&;
    using const_iterator  = const_pointer_iterator<value_type, string_view>;
    using iterator        = const_iterator;

    constexpr string_view() noexcept                   = default;
    constexpr string_view(const string_view&) noexcept = default;
    constexpr string_view(std::u8string_view view);
    constexpr string_view(const char8_t* str);
    constexpr string_view(const char8_t* str, size_type len);

    string_view(nullptr_t) = delete;

    template <std::contiguous_iterator It>
    constexpr string_view(It begin, It end);

    constexpr explicit operator std::u8string_view() const noexcept;

    [[nodiscard]] constexpr auto begin() const -> iterator;
    [[nodiscard]] constexpr auto cbegin() const -> const_iterator;
    [[nodiscard]] constexpr auto end() const -> iterator;
    [[nodiscard]] constexpr auto cend() const -> const_iterator;

    [[nodiscard]] constexpr auto length() const noexcept -> size_type;
    [[nodiscard]] constexpr auto empty() const noexcept -> bool;
    [[nodiscard]] constexpr auto data() const noexcept -> const char8_t*;

    constexpr auto operator[](size_type pos) const noexcept -> const_reference;
    constexpr auto front() const noexcept -> const_reference;
    constexpr auto back() const noexcept -> const_reference;

    constexpr void remove_prefix(size_type n);
    constexpr void remove_suffix(size_type n);

    [[nodiscard]] constexpr bool contains(string_view x) const noexcept;
    [[nodiscard]] constexpr bool contains(char8_t x) const noexcept;

    [[nodiscard]] constexpr auto count(string_view x) const noexcept -> size_type;
    [[nodiscard]] constexpr auto count(char8_t x) const noexcept -> size_type;

    [[nodiscard]] constexpr auto substr(size_type pos, size_type size) const noexcept
      -> string_view;

    [[nodiscard]] constexpr bool starts_with(string_view x) const noexcept;
    [[nodiscard]] constexpr bool starts_with(char8_t x) const noexcept;

    [[nodiscard]] constexpr bool ends_with(string_view x) const noexcept;
    [[nodiscard]] constexpr bool ends_with(char8_t x) const noexcept;

  private:
    std::u8string_view m_view;
  };

  ///
  /// \brief Construct string_view from u8string_view.
  ///
  constexpr string_view::string_view(std::u8string_view view)
    : m_view {view}
  {
  }

  ///
  /// \brief Construct string_view from null-terminated string.
  ///
  constexpr string_view::string_view(const char8_t* str)
    : m_view {str}
  {
  }

  ///
  /// \brief Construct string_view from charactor span.
  ///
  constexpr string_view::string_view(const char8_t* str, size_type len)
    : m_view {str, len}
  {
  }

  ///
  /// \brief Construct string_view from pair of iterators.
  ///
  template <std::contiguous_iterator It>
  constexpr string_view::string_view(It begin, It end)
    : m_view {begin, end}
  {
  }

  ///
  /// \brief Convert string_view to std::u8string_view.
  ///
  constexpr string_view::operator std::u8string_view() const noexcept
  {
    return m_view;
  }

  ///
  /// \brief Get begin iterator.
  ///
  constexpr auto string_view::begin() const -> iterator
  {
    return m_view.data();
  }

  ///
  /// \brief Get begin iterator.
  ///
  constexpr auto string_view::cbegin() const -> const_iterator
  {
    return m_view.data();
  }

  ///
  /// \brief Get end iterator.
  ///
  constexpr auto string_view::end() const -> iterator
  {
    return m_view.data() + m_view.length();
  }

  ///
  /// \brief Get end iterator.
  ///
  constexpr auto string_view::cend() const -> const_iterator
  {
    return m_view.data() + m_view.length();
  }

  ///
  /// \brief Get length of string.
  ///
  constexpr auto string_view::length() const noexcept -> size_type
  {
    return m_view.length();
  }

  ///
  /// \brief Empty string?
  ///
  constexpr auto string_view::empty() const noexcept -> bool
  {
    return m_view.empty();
  }

  ///
  /// \brief Get data.
  ///
  constexpr auto string_view::data() const noexcept -> const char8_t*
  {
    return m_view.data();
  }

  ///
  /// \brief Access data.
  ///
  constexpr auto string_view::operator[](size_type pos) const noexcept -> const_reference
  {
    assert(pos < m_view.size());
    return m_view[pos];
  }

  ///
  /// \brief Access front.
  ///
  constexpr auto string_view::front() const noexcept -> const_reference
  {
    assert(!m_view.empty());
    return m_view.front();
  }

  ///
  /// \brief Access back.
  ///
  constexpr auto string_view::back() const noexcept -> const_reference
  {
    assert(!m_view.empty());
    return m_view.back();
  }

  ///
  /// \brief Remove n prefix chars.
  ///
  constexpr void string_view::remove_prefix(size_type n)
  {
    assert(n <= m_view.size());
    m_view.remove_prefix(n);
  }

  ///
  /// \brief Remove n suffix chars.
  ///
  constexpr void string_view::remove_suffix(size_type n)
  {
    assert(n <= m_view.size());
    m_view.remove_suffix(n);
  }

  ///
  /// \brief Check if this view contains string.
  ///
  constexpr bool string_view::contains(string_view x) const noexcept
  {
    auto i1 = size_type(0);
    auto i2 = size_type(0);

    while (i1 < length() && i2 < x.length()) {
      if (m_view[i1++] == x[i2++]) {
        if (i2 == x.length()) {
          return true;
        }
      } else {
        i2 = 0;
      }
    }
    return false;
  }

  ///
  /// \brief Check if this view contains string.
  ///
  constexpr bool string_view::contains(char8_t x) const noexcept
  {
    return contains(string_view(&x, 1));
  }

  ///
  /// \brief Count string in view.
  ///
  constexpr auto string_view::count(string_view x) const noexcept -> size_type
  {
    auto i1 = size_type(0);
    auto i2 = size_type(0);

    auto c = size_type(0);
    while (i1 < length() && i2 < x.length()) {
      if (m_view[i1++] == x[i2++]) {
        if (i2 == x.length()) {
          ++c;
          i2 = 0;
        }
      } else {
        i2 = 0;
      }
    }
    return c;
  }

  ///
  /// \brief Count string in view.
  ///
  constexpr auto string_view::count(char8_t x) const noexcept -> size_type
  {
    return count(string_view(&x, 1));
  }

  ///
  /// \brief Get substring.
  ///
  constexpr auto string_view::substr(size_type pos, size_type size) const noexcept -> string_view
  {
    const auto view = m_view.substr(pos, size);
    return {view.data(), view.length()};
  }

  ///
  /// \brief Starts with?
  ///
  constexpr bool string_view::starts_with(string_view x) const noexcept
  {
    return m_view.starts_with(x.m_view);
  }

  ///
  /// \brief Starts with?
  ///
  constexpr bool string_view::starts_with(char8_t x) const noexcept
  {
    return m_view.starts_with(x);
  }

  ///
  /// \brief Ends with?
  ///
  constexpr bool string_view::ends_with(string_view x) const noexcept
  {
    return m_view.ends_with(x.m_view);
  }

  ///
  /// \brief Ends with?
  ///
  constexpr bool string_view::ends_with(char8_t x) const noexcept
  {
    return m_view.ends_with(x);
  }

} // namespace y2
