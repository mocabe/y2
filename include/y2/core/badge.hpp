// SPDX-License-Identifier: MIT

#pragma once

#include <y2/core/prelude.hpp>

namespace y2 {

  ///
  /// \brief "PassKey" idiom helper.
  ///
  /// This class can only be created from T.
  /// You can use this class to achieve fine-grained access control.
  ///
  template <class T>
  class badge
  {
    friend T;
    badge() = default;
  };

} // namespace y2