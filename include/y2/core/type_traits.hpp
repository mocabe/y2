// SPDX-License-Identifier: MIT

#pragma once

#include <y2/core/prelude.hpp>
#include <type_traits>

namespace y2 {

  // ------------------------------------------
  // false_v

  template <class...>
  struct make_false
  {
    static constexpr bool value = false;
  };

  /// Always returns false
  template <class... Ts>
  static constexpr bool false_v = make_false<Ts...>::value;

  // ------------------------------------------
  // is_complete_v

  /// Detect complete type
  template <class T, auto _x = []() -> void {}>
  concept is_complete_v = requires
  {
    sizeof(T);
    _x;
  };

  // ------------------------------------------
  // inherit_const

  /// Conditionally add const to T depending on constness of U
  template <class T, class U>
  using inherit_const_t =
    std::conditional_t<std::is_const_v<U>, std::add_const_t<T>, T>;

  // ------------------------------------------
  // arithmetic

  template <class T>
  concept arithmetic = std::is_arithmetic_v<T>;

} // namespace y2