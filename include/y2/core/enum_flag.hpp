// SPDX-License-Identifier: MIT

#pragma once

#include <y2/core/prelude.hpp>
#include <y2/core/to_underlying.hpp>
#include <type_traits>

///
/// Macro for define specialization of enum flags.
///
#define Y2_DECL_ENUM_FLAG(TYPE)                       \
  namespace y2::scoped_enum_operators {               \
    template <>                                       \
    struct is_scoped_enum_flag<TYPE> : std::true_type \
    {                                                 \
    };                                                \
  }

///
/// Namespace for scoped enum flag operators.
///
namespace y2::scoped_enum_operators {

  ///
  /// Trait class to use SFINAE on enum class types.
  ///
  template <class T>
  struct is_scoped_enum_flag : std::false_type
  {
  };

  ///
  /// Scoped enum flag concept.
  ///
  template <class T, auto _x = []() -> void {}>
  concept scoped_enum_flag = requires
  {
    is_scoped_enum_flag<T>::value == true;
    _x;
  };

  ///
  /// operator| for enum class
  ///
  template <scoped_enum_flag Flag>
  [[nodiscard]] constexpr Flag operator|(Flag lhs, Flag rhs)
  {
    return static_cast<Flag>(to_underlying(lhs) | to_underlying(rhs));
  }

  ///
  /// operator|= for enum class
  ///
  template <scoped_enum_flag Flag>
  constexpr Flag& operator|=(Flag& lhs, Flag rhs)
  {
    lhs = lhs | rhs;
    return lhs;
  }

  ///
  /// operator& for enum class
  ///
  template <scoped_enum_flag Flag>
  [[nodiscard]] constexpr Flag operator&(Flag lhs, Flag rhs)
  {
    return static_cast<Flag>(to_underlying(lhs) & to_underlying(rhs));
  }

  ///
  /// operator&= for enum class
  ///
  template <scoped_enum_flag Flag>
  constexpr Flag& operator&=(Flag& lhs, Flag rhs)
  {
    lhs = lhs & rhs;
    return lhs;
  }

  ///
  /// operator^ for enum class
  ///
  template <scoped_enum_flag Flag>
  [[nodiscard]] constexpr Flag operator^(Flag lhs, Flag rhs)
  {
    return static_cast<Flag>(to_underlying(lhs) ^ to_underlying(rhs));
  }

  ///
  /// operator^= for enum class
  ///
  template <scoped_enum_flag Flag>
  constexpr Flag& operator^=(Flag& lhs, Flag rhs)
  {
    lhs = lhs ^ rhs;
    return lhs;
  }

  ///
  /// operator~ for enum class
  ///
  template <scoped_enum_flag Flag>
  [[nodiscard]] constexpr Flag operator~(Flag flag)
  {
    return static_cast<Flag>(~(to_underlying(flag)));
  }

  ///
  /// operator!
  ///
  template <scoped_enum_flag Flag>
  [[nodiscard]] constexpr bool operator!(Flag flag)
  {
    return !(to_underlying(flag));
  }

} // namespace y2::scoped_enum_operators

namespace y2 {

  ///
  /// Test scoped enum flag.
  ///
  template <scoped_enum_operators::scoped_enum_flag Flag>
  [[nodiscard]] constexpr bool contains(Flag set, Flag test)
  {
    using namespace scoped_enum_operators;
    return !!(set & test);
  }
}