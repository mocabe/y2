// SPDX-License-Identifier: MIT

#pragma once

#include <cstddef>
#include <cstdint>
#include <climits>
#include <limits>
#include <cassert>

// unreachable
#if defined(__GNUC__)
#  define Y2_UNREACHABLE()     \
    do {                       \
      Y2_ASSERT(false);        \
      __builtin_unreachable(); \
    } while (0)
#elif defined(_MSC_VER)
#  define Y2_UNREACHABLE() \
    do {                   \
      Y2_ASSERT(false);    \
      __assume(0);         \
    } while (0)
#else
#  define Y2_UNREACHABLE() Y2_ASSERT(false)
#endif

// unused
#define Y2_UNUSED(x) (void)x

/// \brief Assert function
///
/// Calls assert(e) for debug builds, otherwise calls std::abort().
/// Use assert(e) for performance critical codes.
#define Y2_ASSERT(e)       \
  do {                     \
    if (!e) [[unlikely]] { \
      assert(false);       \
      std::abort();        \
    }                      \
  } while (0)

// win
#if defined(_WIN32) || defined(_WIN64)
#  define Y2_PLATFORM_WINDOWS
#endif

// linux
#if defined(__linux)
#  define Y2_PLATFORM_LINUX
#endif

// unix
#if defined(__unix)
#  define Y2_PLATFORM_UNIX
#endif

// posix
#if defined(__posix)
#  define Y2_PLATFORM_POSIX
#endif

// apple (osx)
#if defined(__APPLE__)
#  define Y2_PLATFORM_OSX
#endif

namespace y2 {

  using u8  = std::uint8_t;
  using u16 = std::uint16_t;
  using u32 = std::uint32_t;
  using u64 = std::uint64_t;

  using s8  = std::int8_t;
  using s16 = std::int16_t;
  using s32 = std::int32_t;
  using s64 = std::int64_t;

  using f32 = float;
  using f64 = double;

  using ssize = s64;
  using usize = u64;

  static_assert(CHAR_BIT == 8);
  static_assert(sizeof(char) == sizeof(s8));
  static_assert(sizeof(unsigned char) == sizeof(u8));
  static_assert(sizeof(bool) == sizeof(char));
  static_assert(alignof(bool) == alignof(char));

  static_assert(sizeof(float) == 4);
  static_assert(sizeof(double) == 8);
  static_assert(std::numeric_limits<float>::is_iec559);
  static_assert(std::numeric_limits<double>::is_iec559);

  // cstddef
  using nullptr_t = std::nullptr_t;
  using size_t = std::size_t;
  using ptrdiff_t = std::ptrdiff_t;
  using max_align_t = std::max_align_t;
  using byte = std::byte;

  /// false when NDEBUG is defined, otherwise true
  inline constexpr bool is_debug
#if defined(NDEBUG)
    = false;
#else
    = true;
#endif

} // namespace y2