
#pragma once

#include <y2/core/prelude.hpp>

#include <variant>

namespace y2 {

  ///
  /// \brief Variant type.
  ///
  template <class... Ts>
  struct variant : std::variant<Ts...>
  {
    using variant_type = std::variant<Ts...>;

    using variant_type::variant_type;

    auto& as_variant() noexcept
    {
      return static_cast<variant_type&>(*this);
    }

    auto& as_variant() const noexcept
    {
      return static_cast<const variant_type&>(*this);
    }

    template <class F>
    decltype(auto) visit(F&& f)
    {
      return std::visit(std::forward<F>(f), as_variant());
    }

    template <class F>
    decltype(auto) visit(F&& f) const
    {
      return std::visit(std::forward<F>(f), as_variant());
    }

    template <class T>
    [[nodiscard]] auto& get()
    {
      return std::get<T>(as_variant());
    }

    template <class T>
    [[nodiscard]] auto& get() const
    {
      return std::get<T>(as_variant());
    }

    template <class T>
    [[nodiscard]] auto* get_as()
    {
      return std::get_if<T>(&as_variant());
    }

    template <class T>
    [[nodiscard]] auto* get_as() const
    {
      return std::get_if<T>(&as_variant());
    }

    template <class T>
    [[nodiscard]] bool is() const
    {
      return static_cast<bool>(get_as<T>());
    }
  };

} // namespace y2