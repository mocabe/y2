// SPDX-License-Identifier: MIT

#pragma once

#include <y2/core/prelude.hpp>
#include <source_location>
#include <stdexcept>

namespace y2 {

  ///
  /// \brief General exception class.
  ///
  /// Avoid adding new exception subclasses unless you really need to.
  ///
  class exception : public std::runtime_error
  {
    std::source_location m_loc;

  public:
    exception(const char* str, std::source_location loc = std::source_location::current());

    [[nodiscard]] auto u8_what() const -> const char8_t*;
    [[nodiscard]] auto source_location() const;
  };

} // namespace y2