// SPDX-License-Identifier: MIT

#pragma once

#include <y2/core/prelude.hpp>
#include <y2/core/string.hpp>

namespace y2::string_function {

  [[nodiscard]] auto split_copy(string_view str, char8_t delim, usize max_split = usize(-1))
    -> std::vector<string>;

  [[nodiscard]] auto split_view(string_view str, char8_t delim, usize max_split = usize(-1))
    -> std::vector<string_view>;

} // namespace y2::string_function
