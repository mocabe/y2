// SPDX-License-Identifier: MIT

#pragma once

#include <y2/core/prelude.hpp
#include <y2/core/uid.hpp>

#include <string>
#include <iosfwd>

namespace y2 {

  namespace uid_detail {

    // id to_string
    auto uid_to_string(u64 id) -> std::string;

  } // namespace uid_detail

  /// Convert uid to string
  template <class T>
  [[nodiscard]] std::string to_string(const uid<T>& id)
  {
    return uid_detail::uid_to_string(id.int_value());
  }

  /// iostream support for uid
  template <class T>
  auto operator<<(std::ostream& os, const uid<T>& id) -> std::ostream&
  {
    os << id.int_value();
    return os;
  }

} // namespace y2