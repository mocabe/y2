// SPDX-License-Identifier: MIT

#pragma once

#include <y2/core/prelude.hpp>

namespace y2 {

  namespace detail {

    template <typename T1, typename T2>
    struct offset_of_impl
    {
      union U
      {
        U()
          : c {}
        {
        }
        ~U()
        {
        }
        char c[sizeof(T2)];
        T2 o;
      };
      static U u;
      static constexpr size_t get(T1 T2::*member)
      {
        size_t i = 0;
        for (; i < sizeof(T2); ++i)
          if (((void*)&(u.c[i])) == &(u.o.*member))
            break;

        // g++ bug 67371 workaround
        if (i >= sizeof(T2))
          throw;
        else
          return i;
      }
    };

    template <class T1, class T2>
    typename offset_of_impl<T1, T2>::U offset_of_impl<T1, T2>::u {};

  } // namespace detail

  /// Get offset of member at compile time.
  /// i.e. constexpr offsetof.
  template <class T1, class T2>
  [[nodiscard]] consteval size_t offset_of(T1 T2::*member)
  {
    return detail::offset_of_impl<T1, T2>::get(member);
  }

} // namespace y2
