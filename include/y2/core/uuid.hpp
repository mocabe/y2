// SPDX-License-Identifier: MIT

#pragma once

#include <y2/core/prelude.hpp>

#include <string_view>
#include <array>

namespace y2 {

  /// UUID variant
  enum class uuid_variant
  {
    ncs,       // NCS backward compatibility
    rfc_4122,  // defined in RFC 4122 document
    microsoft, // Microsoft Corporation backward compatibility
    future     // future definition
  };

  /// UUID version
  enum class uuid_version
  {
    unknown = 0,
    time_based = 1,
    dce_security = 2,
    name_based_md5 = 3,
    random_number_based = 4,
    name_based_sha1 = 5
  };

  /// 128bit UUID datatype
  class uuid
  {
    std::array<u8, 16> m_data = {0};

    constexpr uuid(const std::array<u8, 16> d)
      : m_data {d}
    {
    }

    constexpr uuid() = default;

  public:
    [[nodiscard]] static uuid make_zero();
    [[nodiscard]] static uuid make_random();
    [[nodiscard]] static uuid make_from_string(std::string_view str);
    [[nodiscard]] static constexpr uuid make_from_array(const char (&str)[37]);
    [[nodiscard]] static constexpr auto size() -> size_t
    {
      return 16;
    }

  public:
    constexpr uuid(const uuid&) = default;
    constexpr uuid& operator=(const uuid&) = default;

  public:
    [[nodiscard]] bool is_zero() const noexcept;
    [[nodiscard]] auto variant() const noexcept -> uuid_variant;
    [[nodiscard]] auto version() const noexcept -> uuid_version;

    auto& data() const
    {
      return m_data;
    }
    auto& data()
    {
      return m_data;
    }
  };

  namespace uuid_detail {

    constexpr auto make_from_string_static(const char (&str)[37])
      -> std::array<u8, 16>
    {
      char hex[32] {};
      size_t hex_idx = 0;

      // for gcc constexpr bug workaround
      bool fail = false;

      // read hex
      for (auto&& c : str) {
        if (c == '-' || c == '\0') {
          continue;
        }
        if ('0' <= c && c <= '9') {
          hex[hex_idx] = static_cast<char>(c - '0');
          ++hex_idx;
          continue;
        }
        if ('a' <= c && c <= 'f') {
          hex[hex_idx] = static_cast<char>(c - 'a' + 10);
          ++hex_idx;
          continue;
        }
        if ('A' <= c && c <= 'F') {
          hex[hex_idx] = static_cast<char>(c - 'A' + 10);
          ++hex_idx;
          continue;
        }
        fail = true;
        break;
      }

      if (fail)
        throw; // compile time error

      std::array<u8, 16> ret {};

      // convert to values
      for (size_t i = 0; i < 16; ++i) {
        auto upper = 2 * i;
        auto lower = 2 * i + 1;
        ret[i] = static_cast<char>(hex[upper] * 16 + hex[lower]);
      }
      return ret;
    }

    auto make_from_string_dynamic(const char (&str)[37]) -> std::array<u8, 16>;

  } // namespace uuid_detail

  constexpr uuid uuid::make_from_array(const char (&str)[37])
  {
    if (std::is_constant_evaluated())
      return {uuid_detail::make_from_string_static(str)};
    else
      return {uuid_detail::make_from_string_dynamic(str)};
  }

} // namespace  y2
