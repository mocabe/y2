// SPDX-License-Identifier: MIT

#pragma once

#include <y2/core/prelude.hpp>
#include <type_traits>

namespace y2 {

  ///
  /// \brief Convert enum class to underlying type.
  ///
  template <class E>
  [[nodiscard]] constexpr auto to_underlying(E e)
  {
    return static_cast<std::underlying_type_t<E>>(e);
  }

} // namespace y2