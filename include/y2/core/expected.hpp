// SPDX-License-Identifier: MIT

#pragma once

#include <y2/core/prelude.hpp>
#include <tl/expected.hpp>

namespace y2 {

  /// Expected type
  template <class T, class E>
  using expected = tl::expected<T, E>;
}