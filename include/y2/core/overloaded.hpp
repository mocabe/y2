// SPDX-License-Identifier: MIT

#pragma once

#include <y2/core/prelude.hpp>

namespace y2 {

  /// Helper template for std::visit
  template <class... Ts>
  struct overloaded : Ts...
  {
    using Ts::operator()...;
  };

  template <class... Ts>
  overloaded(Ts...) -> overloaded<Ts...>;

} // namespace y2