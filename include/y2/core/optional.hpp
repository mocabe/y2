// SPDX-License-Identifier: MIT

#pragma once

#include <y2/core/prelude.hpp>
#include <tl/optional.hpp>

namespace y2 {

  /// Optional type
  template <class T>
  using optional = tl::optional<T>;

  /// nullopt_t
  using nullopt_t = tl::nullopt_t;

  /// nullopt
  inline constexpr auto nullopt = tl::nullopt;
}