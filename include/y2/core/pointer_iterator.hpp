// SPDX-License-Identifier: MIT

#pragma once

#include <y2/core/prelude.hpp>
#include <boost/stl_interfaces/iterator_interface.hpp>

#include <type_traits>

namespace y2 {

  /// Contiguous iterator.
  template <class ValueType, class TagType>
  struct pointer_iterator : boost::stl_interfaces::iterator_interface<
                              pointer_iterator<ValueType, TagType>,
                              std::contiguous_iterator_tag,
                              ValueType>
  {
    constexpr pointer_iterator() noexcept = default;

    constexpr pointer_iterator(ValueType* it) noexcept
      : m_it(it)
    {
    }

    template <typename T, class U>
    requires std::is_convertible_v<T*, ValueType*>
    constexpr pointer_iterator(pointer_iterator<T, U> other) noexcept
      : m_it(other.m_it)
    {
    }

    constexpr auto& operator*() const noexcept
    {
      return *m_it;
    }

    constexpr auto& operator+=(std::ptrdiff_t i) noexcept
    {
      m_it += i;
      return *this;
    }

    constexpr auto operator-(pointer_iterator other) const noexcept
    {
      return m_it - other.m_it;
    }

    ValueType* m_it = nullptr;
  };

  /// Contiguous iterator.
  template <class ValueType, class TagType>
  using const_pointer_iterator =
    pointer_iterator<std::add_const_t<ValueType>, TagType>;

} // namespace y2