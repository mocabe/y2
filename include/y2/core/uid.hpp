// SPDX-License-Identifier: MIT

#pragma once

#include <y2/core/prelude.hpp>
#include <compare>

namespace y2 {

  ///
  /// \brief Tagged 64bit integer ID type.
  /// \param T tag type.
  ///
  template <class T = void>
  class uid
  {
    u64 m_data = 0;

  public:
    /// Tag type
    using tag_type = T;
    /// Integer type
    using int_type = u64;

  public:
    [[nodiscard]] static auto make_zero() -> uid;
    [[nodiscard]] static auto make_random() -> uid;
    [[nodiscard]] static auto make_from_uint(int_type v) -> uid;

  public:
    uid()           = default;
    uid(const uid&) = default;
    uid& operator=(const uid&) = default;

    explicit uid(u64 data)
      : m_data {data}
    {
    }

    [[nodiscard]] auto int_value() const
    {
      return m_data;
    }

    [[nodiscard]] auto operator<=>(const uid&) const = default;
  };

  namespace uid_detail {

    auto make_random_id() -> u64;

  } // namespace uid_detail

  template <class T>
  auto uid<T>::make_zero() -> uid
  {
    return {};
  }

  template <class T>
  auto uid<T>::make_random() -> uid
  {
    return {uid_detail::make_random_id()};
  }

  template <class T>
  auto uid<T>::make_from_uint(int_type v) -> uid
  {
    return {v};
  }

} // namespace y2