// SPDX-License-Identifier: MIT

#pragma once

#include <y2/core/prelude.hpp>
#include <y2/core/uuid.hpp>

#include <iosfwd>
#include <string>

namespace y2 {

  /// Stringify
  [[nodiscard]] auto to_string(const uuid& id) -> std::string;

  /// Stream output
  auto operator<<(std::ostream& os, const uuid& id) -> std::ostream&;

} // namespace y2