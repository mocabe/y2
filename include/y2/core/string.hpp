// SPDX-License-Identifier: MIT

#pragma once

#include <y2/core/prelude.hpp>
#include <y2/core/pointer_iterator.hpp>
#include <y2/core/string_view.hpp>

#include <fmt/format.h>

#include <memory_resource>
#include <string>
#include <string_view>
#include <concepts>
#include <span>
#include <vector>
#include <iterator>

namespace y2 {

  /// String type.
  class string
  {
  public:
    /// Make from UTF8 encoded string.
    static auto from_utf8_unchecked(std::string_view view) -> string;

  public:
    using value_type      = char8_t;
    using size_type       = usize;
    using reference       = const value_type&;
    using const_reference = const value_type&;
    using iterator        = const_pointer_iterator<value_type, string>;
    using const_iterator  = const_pointer_iterator<value_type, string>;
    using allocator_type  = std::pmr::polymorphic_allocator<char8_t>;

  public:
    string() noexcept;
    string(char8_t c);
    string(const char8_t* cstr);
    string(std::u8string_view view);
    string(string_view view);
    string(const string& other)     = default;
    string(string&& other) noexcept = default;
    string& operator=(const string& other) = default;
    string& operator=(string&& other) noexcept = default;

    explicit string(const allocator_type& alloc) noexcept;

    template <std::input_iterator Iterator>
    string(Iterator begin, Iterator end)
      : string(string_view(begin, end))
    {
    }

    [[nodiscard]] auto operator<=>(const string&) const = default;

    [[nodiscard]] auto begin() const -> const_iterator;
    [[nodiscard]] auto end() const -> const_iterator;
    [[nodiscard]] auto cbegin() const -> const_iterator;
    [[nodiscard]] auto cend() const -> const_iterator;

    [[nodiscard]] auto length() const noexcept -> size_type;
    [[nodiscard]] auto capacity() const noexcept -> size_type;
    [[nodiscard]] bool empty() const noexcept;

    [[nodiscard]] auto data() const noexcept -> const char8_t*;
    [[nodiscard]] auto view() const noexcept -> string_view;
    [[nodiscard]] auto span() const noexcept -> std::span<const char8_t>;

    [[nodiscard]] bool contains(string_view x) const noexcept;
    [[nodiscard]] bool contains(char8_t x) const noexcept;

    [[nodiscard]] auto count(string_view x) const noexcept -> size_type;
    [[nodiscard]] auto count(char8_t x) const noexcept -> size_type;

    [[nodiscard]] bool starts_with(string_view x) const noexcept;
    [[nodiscard]] bool starts_with(char8_t x) const noexcept;

    [[nodiscard]] bool ends_with(string_view x) const noexcept;
    [[nodiscard]] bool ends_with(char8_t x) const noexcept;

    void resize(size_type length);
    void reserve(size_type length);
    void clear();

    void replace(string_view x, string_view y);
    void replace(char8_t x, char8_t y);
    void replace(char8_t x, string_view y);
    void replace(string_view x, char8_t y);

    void append(const char8_t* x);
    void append(string_view x);
    void append(const string& x);
    void append(char8_t x);

    template <std::input_iterator Iterator>
    void insert(const_iterator pos, Iterator first, Iterator last)
    {
      m_str.insert(m_str.cbegin() + std::distance(cbegin(), pos), first, last);
    }

    void erase(const_iterator p);
    void erase(const_iterator first, const_iterator last);

    operator string_view() const noexcept;

    auto operator[](size_type pos) const -> const_reference;

    auto operator+=(const char8_t* view) -> string&;
    auto operator+=(const string& other) -> string&;
    auto operator+=(char8_t c) -> string&;
    auto operator+=(string_view view) -> string&;

  private:
    std::pmr::u8string m_str;
  };

  [[nodiscard]] auto operator+(const string& lhs, const string& rhs) -> string;
  [[nodiscard]] auto operator+(string&& lhs, const string& rhs) -> string;
  [[nodiscard]] auto operator+(const string& lhs, string&& rhs) -> string;
  [[nodiscard]] auto operator+(string&& lhs, string&& rhs) -> string;

} // namespace y2

template <>
struct fmt::formatter<y2::string> : fmt::formatter<std::u8string_view>
{
  template <class FormatCtx>
  auto format(const y2::string& a, FormatCtx& ctx)
  {
    return format(std::u8string_view(a.data(), a.length()), ctx);
  }
};
