// SPDX-License-Identifier: MIT

#pragma once

#include <y2/core/prelude.hpp>
#include <cmath>

namespace y2::math {

  ///
  /// \brief Truncate function.
  ///
  template <std::floating_point T>
  [[nodiscard]] auto trunc(T a) -> T
  {
    if (!std::isfinite(a)) {
      return a;
    }
    return static_cast<T>(static_cast<s64>(a));
  }

  ///
  /// \brief Floor function.
  ///
  template <std::floating_point T>
  [[nodiscard]] auto floor(T a) -> T
  {
    if (!std::isfinite(a)) {
      return a;
    }
    const auto b = math::trunc(a);
    return ((a - b) < static_cast<T>(0.0)) ? b - static_cast<T>(1.0) : b;
  }

  ///
  /// \brief Ceil function.
  ///
  template <std::floating_point T>
  [[nodiscard]] auto ceil(T a) -> T
  {
    if (!std::isfinite(a)) {
      return a;
    }
    const auto b = math::trunc(a);
    return ((a - b) <= static_cast<T>(0.0)) ? b : b + static_cast<T>(1.0);
  }

  ///
  /// \brief Round half up.
  ///
  template <std::floating_point T>
  [[nodiscard]] auto round_half_up(T a) -> T
  {
    if (!std::isfinite(a)) {
      return a;
    }
    const auto b = math::floor(a);
    if ((a - b) < static_cast<T>(0.5)) {
      return b;
    }
    return math::ceil(a);
  }

  ///
  /// \brief Round half down.
  ///
  template <std::floating_point T>
  [[nodiscard]] auto round_half_down(T a) -> T
  {
    if (!std::isfinite(a)) {
      return a;
    }
    const auto b = math::floor(a);
    if ((a - b) <= static_cast<T>(0.5)) {
      return b;
    }
    return math::ceil(a);
  }

} // namespace y2::math