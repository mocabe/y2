// SPDX-License-Identifier: MIT

#pragma once

#include <y2/ui/event.hpp>

namespace y2::ui {

  ///
  /// \brief Phase scope for event.
  ///
  class event_phase_scope
  {
    event& m_event;
    event_phase m_old_phase = event_phase::none;

  public:
    event_phase_scope(event& event, event_phase phase);
    event_phase_scope(const event_phase_scope&) = delete;
    ~event_phase_scope() noexcept;
  };

} // namespace y2::ui