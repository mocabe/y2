// SPDX-License-Identifier: MIT

#pragma once

#include <y2/ui/event_fwd.hpp>
#include <y2/ui/event_phase.hpp>
#include <y2/ui/dynamic_cast.hpp>
#include <y2/ui/trackable.hpp>
#include <y2/ui/memory.hpp>
#include <y2/core/prelude.hpp>

#include <memory>
#include <typeinfo>

namespace y2::ui {

  ///
  /// \brief Base class of events.
  ///
  class event : public enable_dynamic_cast<event>
  {
    event_phase m_phase;
    event_phase m_current_phase;

  public:
    event(event_phase phase);
    virtual ~event() noexcept = default;

    auto phase() const -> event_phase;

    bool dispatching() const;

    auto current_phase() const -> event_phase;
    void set_current_phase(event_phase phase);

  protected:
    void set_phase(event_phase p);
  };

} // namespace y2::ui
