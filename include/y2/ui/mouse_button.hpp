// SPDX-License-Identifier: MIT

#pragma once

#include <y2/core/enum_flag.hpp>

namespace y2::ui
{
  /// Mouse button enum.
  enum class mouse_button : int
  {
    button_1,
    button_2,
    button_3,
    button_4,
    button_5,
    button_6,
    button_7,
    button_8,
    left,
    right,
    middle,
  };

  /// Button state.
  enum class button_state
  {
    up,
    down,
  };

  /// Button action.
  enum class button_action
  {
    release,
    press,
  };

} // namespace y2::ui