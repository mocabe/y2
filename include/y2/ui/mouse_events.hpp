// SPDX-License-Identifier: MIT

#pragma once

#include <y2/ui/key.hpp>
#include <y2/ui/event.hpp>
#include <y2/ui/memory.hpp>
#include <y2/ui/mouse_button.hpp>
#include <y2/ui/vec.hpp>

namespace y2::ui::events {

  /// Mouse event base.
  class mouse_event : public event
  {
  public:
    mouse_event(event_phase phase, mouse_button button, vec_px pos);

    /// position of mouse in window coordinate
    auto pos() const -> vec_px;

    /// event source button
    auto button() const -> mouse_button;

  protected:
    mouse_button m_button;
    vec_px m_pos;
  };

  /// Mouse click event.
  /// \note propagate = yes
  class mouse_click final : public mouse_event
  {
  public:
    mouse_click(event_phase phase, mouse_button button, vec_px pos);
  };

  /// Mouse double click event.
  /// \note propagate = yes
  class mouse_double_click final : public mouse_event
  {
  public:
    mouse_double_click(event_phase phase, mouse_button button, vec_px pos);
  };

  /// Mouse press event.
  /// \note propagate = yes
  class mouse_press final : public mouse_event
  {
  public:
    mouse_press(event_phase phase, mouse_button button, vec_px pos);
  };

  /// Mouse release event.
  /// \note propagate = yes
  class mouse_release final : public mouse_event
  {
  public:
    mouse_release(event_phase phase, mouse_button button, vec_px pos);
  };

  /// Mouse repeat event.
  /// \note propagate = yes
  class mouse_repeat final : public mouse_event
  {
  public:
    mouse_repeat(event_phase phase, mouse_button button, vec_px pos);
  };

  /// Mouse moves over view.
  /// \note propagate: yes
  class mouse_move final : public mouse_event
  {
    vec_px m_delta;

  public:
    mouse_move(event_phase phase, vec_px pos, vec_px delta);

    /// delta
    auto delta() const -> vec_px;
  };

  /// Mouse move into view or its child.
  /// \note propagate: yes
  class mouse_over final : public mouse_event
  {
  public:
    mouse_over(event_phase phase, vec_px pos);
  };

  /// Mouse move out from view or its child.
  /// \note propagate: yes
  class mouse_out final : public mouse_event
  {
  public:
    mouse_out(event_phase phase, vec_px pos);
  };

  /// Mosue move into region of view.
  /// \note propagate: no
  class mouse_enter final : public mouse_event
  {
  public:
    mouse_enter(event_phase phase, vec_px pos);
  };

  /// Mosue move out from region of view.
  /// \note propagate: no
  class mouse_leave final : public mouse_event
  {
  public:
    mouse_leave(event_phase phase, vec_px pos);
  };

} // namespace y2::ui::events