// SPDX-License-Identifier: MIT

#pragma once

#include <y2/ui/vec.hpp>
#include <y2/ui/unit.hpp>

#include <compare>

namespace y2::ui {

  template <class T>
  struct point
  {
    T x = {};
    T y = {};

    constexpr point() = default;

    template <class U>
    explicit constexpr point(U s)
      : x {s}
      , y {s}
    {
    }

    template <class U>
    constexpr point(U x, U y)
      : x {x}
      , y {y}
    {
    }

    constexpr point(const vec<T>& v)
      : x {v.x}
      , y {v.y}
    {
    }

    constexpr auto operator+=(const vec<T>& o) -> point&
    {
      x += o.x;
      y += o.y;
      return *this;
    }

    constexpr auto operator-=(const vec<T>& o) -> point&
    {
      x -= o.x;
      y -= o.y;
      return *this;
    }

    constexpr auto vec() -> vec<T> const
    {
      return {x, y};
    }

    constexpr bool operator==(const point&) const = default;
    constexpr bool operator!=(const point&) const = default;

    constexpr auto operator<=>(const point&) const -> std::partial_ordering = default;
  };

  template <class T>
  constexpr auto operator+(const point<T>& p, const vec<T> o) -> point<T>
  {
    auto r = p;
    r += o;
    return r;
  }

  template <class T>
  constexpr auto operator-(const point<T>& p, const vec<T> o) -> point<T>
  {
    auto r = p;
    r -= o;
    return r;
  }

  template <class T>
  constexpr auto operator-(const point<T>& p1, const point<T>& p2) -> vec<T>
  {
    return vec<T>(p1.x - p2.x, p1.y - p2.y);
  }

  template <class U>
  point(U) -> point<U>;

  template <class U>
  point(U, U) -> point<U>;

  using point_pt = point<unit::point>;
  using point_px = point<unit::pixel>;
  using point_lp = point<unit::logical_pixel>;
  using point_bp = point<unit::buffer_pixel>;

} // namespace y2::ui