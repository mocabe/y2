// SPDX-License-Identifier: MIT

#pragma once

#include <y2/ui/signal_fwd.hpp>
#include <boost/signals2.hpp>

namespace y2::ui {

  /// Scoped connection type
  class connection
  {
    boost::signals2::scoped_connection m_c;

    template <class Signature, template <class> class Combiner>
    friend class signal;

    connection(boost::signals2::scoped_connection c)
      : m_c {std::move(c)}
    {
    }

  public:
    connection()                      = default;
    connection(const connection&)     = delete;
    connection& operator=(const connection&) = delete;

    connection(connection&& other) noexcept
      : m_c {std::move(other.m_c)}
    {
    }

    connection& operator=(connection&& other) noexcept
    {
      m_c = std::move(other.m_c);
      return *this;
    }

    /// Has connection?
    bool connected() const
    {
      return m_c.connected();
    }

    /// Convert to boolean
    operator bool() const
    {
      return connected();
    }

    /// Release connection
    void disconnect()
    {
      m_c.disconnect();
    };
  };

} // namespace y2::ui