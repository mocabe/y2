// SPDX-License-Identifier: MIT

#pragma once

#include <y2/ui/vec.hpp>
#include <y2/ui/unit.hpp>

namespace y2::ui {

  /// 2D size data
  template <class T>
  struct size
  {
    T w = {};
    T h = {};

    constexpr size() = default;

    template <class U>
    explicit constexpr size(U s)
      : w {s}
      , h {w}
    {
    }

    template <class U>
    constexpr size(U w, U h)
      : w {w}
      , h {h}
    {
    }

    constexpr size(const vec<T>& v)
      : w {v.x}
      , h {v.y}
    {
    }

    constexpr auto vec() const -> vec<T>
    {
      return {w, h};
    }

    constexpr auto operator*=(f64 s) -> size&
    {
      w *= s;
      h *= s;
      return *this;
    }

    constexpr auto operator/=(f64 s) -> size&
    {
      w /= s;
      h /= s;
      return *this;
    }

    constexpr bool operator==(const size&) const = default;
    constexpr bool operator!=(const size&) const = default;
  };

  template <class T>
  constexpr auto operator*(const size<T>& sz, f64 s) -> size<T>
  {
    auto tmp = sz;
    tmp *= s;
    return tmp;
  }

  template <class T>
  constexpr auto operator/(const size<T>& sz, f64 s) -> size<T>
  {
    auto tmp = sz;
    tmp /= s;
    return tmp;
  }

  using size_pt = size<unit::point>;
  using size_px = size<unit::pixel>;
  using size_lp = size<unit::logical_pixel>;
  using size_bp = size<unit::buffer_pixel>;

} // namespace y2::ui