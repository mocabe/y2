// SPDX-License-Identifier: MIT

#pragma once

#include <y2/ui/event.hpp>
#include <y2/ui/rect.hpp>
#include <y2/ui/screen.hpp>
#include <y2/core/string.hpp>

namespace y2::ui::events {

  class screen_event : public event
  {
  public:
    screen_event();
  };

} // namespace y2::ui::events

namespace y2::ui::screen_events {

  class connect final : public events::screen_event
  {
    ui::screen m_screen;

  public:
    connect(const ui::screen& m);

    auto screen() const -> ui::screen;
  };

  class disconnect final : public events::screen_event
  {
    ui::screen m_screen;

  public:
    disconnect(const ui::screen& m);

    auto screen() const -> ui::screen;
  };

  class primary_change final : public events::screen_event
  {
    ui::screen m_screen;

  public:
    primary_change(const ui::screen& m);

    auto screen() const -> ui::screen;
  };

} // namespace y2::ui::screen_events