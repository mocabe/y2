// SPDX-License-Identifier: MIT

#pragma once

#include <y2/ui/application_fwd.hpp>
#include <y2/ui/os/application_fwd.hpp>
#include <y2/ui/application_events.hpp>
#include <y2/ui/screen_events.hpp>
#include <y2/ui/event_receiver.hpp>
#include <y2/ui/exit_code.hpp>
#include <y2/ui/memory.hpp>
#include <y2/ui/signal.hpp>
#include <y2/core/uuid.hpp>
#include <y2/core/badge.hpp>

#include <string>

namespace y2::ui {

  ///
  /// \brief Application class.
  ///
  /// You should inherit this class and create precisely one instance (in unique or shared).
  ///
  class application : public event_receiver
  {
  public:
    static auto instance() -> application&;
    static bool send_event(event_receiver& t, ui::event& e);
    static void post_event(event_receiver& t, unique<ui::event> e);
    static void exit(exit_code code = exit_code::success);

    template <std::derived_from<application> Derived, class... Args>
    static auto make_derived(Args&&... args);

  public:
    application(const application&) = delete;
    virtual ~application() noexcept;

    auto run() -> exit_code;

    bool send_event(ui::event& e) override;
    void post_event(unique<ui::event> e);

  protected:
    application(badge<application>, string id, int argc, char** argv);
    application(badge<application>, string id, std::vector<string> args);

    virtual bool receive_event(ui::event& event);
    virtual bool receive_event(events::application_event& event);
    virtual bool receive_event(events::screen_event& event);

  private:
    class impl;
    unique<impl> m_impl;
  };

  ///
  /// \brief Create derived class of application.
  /// \param args Arguments for constructor of derived class.
  ///
  template <std::derived_from<application> Derived, class... Args>
  auto application::make_derived(Args&&... args)
  {
    return ui::make_unique<Derived>(badge<application>(), std::forward<Args>(args)...);
  }

} // namespace y2::ui