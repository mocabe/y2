// SPDX-License-Identifier: MIT

#pragma once

#include <y2/ui/typeid_cast.hpp>

#include <concepts>
#include <functional>

namespace y2::ui {

  template <class Derived>
  struct enable_dynamic_cast
  {
    template <std::derived_from<Derived> T>
    bool is() const
    {
      return dynamic_cast_p<T>();
    }

    template <std::derived_from<Derived> T>
    auto as() const -> const T&
    {
      return dynamic_cast_r<T>();
    }

    template <std::derived_from<Derived> T>
    auto as() -> T&
    {
      return dynamic_cast_r<T>();
    }

    template <std::derived_from<Derived> T>
    auto get_as() const -> const T*
    {
      return dynamic_cast_p<T>();
    }

    template <std::derived_from<Derived> T>
    auto get_as() -> T*
    {
      return dynamic_cast_p<T>();
    }

    template <std::derived_from<Derived> T, std::invocable<const T&> F>
    auto as(F&& f) const -> decltype(auto)
    {
      return std::invoke(std::forward<F>(f), as<T>());
    }

    template <std::derived_from<Derived> T, std::invocable<T&> F>
    auto as(F&& f) -> decltype(auto)
    {
      return std::invoke(std::forward<F>(f), as<T>());
    }

  private:
    auto as_derived() const
    {
      return static_cast<const Derived*>(this);
    }
    auto as_derived()
    {
      return static_cast<Derived*>(this);
    }

    template <class T>
    auto dynamic_cast_p() const
    {
      if constexpr (std::is_final_v<T>)
        return typeid_cast<const T>(as_derived());
      else
        return dynamic_cast<const T*>(as_derived());
    }

    template <class T>
    auto dynamic_cast_p()
    {
      if constexpr (std::is_final_v<T>)
        return typeid_cast<T>(as_derived());
      else
        return dynamic_cast<T*>(as_derived());
    }

    template <class T>
    auto& dynamic_cast_r() const
    {
      if constexpr (std::is_final_v<T>)
        return typeid_cast<const T>(*as_derived());
      else
        return dynamic_cast<const T&>(*as_derived());
    }

    template <class T>
    auto& dynamic_cast_r()
    {
      if constexpr (std::is_final_v<T>)
        return typeid_cast<T>(*as_derived());
      else
        return dynamic_cast<T&>(*as_derived());
    }
  };

} // namespace y2::ui