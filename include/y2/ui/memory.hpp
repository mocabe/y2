// SPDX-License-Identifier: MIT

#pragma once

#include <y2/core/prelude.hpp>

#include <stdexcept>
#include <cassert>
#include <memory>
#include <functional>

namespace y2::ui {

  template <class T>
  class unique;
  template <class T>
  class shared;
  template <class T>
  class weak;

  ///
  /// \brief Unique pointer class.
  ///
  /// Unlike std::unique_ptr, this class allows taking weak reference from it.
  ///
  template <class T>
  class unique
  {
    std::shared_ptr<T> m_ptr;

    template <class U>
    friend class unique;

    friend class shared<T>;
    friend class weak<T>;

    template <class U, class... Args>
    friend auto make_unique(Args&&... args) -> unique<U>;

    explicit unique(std::shared_ptr<T>&& up) noexcept
      : m_ptr {std::move(up)}
    {
    }

    auto get_weak() const
    {
      return std::weak_ptr(m_ptr);
    }

  public:
    using element_type = typename std::shared_ptr<T>::element_type;
    using weak_type    = weak<T>;

    unique()                  = default;
    unique(const unique&)     = delete;
    unique(unique&&) noexcept = default;
    unique& operator=(const unique&) = delete;
    unique& operator=(unique&&) noexcept = default;

    ///
    /// \brief Construct from std::unique_ptr.
    ///
    explicit unique(std::unique_ptr<T>&& up) noexcept
      : m_ptr {std::move(up)}
    {
    }

    /// 
    /// \brief Construct from other unique.
    ///
    template <class U>
    unique(unique<U>&& other) noexcept
      : m_ptr {std::move(other.m_ptr)}
    {
    }

    ///
    /// \brief Assign from other unique.
    ///
    template <class U>
    unique& operator=(unique<U>&& other) noexcept
    {
      m_ptr = std::move(other.m_ptr);
      return *this;
    }

    ///
    /// \brief Construct from nullptr_t.
    ///
    unique(std::nullptr_t) noexcept
      : m_ptr {nullptr}
    {
    }

    ///
    /// \brief Assign nullptr_t.
    ///
    unique& operator=(std::nullptr_t) noexcept
    {
      m_ptr = nullptr;
      return *this;
    }

    ///
    /// \brief Get pointer.
    ///
    auto get() const noexcept
    {
      return m_ptr.get();
    }

    ///
    /// \brief Dereference.
    ///
    auto& operator*() const
    {
      assert(m_ptr);
      return m_ptr.operator*();
    }

    ///
    /// \brief Access member of object.
    ///
    auto operator->() const noexcept
    {
      return m_ptr.operator->();
    }

    ///
    /// \brief Convert to boolean value.
    ///
    explicit operator bool() const noexcept
    {
      return static_cast<bool>(m_ptr);
    }

    ///
    /// \brief Swap.
    ///
    void swap(unique& other) noexcept
    {
      std::ranges::swap(m_ptr, other.m_ptr);
    }

    ///
    /// \brief Call static_cast on pointer value.
    /// \return Unique pointer of new type U.
    ///
    template <class U>
    auto cast_static() && noexcept -> unique<U>
    {
      return unique<U>(std::static_pointer_cast<U>(std::move(m_ptr)));
    }

    ///
    /// \brief Call dynamic_cast on pointer.
    /// \return Unique pointer of type U on success, otherwise nullptr.
    ///
    template <class U>
    auto cast_dynamic() && noexcept -> unique<U>
    {
      return unique<U>(std::dynamic_pointer_cast<U>(std::move(m_ptr)));
    }

    ///
    /// \brief Three-way comparison.
    ///
    auto operator<=>(const unique&) const -> std::strong_ordering = default;
  };

  ///
  /// \brief make_unique.
  ///
  template <class T, class... Args>
  auto make_unique(Args&&... args) -> unique<T>
  {
    return unique(std::make_shared<T>(std::forward<Args>(args)...));
  }

  ///
  /// \brief Call static_cast on pointer value.
  /// \return Unique pointer of new type U.
  ///
  template <class T, class U>
  auto static_pointer_cast(unique<U>&& p)
  {
    return std::move(p).template cast_static<T>();
  }

  ///
  /// \brief Call dynamic_cast on pointer.
  /// \return Unique pointer of type U on success, otherwise nullptr.
  ///
  template <class T, class U>
  auto dynamic_pointer_cast(unique<U>&& p)
  {
    return std::move(p).template cast_dynamic<T>();
  }

  ///
  /// \brief Shared pointer class.
  ///
  /// This class wraps std::shared_ptr.
  ///
  template <class T>
  class shared
  {
    std::shared_ptr<T> m_ptr;

    template <class U>
    friend class shared;

    friend class weak<T>;

    auto get_weak() const
    {
      return std::weak_ptr(m_ptr);
    }

  public:
    using element_type = typename std::shared_ptr<T>::element_type;
    using weak_type    = weak<T>;

    shared()                  = default;
    shared(const shared&)     = default;
    shared(shared&&) noexcept = default;
    shared& operator=(const shared&) = default;
    shared& operator=(shared&&) = default;

    ///
    /// \brief Construct from std::shared_ptr.
    ///
    explicit shared(std::shared_ptr<T> sp) noexcept
      : m_ptr {std::move(sp)}
    {
    }

    ///
    /// \brief Construct from other shared.
    ///
    template <class U>
    shared(shared<U> other) noexcept
      : m_ptr {std::move(other.m_ptr)}
    {
    }

    ///
    /// \brief Construct by aliasing pointer.
    ///
    template <class U>
    shared(shared<U> r, element_type* p) noexcept
      : m_ptr {std::move(r.m_ptr), p}
    {
    }

    ///
    /// \brief Assing other shared.
    ///
    template <class U>
    shared& operator=(shared<U> other) noexcept
    {
      m_ptr = std::move(other.m_ptr);
      return *this;
    }

    /// 
    /// \brief Construct from other shared.
    ///
    template <class U>
    shared(unique<U>&& other) noexcept
      : m_ptr {std::move(other.m_ptr)}
    {
    }

    ///
    /// \brief Assing other shared.
    ///
    template <class U>
    shared& operator=(unique<U>&& other) noexcept
    {
      m_ptr = std::move(other.m_ptr);
      return *this;
    }

    ///
    /// \brief Construct from nullptr_t.
    ///
    shared(std::nullptr_t)
      : m_ptr {nullptr}
    {
    }

    ///
    /// \brief Assign nullptr_t.
    ///
    shared& operator=(std::nullptr_t)
    {
      m_ptr = nullptr;
      return *this;
    }

    ///
    /// \brief Get pointer.
    ///
    auto get() const noexcept
    {
      return m_ptr.get();
    }

    ///
    /// \brief Dereference.
    ///
    auto& operator*() const noexcept
    {
      assert(m_ptr);
      return m_ptr.operator*();
    }

    ///
    /// \brief Access member of contained value.
    ///
    auto operator->() const noexcept
    {
      return m_ptr.operator->();
    }

    ///
    /// \brief Convert to boolean.
    ///
    explicit operator bool() const noexcept
    {
      return m_ptr.operator bool();
    }

    ///
    /// \brief Get reference count.
    ///
    auto use_count() const noexcept
    {
      return m_ptr.use_count();
    }

    ///
    /// \brief Swap.
    ///
    void swap(shared& other) noexcept
    {
      std::ranges::swap(m_ptr, other.m_ptr);
    }

    ///
    /// \brief static_cast.
    ///
    template <class U>
    auto cast_static() const& noexcept -> shared<U>
    {
      return shared<U>(std::static_pointer_cast<U>(m_ptr));
    }

    ///
    /// \brief static_cast.
    ///
    template <class U>
    auto cast_static() && noexcept -> shared<U>
    {
      return shared<U>(std::static_pointer_cast<U>(std::move(m_ptr)));
    }

    ///
    /// \brief dynamic_cast.
    ///
    template <class U>
    auto cast_dynamic() const& noexcept -> shared<U>
    {
      return shared<U>(std::dynamic_pointer_cast<U>(m_ptr));
    }

    ///
    /// \brief dynamic_cast.
    ///
    template <class U>
    auto cast_dynamic() && noexcept -> shared<U>
    {
      return shared<U>(std::dynamic_pointer_cast<U>(std::move(m_ptr)));
    }

    ///
    /// \brief three-way comparison.
    ///
    auto operator<=>(const shared&) const -> std::strong_ordering = default;
  };

  template <class T>
  shared(unique<T>) -> shared<T>;

  ///
  /// \brief make_shared.
  ///
  template <class T, class... Args>
  auto make_shared(Args&&... args) -> shared<T>
  {
    return shared(std::make_shared<T>(std::forward<Args>(args)...));
  }

  ///
  /// \brief static_cast.
  ///
  template <class T, class U>
  auto static_pointer_cast(const shared<U>& p)
  {
    return p.template cast_static<T>();
  }

  ///
  /// \brief dynamic_cast.
  ///
  template <class T, class U>
  auto dynamic_pointer_cast(const shared<U>& p)
  {
    return p.template cast_dynamic<T>();
  }

  ///
  /// \brief Weak pointer class.
  ///
  /// Unlike std::weak_ptr, you cannot take shared reference from this class.
  ///
  template <class T>
  class weak
  {
    T* m_ref = nullptr;
    std::weak_ptr<const void> m_weak;

    template <class U>
    friend class weak;

  public:
    using element_type = typename std::weak_ptr<T>::element_type;

    weak()            = default;
    weak(const weak&) = default;
    weak& operator=(const weak&) = default;

    ///
    /// \brief Construct from rvalue.
    ///
    weak(weak&& other) noexcept
    {
      swap(other);
    }

    ///
    /// \brief Assign rvalue.
    ///
    weak& operator=(weak&& other) noexcept
    {
      auto tmp = std::move(other);
      swap(tmp);
      return *this;
    }

    ///
    /// \brief Construct from std::shared_ptr.
    ///
    explicit weak(const std::shared_ptr<T>& ptr)
      : m_ref {ptr.get()}
      , m_weak {ptr}
    {
    }

    ///
    /// \brief Construct from trackable reference.
    ///
    explicit weak(T& ref)
      : m_ref {&ref}
      , m_weak {ref.track().m_weak}
    {
    }

    ///
    /// \brief Copy from other weak.
    ///
    template <class U>
    weak(const weak<U>& other)
      : m_ref {other.m_ref}
      , m_weak {other.m_weak}
    {
    }

    ///
    /// \brief Assign other weak.
    ///
    template <class U>
    weak& operator=(const weak<U>& other)
    {
      m_ref  = other.m_ref;
      m_weak = other.m_weak;
      return *this;
    }

    ///
    /// \brief Construct from unique<T>.
    ///
    template <class U>
    weak(const unique<U>& u)
      : m_ref {u.m_ptr.get()}
      , m_weak {u.m_ptr}
    {
    }

    ///
    /// \brief Construct from shared<T>.
    ///
    template <class U>
    weak(const shared<U>& u)
      : m_ref {u.m_ptr.get()}
      , m_weak {u.m_ptr}
    {
    }

    ///
    /// \brief Check if this pointer is null.
    /// \note This function should not be used to check lifetime of tracked object.
    ///
    bool is_null() const noexcept
    {
      return m_ref == nullptr;
    }

    ///
    /// \brief Check if weak reference is invalid.
    ///
    bool expired() const
    {
      return m_weak.expired();
    }

    ///
    /// \brief Convert to boolean.
    ///
    explicit operator bool() const
    {
      return get();
    }

    ///
    /// \brief three-way comparison.
    ///
    auto operator<=>(const weak& other) const -> std::strong_ordering
    {
      return operator<=>(m_ref, other.m_ref);
    }

    ///
    /// \brief Swap.
    ///
    void swap(weak& other) noexcept
    {
      std::ranges::swap(m_ref, other.m_ref);
      std::ranges::swap(m_weak, other.m_weak);
    }

    ///
    /// \brief Get raw pointer.
    /// \note This does not lock reference.
    ///
    auto get() const -> T*
    {
      if (!m_weak.expired()) {
        return m_ref;
      }
      return nullptr;
    }

    ///
    /// \brief Apply function when weak is valid.
    ///
    /// Locks weak reference and calls predicate on it.
    ///
    /// \param pred Predicate to execute.
    /// \return true when succeeded to lock.
    ///
    template <class Pred>
    bool lock(Pred&& pred)
    {
      if (auto ref = m_weak.lock()) {
        std::invoke(std::forward<Pred>(pred), *m_ref);
        return true;
      }
      return false;
    }

    ///
    /// \brief Apply function when weak is valid.
    ///
    /// Locks weak reference and calls predicate on it.
    ///
    /// \param pred Predicate to execute.
    /// \return true when succeeded to lock.
    ///
    template <class Pred>
    bool lock(Pred&& pred) const
    {
      if (auto ref = m_weak.lock()) {
        std::invoke(std::forward<Pred>(pred), *m_ref);
        return true;
      }
      return false;
    }
  };

  template <class T>
  weak(unique<T>) -> weak<T>;

  template <class T>
  weak(shared<T>) -> weak<T>;

} // namespace y2::ui