// SPDX-License-Identifier: MIT

#pragma once

#include <y2/ui/signal.hpp>
#include <y2/ui/event.hpp>
#include <y2/ui/event_phase_scope.hpp>

namespace y2::ui {

  ///
  /// \brief List of event handlers.
  ///
  class event_handler_list
  {
    signal<bool(event&), signal_combiner_any_of> m_signal;

  public:
    event_handler_list()                              = default;
    event_handler_list(event_handler_list&&) noexcept = default;
    event_handler_list& operator=(event_handler_list&&) noexcept = default;

    template <class Event>
    bool handle_event(Event& event);

    template <class Trackable, class Handler>
    void connect_event_handler(Trackable& trackable, Handler& handler);

    void clear();
  };

  ///
  /// \brief Dispatch event by calling event handlers.
  /// \param event Event to handle.
  ///
  template <class Event>
  bool event_handler_list::handle_event(Event& event)
  {
    if (!event.dispatching()) {
      const auto phase = event_phase::target;
      if (contains(event.phase(), phase)) {
        auto scope = event_phase_scope(event, phase);
        return handle_event(event);
      }
      return false;
    }
    return m_signal(event);
  }

  ///
  /// \brief Add event handler to end of list.
  /// \param trackable Reference to trackable object.
  /// \param handler Event handler.
  ///
  template <class Trackable, class Handler>
  void event_handler_list::connect_event_handler(Trackable& trackable, Handler& handler)
  {
    connect(m_signal, trackable, std::move(handler));
  }

} // namespace y2::ui