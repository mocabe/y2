//
// Copyright (c) 2019 mocabe (https://github.com/mocabe)
// Distributed under LGPLv3 License. See LICENSE for more details.
//

#pragma once

#include <y2/ui/os/screen_fwd.hpp>
#include <y2/ui/memory.hpp>
#include <y2/ui/signal.hpp>
#include <y2/ui/rect.hpp>
#include <y2/core/string.hpp>

#include <string>

namespace y2::ui {

  ///
  /// \brief Screen handle object.
  ///
  /// This class represents handle to a screen.
  ///
  /// Screen can expire when disconnected.
  /// After disconnection, `valid()` will return false.
  ///
  /// You can connect to signals to handle events sent to the screen.
  /// Signal connections are 'severed' from copy/assignment of handle.
  /// Only way you can transfer signal connections is by moving it.
  ///
  class screen final
  {
    shared<os_screen> m_os;
    unique<os_screen_delegate> m_delegate;

    void disconnect_signals();
    void add_os_delegate();
    void remove_os();

  public:
    /// Screen is disconnected.
    signal<void()> disconnected;

    /// Screen geometry changed.
    /// \param geometry New screen geometry.
    signal<void(ui::rect_px)> geometry_changed;

    /// Screen work area changed.
    /// \param work_area New screen work area.
    signal<void(ui::rect_px)> work_area_changed;

    /// Screen content scale changed.
    /// \param content_scale New content scale.
    signal<void(unit::content_scale)> content_scale_changed;

    /// Screen logical DPI changed.
    /// \param dpi New logical dpi.
    signal<void(unit::logical_dpi)> logical_dpi_changed;

    /// Screen physical DPI changed.
    /// \param dpi New physical dpi.
    signal<void(unit::physical_dpi)> physical_dpi_changed;

    /// Screen display name changed.
    /// \param name New display name.
    signal<void(string_view)> name_changed;

  public:
    [[nodiscard]] static auto primary() -> screen;
    [[nodiscard]] static auto enumerate() -> std::vector<screen>;
    [[nodiscard]] static auto default_logical_dpi() -> unit::logical_dpi;

  public:
    explicit screen(shared<os_screen> p);
    screen(const screen& other);
    screen(screen&& other) noexcept;
    screen& operator=(const screen&);
    screen& operator=(screen&&) noexcept;
    ~screen() noexcept;

  public:
    [[nodiscard]] bool valid() const;
    [[nodiscard]] auto geometry() const -> ui::rect_px;
    [[nodiscard]] auto work_area() const -> ui::rect_px;
    [[nodiscard]] auto content_scale() const -> unit::content_scale;
    [[nodiscard]] auto logical_dpi() const -> unit::logical_dpi;
    [[nodiscard]] auto physical_dpi() const -> unit::physical_dpi;
    [[nodiscard]] auto name() const -> string;
  };

} // namespace y2::ui