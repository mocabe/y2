// SPDX-License-Identifier: MIT

#pragma once

#include <y2/ui/vec.hpp>
#include <y2/ui/size.hpp>
#include <y2/ui/point.hpp>

#include <algorithm>
#include <cassert>

namespace y2::ui {

  /// 2D rect
  template <class T>
  struct rect
  {
    ui::point<T> m_pos;
    ui::size<T> m_size;

  public:
    rect()            = default;
    rect(const rect&) = default;
    rect& operator=(const rect&) = default;

    rect(ui::point<T> pos, ui::size<T> size)
      : m_pos {pos}
      , m_size {size}
    {
    }

    rect(ui::point<T> pos, ui::vec<T> vec)
      : m_pos {pos}
      , m_size {vec}
    {
    }

    rect(ui::point<T> p1, ui::point<T> p2)
      : m_pos {p1}
      , m_size {p2 - p1}
    {
    }

    rect(ui::size<T> s)
      : m_pos {}
      , m_size {s}
    {
    }

    bool valid() const
    {
      return m_size.w >= T() && m_size.h >= T();
    }

    bool empty() const
    {
      return m_size.vec() == vec<T>();
    };

    bool exists() const
    {
      return valid() && !empty();
    }

    auto& pos() const
    {
      return m_pos;
    }

    void set_pos(ui::point<T> pos)
    {
      m_pos = pos;
    }

    auto& size() const
    {
      return m_size;
    }

    void set_size(ui::size<T> size)
    {
      m_size = size;
    }

    auto width() const
    {
      return m_size.w;
    }

    void set_width(T width)
    {
      m_size.w = width;
    }

    auto height() const
    {
      return m_size.h;
    }

    void set_height(T height)
    {
      m_size.h = height;
    }

    auto top() const
    {
      return m_pos.y;
    }

    void set_top(T top)
    {
      m_pos.y = top;
    }

    auto left() const
    {
      return m_pos.x;
    }

    void set_left(T left)
    {
      m_pos.x = left;
    }

    auto bottom() const
    {
      return top() + m_size.h;
    }

    void set_bottom(T bottom)
    {
      m_size.h = bottom - top();
    }

    auto right() const
    {
      return left() + m_size.w;
    }

    void set_right(T right)
    {
      m_size.w = right - left();
    }

    auto top_left() const
    {
      return point<T>(top(), left());
    }

    auto right_bottom() const
    {
      return point<T>(right(), bottom());
    }

    void inflate(T w, T h)
    {
      m_pos.x -= w;
      m_size.w += (2 * w);
      m_pos.y -= h;
      m_size.h += (2 * h);
    }

    [[nodiscard]] bool contains(const rect& r) const;

    [[nodiscard]] bool contains(const point<T>& p) const;

    [[nodiscard]] bool intersects(const rect& r) const;

    constexpr bool operator==(const rect&) const = default;
    constexpr bool operator!=(const rect&) const = default;
  };

  template <class T>
  bool rect<T>::contains(const rect<T>& r) const
  {
    if (!valid() || !r.valid())
      return false;

    if (!exists() || !r.exists())
      return false;

    if (top_left().x > r.top_left().x || top_left().y > r.top_left().y)
      return false;

    if (r.right_bottom().x > right_bottom().x || r.right_bottom().y > right_bottom().y)
      return false;

    return true;
  }

  template <class T>
  bool rect<T>::contains(const point<T>& p) const
  {
    if (!valid() || !exists())
      return false;

    if (top_left().x > p.x || top_left().y > p.y)
      return false;

    if (p.x > right_bottom().x || p.y > right_bottom().y)
      return false;

    return true;
  }

  template <class T>
  bool rect<T>::intersects(const rect<T>& r) const
  {
    if (!valid() || !exists())
      return false;

    if (r.top_left().x > right_bottom().x || r.top_left().y > right_bottom().y)
      return false;

    return true;
  }

  ///
  /// \brief Calculate intersection of two rectangles.
  ///
  template <class T>
  [[nodiscard]] auto intersect(const rect<T>& rect1, const rect<T>& rect2) noexcept -> rect<T>
  {
    if (!rect1.valid() || !rect2.valid())
      return {};

    const auto p1 = rect1.pos();
    const auto p2 = rect1.pos() + rect1.size().vec();
    const auto q1 = rect2.pos();
    const auto q2 = rect2.pos() + rect2.size().vec();

    const auto r1 = point<T>(std::max(p1.x, q1.x), std::max(p1.y, q1.y));
    const auto r2 = point<T>(std::min(p2.x, q2.x), std::min(p2.y, q2.y));

    if (r1.x < r2.x && r1.y < r2.y) {
      return {r1, r2};
    }
    return {};
  }

  ///
  /// \brief Calculate union of two rectangles.
  ///
  template <class T>
  [[nodiscard]] auto unite(const rect<T>& rect1, const rect<T>& rect2) noexcept -> rect<T>
  {
    if (!rect1.valid() || !rect2.valid()) {
      return rect1.valid() ? rect1 : (rect2.valid() ? rect2 : rect<T>());
    }

    const auto p1 = rect1.pos();
    const auto p2 = rect1.pos() + rect1.size().vec();
    const auto q1 = rect2.pos();
    const auto q2 = rect2.pos() + rect2.size().vec();

    const auto r1 = point<T>(std::min(p1.x, q1.x), std::min(p1.y, q1.y));
    const auto r2 = point<T>(std::max(p2.x, q2.x), std::max(p2.y, q2.y));
    return {r1, r2};
  }

  ///
  /// \brief Contains operators for rect<T>.
  ///
  namespace rect_operators {

    ///
    /// \brief Calculate intersection of two rectangles.
    ///
    template <class T>
    [[nodiscard]] auto operator&(const rect<T>& rect1, const rect<T>& rect2) noexcept -> rect<T>
    {
      return intersect(rect1, rect2);
    }

    ///
    /// \brief Calculate union of two rectangles.
    ///
    template <class T>
    [[nodiscard]] auto operator|(const rect<T>& rect1, const rect<T>& rect2) noexcept -> rect<T>
    {
      return unite(rect1, rect2);
    }

  } // namespace rect_operators

  using rect_pt = rect<unit::point>;
  using rect_px = rect<unit::pixel>;
  using rect_lp = rect<unit::logical_pixel>;
  using rect_bp = rect<unit::buffer_pixel>;

} // namespace y2::ui