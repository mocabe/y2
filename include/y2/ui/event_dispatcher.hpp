// SPDX-License-Identifier: MIT

#pragma once

#include <y2/ui/event_receiver_fwd.hpp>
#include <y2/ui/event.hpp>
#include <y2/ui/memory.hpp>
#include <y2/core/badge.hpp>

namespace y2::ui {

  ///
  /// \brief Event dispatcher.
  ///
  class event_dispatcher : public trackable
  {
  public:
    static auto make(event_receiver& owner) -> unique<event_dispatcher>;
    static auto make(event_receiver& owner, event_dispatcher& parent) -> unique<event_dispatcher>;

  public:
    event_dispatcher(badge<event_dispatcher>, event_receiver& owner);
    event_dispatcher(badge<event_dispatcher>, event_receiver& owner, event_dispatcher& parent);
    virtual ~event_dispatcher() noexcept = default;

    virtual bool dispatch(ui::event& event);

  protected:
    bool send_event_with_phase(ui::event& e, event_phase p) const;

    static bool dispatch_with_phase(weak<event_dispatcher> d, ui::event& e, event_phase p);
    static bool dispatch_capture(weak<event_dispatcher> d, ui::event& e);
    static bool dispatch_bubble(weak<event_dispatcher> d, ui::event& e);
    static bool dispatch_target(weak<event_dispatcher> d, ui::event& e);

  private:
    bool dispatch_internal(ui::event& event);
    event_receiver& m_owner;
    weak<event_dispatcher> m_parent;
  };

} // namespace y2::ui