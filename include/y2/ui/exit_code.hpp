// SPDX-License-Identifier: MIT

#pragma once

#include <y2/core/prelude.hpp>

namespace y2::ui {

  ///
  /// \brief Exit code.
  ///
  enum class exit_code : s32
  {
    success = 0, ///< Success.
    failure,     ///< Failure.
    error,       ///< Error.
  };
}