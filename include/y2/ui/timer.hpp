// SPDX-License-Identifier: MIT

#pragma once

#include <y2/ui/signal.hpp>
#include <y2/ui/memory.hpp>
#include <y2/ui/event_handler_list.hpp>
#include <y2/ui/timer_events.hpp>
#include <y2/core/prelude.hpp>

#include <chrono>

namespace y2::ui {

  ///
  /// \brief UI timer class.
  ///
  /// Currently only usable from main thread.
  ///
  class timer final
  {
    class delegate;
    unique<delegate> m_delegate;
    event_handler_list m_handlers;

    void init();

  public:
    /// Type used to represent interval time.
    using interval_type = std::chrono::milliseconds;

    ///
    /// \brief Connect external event handler.
    ///
    template <class Trackable, class Handler>
    void connect_event_handler(Trackable& trackable, Handler handler)
    {
      m_handlers.connect_event_handler(trackable, handler);
    }

  public:
    timer();
    ~timer() noexcept;
    timer(const timer&) = delete;
    timer& operator=(const timer&) = delete;
    timer(timer&&) noexcept;
    timer& operator=(timer&&) noexcept;

    void start();
    void stop();
    auto id() const -> u32;
    auto interval() const -> interval_type;
    void set_interval(interval_type interval);
  };

} // namespace y2::ui