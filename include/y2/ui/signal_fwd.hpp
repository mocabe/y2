// SPDX-License-Identifier: MIT

#pragma once

#include <y2/core/prelude.hpp>
#include <y2/ui/signal_combiners.hpp>

#include <boost/signals2/signal.hpp>

namespace y2::ui {

  template <class Signature, template <class> class Combiner = signal_combiner_last_optional>
  class signal;
}