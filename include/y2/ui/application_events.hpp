// SPDX-License-Identifier: MIT

#pragma once

#include <y2/ui/event.hpp>
#include <y2/ui/exit_code.hpp>
#include <y2/core/string.hpp>
#include <y2/core/uuid.hpp>

#include <exception>

namespace y2::ui::events {

  ///
  /// \brief Base of application events.
  ///
  /// Application events will not bubble or capture.
  ///
  class application_event : public event
  {
  protected:
    application_event();
  };

} // namespace y2::ui::events

namespace y2::ui::application_events {

  ///
  /// \brief Application event loop is started.
  ///
  /// Application will receive this event right before entering event loop.
  ///
  class start final : public events::application_event
  {
    string m_id;
    std::vector<string> m_args;

  public:
    start(string id, std::vector<string> args);

    auto args() const -> std::vector<string>;
    auto id() const -> string;
  };

  ///
  /// \brief Application event loop is exiting.
  ///
  /// This event will be sent just before exiting main event loop.
  /// This event might no be sent when application is terminated by exception.
  ///
  class exit final : public events::application_event
  {
    exit_code m_code;

  public:
    exit(exit_code code);

    auto code() const -> exit_code;
  };

  ///
  /// \brief Unhandled exception in event loop.
  ///
  /// Application will receive this event when event loop received unhandled exception.
  /// You can ignore exception by not rethrowing it.
  ///
  class unhandled_exception final : public events::application_event
  {
    std::exception_ptr m_e;

  public:
    unhandled_exception(std::exception_ptr e);

    auto exception() const -> std::exception_ptr;
  };

} // namespace y2::ui::application_events
