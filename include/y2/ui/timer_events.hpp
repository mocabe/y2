// SPDX-License-Identifier: MIT

#pragma once

#include <y2/ui/event.hpp>
#include <y2/ui/memory.hpp>

namespace y2::ui::events {

  ///
  /// \brief Base class of timer events.
  ///
  class timer_event : public event
  {
  protected:
    timer_event();
  };

} // namespace y2::ui::events

namespace y2::ui::timer_events {

  ///
  /// \brief Timer timeout.
  ///
  class timeout final : public events::timer_event
  {
    s32 m_id = 0;

  public:
    timeout(s32 id);

    auto id() const -> s32;
  };

} // namespace y2::ui::timer_events