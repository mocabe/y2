// SPDX-License-Identifier: MIT

#pragma once

#include <y2/core/prelude.hpp>

namespace y2::ui {

  ///
  /// \brief Event loop class.
  ///
  class event_loop final
  {
  public:
    [[nodiscard]] static auto level() -> u32;

  public:
    event_loop();

    [[nodiscard]] bool running() const;

    void run();
    void stop();

  private:
    bool m_running = false;
  };

} // namespace y2::ui