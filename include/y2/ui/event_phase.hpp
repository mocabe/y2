// SPDX-License-Identifier: MIT

#pragma once

#include <y2/core/prelude.hpp>
#include <y2/core/enum_flag.hpp>

namespace y2::ui {

  /// even phases
  enum class event_phase : u32
  {
    none = 0,
    /// Capture phase:
    /// Event propagates parent to child.
    capture = 1 << 0,
    /// Bubble phase:
    /// Event propagates child to parent.
    bubble = 1 << 1,
    /// Target phase:
    /// Event is directly sent to target.
    target = 1 << 2,
  };

} // namespace y2::ui

Y2_DECL_ENUM_FLAG(y2::ui::event_phase);