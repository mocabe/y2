// SPDX-License-Identifier: MIT

#pragma once

#include <y2/core/prelude.hpp>
#include <y2/ui/unit.hpp>

namespace y2::ui {

  /// 2D vector value
  template <class T>
  struct vec
  {
    T x = {};
    T y = {};

    constexpr vec() = default;

    template <class U>
    explicit constexpr vec(U s)
      : x {s}
      , y {s}
    {
    }

    template <class U>
    constexpr vec(U x, U y)
      : x {x}
      , y {y}
    {
    }

    constexpr vec& operator+=(const vec& o)
    {
      x += o.x;
      y += o.y;
      return *this;
    }

    constexpr vec& operator+=(T d)
    {
      x += d;
      y += d;
      return *this;
    }

    constexpr vec& operator-=(const vec& o)
    {
      x -= o.x;
      y -= o.y;
      return *this;
    }

    constexpr vec& operator-=(T d)
    {
      x -= d;
      y -= d;
      return *this;
    }

    constexpr vec& operator*=(const vec& v)
    {
      x *= v.x;
      y *= v.y;
      return *this;
    }

    constexpr vec& operator*=(f64 r)
    {
      x *= r;
      y *= r;
      return *this;
    }

    constexpr vec& operator/=(const vec& v)
    {
      x /= v.x;
      y /= v.y;
      return *this;
    }

    constexpr vec& operator/=(f64 r)
    {
      x /= r;
      y /= r;
      return *this;
    }

    constexpr bool operator==(const vec&) const = default;
    constexpr bool operator!=(const vec&) const = default;
  };

  template <class T>
  constexpr auto operator+(const vec<T>& v1, const vec<T>& v2) -> vec<T>
  {
    auto r = v1;
    r += v2;
    return r;
  }

  template <class T>
  constexpr auto operator+(const vec<T>& v, T d) -> vec<T>
  {
    auto r = v;
    r += d;
    return r;
  }

  template <class T>
  constexpr auto operator-(const vec<T>& v1, const vec<T>& v2) -> vec<T>
  {
    auto r = v1;
    r -= v2;
    return r;
  }

  template <class T>
  constexpr auto operator-(const vec<T>& v1, T d) -> vec<T>
  {
    auto r = v1;
    r -= d;
    return r;
  }

  template <class T>
  constexpr auto operator-(const vec<T>& v)
  {
    return vec {-v.x, -v.y};
  }

  template <class T>
  constexpr auto operator*(const vec<T>& v1, const vec<T>& v2) -> vec<T>
  {
    auto r = v1;
    r *= v2;
    return r;
  }

  template <class T>
  constexpr auto operator*(const vec<T>& v1, f64 d) -> vec<T>
  {
    auto r = v1;
    r *= d;
    return r;
  }

  template <class T>
  constexpr auto operator/(const vec<T>& v1, const vec<T>& v2) -> vec<T>
  {
    auto r = v1;
    r /= v2;
    return r;
  }

  template <class T>
  constexpr auto operator/(const vec<T>& v1, f64 d) -> vec<T>
  {
    auto r = v1;
    r /= d;
    return r;
  }

  template <class U>
  vec(U) -> vec<U>;

  template <class U>
  vec(U, U) -> vec<U>;

  using vec_pt = vec<unit::point>;
  using vec_px = vec<unit::pixel>;
  using vec_lp = vec<unit::logical_pixel>;
  using vec_bp = vec<unit::buffer_pixel>;

} // namespace y2::ui