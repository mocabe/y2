// SPDX-License-Identifier: MIT

#pragma once

#include <y2/ui/event_receiver_fwd.hpp>
#include <y2/ui/memory.hpp>
#include <y2/ui/trackable.hpp>
#include <y2/ui/event.hpp>
#include <y2/ui/event_dispatcher.hpp>

namespace y2::ui {

  ///
  /// \brief Event receiver.
  ///
  class event_receiver : public trackable
  {
  public:
    event_receiver(ui::event_receiver* parent);
    virtual ~event_receiver() noexcept = default;

    virtual bool send_event(ui::event& event) = 0;

  protected:
    virtual auto build_event_dispatcher(ui::event_receiver* parent) -> unique<ui::event_dispatcher>;

    void reset_event_dispatcher(ui::event_receiver* parent);
    auto event_dispatcher() -> ui::event_dispatcher&;

  private:
    ui::event_receiver* m_parent = nullptr;
    unique<ui::event_dispatcher> m_dispatcher;
  };

} // namespace y2::ui