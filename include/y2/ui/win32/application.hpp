// SPDX-License-Identifier: MIT

#pragma once

#include <y2/ui/win32/application_fwd.hpp>
#include <y2/ui/win32/application_base.hpp>
#include <y2/ui/win32/screen_info.hpp>
#include <y2/ui/win32/event_queue.hpp>
#include <y2/ui/os/application.hpp>

namespace y2::ui::win32 {

  ///
  /// \brief Win32 implementation of os_application.
  ///
  class application final : public application_base,
                      public os_application,
                      private os_screen_info_delegate
  {
  public:
    application(string id, std::vector<string> args);
    ~application() noexcept;

    auto run() -> exit_code override;
    void exit(exit_code code) override;

  private:
    void process_event(events::os_screen_connect& e) override;
    void process_event(events::os_screen_disconnect& e) override;
    void process_event(events::os_primary_screen_change& e) override;

  private:
    optional<exit_code> m_exit_code;
    y2::context<screen_info> m_screen_info;
  };

} // namespace y2::ui::win32