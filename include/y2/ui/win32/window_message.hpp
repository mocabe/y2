// SPDX-License-Identifier: MIT

#pragma once

#include <y2/ui/win32/prelude.hpp>

namespace y2::ui::win32 {

  inline constexpr auto WM_Y2_POSTEVENT = WM_APP + 0; ///< For posted events.
  inline constexpr auto WM_Y2_WAKE      = WM_APP + 1; ///< For waking up event queue.

} // namespace y2::ui::win32
