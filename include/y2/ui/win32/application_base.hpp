// SPDX-License-Identifier: MIT

#pragma once

#include <y2/ui/win32/event_queue.hpp>
#include <y2/ui/win32/screen_info.hpp>
#include <y2/ui/win32/prelude.hpp>

namespace y2::ui::win32 {

  ///
  /// \brief Base class of application.
  ///
  /// This class manages context which must be configured before os_application.
  ///
  class application_base
  {
  public:
    application_base(HINSTANCE inst);
    ~application_base() noexcept = default;

    auto instance_handle() const -> HINSTANCE;

  private:
    HINSTANCE m_inst = {};
    context<event_queue_context> m_event_queue;
  };
}