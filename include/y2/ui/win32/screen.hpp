// SPDX-License-Identifier: MIT

#pragma once

#include <y2/ui/os/screen.hpp>
#include <y2/ui/win32/screen_info_fwd.hpp>
#include <y2/ui/win32/prelude.hpp>
#include <y2/core/context.hpp>

#include <vector>

namespace y2::ui::win32 {

  ///
  /// \brief Win32 implementation of os_screen.
  ///
  class screen : public os_screen
  {
    HMONITOR m_hScreen = {};

    ui::rect_px m_geometry;
    ui::rect_px m_work_area;
    string m_name;
    unit::content_scale m_content_scale = unit::content_scale(1.0);
    unit::logical_dpi m_logical_dpi     = unit::logical_dpi(USER_DEFAULT_SCREEN_DPI);
    unit::physical_dpi m_physical_dpi   = unit::physical_dpi(USER_DEFAULT_SCREEN_DPI);

  public:
    screen(HMONITOR hMonitor);
    ~screen() noexcept;

    auto native_handle() const -> HANDLE;
    void invalidate();
    void refresh();

  public:
    bool valid() const override;
    auto geometry() const -> ui::rect_px override;
    auto work_area() const -> ui::rect_px override;
    auto content_scale() const -> unit::content_scale override;
    auto logical_dpi() const -> unit::logical_dpi override;
    auto physical_dpi() const -> unit::physical_dpi override;
    auto name() const -> string override;
  };

} // namespace y2::ui