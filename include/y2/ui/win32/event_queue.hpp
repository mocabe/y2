// SPDX-License-Identifier: MIT

#pragma once

#include <y2/ui/win32/application_fwd.hpp>
#include <y2/ui/win32/prelude.hpp>
#include <y2/ui/os/event_queue.hpp>
#include <y2/core/badge.hpp>

#include <vector>
#include <atomic>

namespace y2::ui::win32 {

  ///
  /// \brief Event queue context.
  ///
  /// Manages window class used in event queue.
  ///
  class event_queue_context
  {
    HINSTANCE m_instance = {};
    ATOM m_wnd_class     = {};

  public:
    static void configure(HINSTANCE instance);
    static auto context() -> y2::context<event_queue_context>;

    event_queue_context(HINSTANCE inst, badge<event_queue_context>);
    ~event_queue_context() noexcept;

    auto window_class() const -> ATOM;
    auto create_window() -> HWND;
  };

  ///
  /// \brief Win32 implementation of os_event_queue.
  ///
  class event_queue : public os_event_queue
  {
    context<event_queue_context> m_ctx;
    HWND m_wnd                     = {};
    std::atomic<bool> m_stop       = false;
    std::exception_ptr m_exception = {};

    bool process_message(const MSG& msg);

  public:
    event_queue(badge<event_queue>);
    ~event_queue() noexcept;

    static void thread_configure();
    static bool thread_has_context();
    static auto thread_local_context() -> context<event_queue>;

    void exit(badge<win32::application>);
    auto message_window_proc(HWND wnd, UINT msg, WPARAM wparam, LPARAM lparam) noexcept -> LRESULT;

  protected:
    void run() override;
    void wake() override;
    void stop() override;
    void post() override;
    void set_timer(u32 id, std::chrono::milliseconds interval) override;
    void remove_timer(u32 id) override;
  };

} // namespace y2::ui::win32