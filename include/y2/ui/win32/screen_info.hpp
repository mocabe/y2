// SPDX-License-Identifier: MIT

#pragma once

#include <y2/ui/win32/screen_info_fwd.hpp>
#include <y2/ui/win32/screen.hpp>
#include <y2/ui/win32/prelude.hpp>
#include <y2/ui/os/screen_info.hpp>
#include <y2/ui/memory.hpp>
#include <y2/core/badge.hpp>
#include <y2/core/context.hpp>

#include <winuser.h>
#include <vector>

namespace y2::ui::win32 {

  ///
  /// \brief Win32 implementation of os_screen_info.
  ///
  class screen_info : public os_screen_info
  {
    std::vector<shared<screen>> m_screens;
    shared<screen> m_primary;

  public:
    screen_info();
    ~screen_info() noexcept;

    static auto context() -> y2::context<screen_info>;

    void refresh();
    auto find(HMONITOR h) -> shared<os_screen>;
    auto primary() -> shared<os_screen> override;
    auto screens() -> std::vector<shared<os_screen>> override;
    auto default_dpi() -> unit::logical_dpi override;

  private:
    void add_screens(const std::vector<shared<screen>>&);
    void remove_screens(const std::vector<shared<screen>>&);
    void refresh_screens(const std::vector<shared<screen>>&);
  };

} // namespace y2::ui