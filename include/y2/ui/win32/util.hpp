// SPDX-License-Identifier: MIT

#pragma once

#include <y2/ui/win32/prelude.hpp>
#include <y2/ui/rect.hpp>
#include <y2/core/string.hpp>

#include <string_view>

namespace y2::ui::win32 {

  /// Make ui::rect from RECT.
  [[nodiscard]] auto to_rect(RECT rect) noexcept -> ui::rect_px;

  /// make ui::size from SIZE.
  [[nodiscard]] auto to_size(SIZE size) noexcept -> ui::size_px;

  /// Convert Unicode string to UTF-8 string.
  [[nodiscard]] auto wide_to_utf8(std::wstring_view sv) -> string;

  /// Convert UTF-8 strint to Unicode string.
  [[nodiscard]] auto utf8_to_wide(string_view sv) -> std::wstring;

  /// Initialize WIN32 structs.
  template <class T>
  [[nodiscard]] auto make_struct() noexcept
  {
    auto s = T {};
    if constexpr (requires { s.cbSize; }) {
      s.cbSize = sizeof(T);
    }
    if constexpr (requires { s.cb; }) {
      s.cb = sizeof(T);
    }
    return s;
  }

} // namespace y2::ui