// SPDX-License-Identifier: MIT

#pragma once

#include <y2/core/prelude.hpp>  

#define STRICT
#define NOMINMAX
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
