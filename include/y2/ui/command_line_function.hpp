// SPDX-License-Identifier: MIT

#pragma once

#include <y2/core/prelude.hpp>
#include <y2/core/string.hpp>

#include <vector>
#include <string>

namespace y2::ui::command_line_function {

  /// Parse command line into list of arguments.
  [[nodiscard]] auto parse_args(int argc, char** argv) -> std::vector<string>;

}; // namespace y2::ui::command_line_function
