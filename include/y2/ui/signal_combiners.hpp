// SPDX-License-Identifier: MIT

#pragma once

#include <y2/core/optional.hpp>
#include <boost/signals2/optional_last_value.hpp>

namespace y2::ui {

  ///
  /// \brief A signal combiner which returns return value of last slot as optional value.
  ///
  template <class T>
  struct signal_combiner_last_optional
  {
    using result_type = std::conditional_t<std::same_as<T, void>, void, optional<T>>;

    template <class It>
    auto operator()(It it, It end) const -> result_type
    {
      optional<T> value;
      while (it != end) {
        try {
          value = *it;
        } catch (const boost::signals2::expired_slot&) {
        }
        ++it;
      }
      return value;
    }

    template <class It>
    requires std::same_as<T, void>
    auto operator()(It it, It end) const -> result_type
    {
      while (it != end) {
        try {
          *it;
        } catch (const boost::signals2::expired_slot&) {
        }
        ++it;
      }
    }
  };

  ///
  /// \brief A signal combiner which returns first.
  ///
  template <std::convertible_to<bool> T>
  struct signal_combiner_any_of
  {
    using result_type = bool;

    template <class It>
    auto operator()(It it, It end) const -> result_type
    {
      while (it != end) {
        try {
          if (static_cast<bool>(*it)) {
            return true;
          }
        } catch (const boost::signals2::expired_slot&) {
        }
        ++it;
      }
      return false;
    }
  };

} // namespace y2::ui