// SPDX-License-Identifier: MIT

#pragma once

#include <y2/core/prelude.hpp>
#include <y2/core/type_traits.hpp>

#include <boost/operators.hpp>
#include <fmt/format.h>

namespace y2::ui::unit {

  // clang-format off

  ///
  /// \brief Typed unit class.
  ///
  template <class T, class Tag>
  class typed_unit
    : boost::additive<typed_unit<T, Tag> 
    , boost::multiplicative<typed_unit<T, Tag>
    , boost::multiplicative<typed_unit<T, Tag>, T
    , boost::partially_ordered<typed_unit<T, Tag>
    , boost::partially_ordered<typed_unit<T, Tag>, T
    , boost::incrementable<typed_unit<T, Tag>
    , boost::decrementable<typed_unit<T, Tag>
    >>>>>>>
  {
    T m_value = {};

  public:
    using value_type = T;

    explicit constexpr typed_unit(T value)
      : m_value {value}
    {
    }

    explicit constexpr typed_unit(s32 value)
    requires std::same_as<T, f64>
      : m_value {static_cast<T>(value)} 
    {
    }

    explicit constexpr typed_unit(u32 value)
    requires std::same_as<T, f64>
      : m_value {static_cast<T>(value)}
    {
    }

    typed_unit()                  = default;
    typed_unit(const typed_unit&) = default;
    typed_unit& operator=(const typed_unit&) = default;

    explicit constexpr operator const T&() const { return m_value; }
    explicit constexpr operator       T&()       { return m_value; }
    constexpr bool operator<(const typed_unit& x) const { return m_value < x.m_value; }
    constexpr bool operator==(const typed_unit& x) const { return m_value == x.m_value; }
    constexpr auto operator+=(const typed_unit& x) -> auto& { m_value += x.m_value; return *this; }
    constexpr auto operator-=(const typed_unit& x) -> auto& { m_value -= x.m_value; return *this; }
    constexpr auto operator*=(const typed_unit& x) -> auto& { m_value *= x.m_value; return *this; }
    constexpr auto operator/=(const typed_unit& x) -> auto& { m_value /= x.m_value; return *this; }
    constexpr bool operator<(const  T& x) const { return m_value < x; }
    constexpr bool operator>(const  T& x) const { return m_value > x; }
    constexpr bool operator==(const T& x) const { return m_value == x; }
    constexpr auto operator*=(const T& x) -> auto& { m_value *= x; return *this; }
    constexpr auto operator/=(const T& x) -> auto& { m_value /= x; return *this; }
    constexpr auto operator++() -> auto& { m_value++; return *this; }
    constexpr auto operator--() -> auto& { m_value--; return *this; }
  };

  ///
  /// \brief Point unit.
  ///
  /// Represents 1/72 logical inch per unit.
  ///
  using point = typed_unit<f64, class _point_tag>;
  constexpr auto operator"" _pt(long double x) { return point(static_cast<f64>(x)); }
  constexpr auto operator"" _pt(unsigned long long x) { return point(static_cast<f64>(x)); }

  ///
  /// \brief Pixel unit.
  ///
  /// Represnets native pixels.
  /// Content scale is applied, but buffer scale is not applied.
  ///
  using pixel = typed_unit<f64, class _pixel_tag>;
  constexpr auto operator"" _px(long double x) { return pixel(static_cast<f64>(x)); }
  constexpr auto operator"" _px(unsigned long long x) { return pixel(static_cast<f64>(x)); }

  ///
  /// \brief Logical pixel unit.
  ///
  /// Represents pixels independent of content scale.
  ///
  using logical_pixel = typed_unit<f64, class _logical_pixel_tag>;
  constexpr auto operator"" _lp(long double x) { return logical_pixel(static_cast<f64>(x)); }
  constexpr auto operator"" _lp(unsigned long long x) { return logical_pixel(static_cast<f64>(x)); }

  ///
  /// \brief Buffer pixel unit.
  ///
  /// Represents pixels in framebuffer.
  ///
  using buffer_pixel = typed_unit<f64, class _buffer_pixel_tag>;
  constexpr auto operator"" _bp(long double x) { return buffer_pixel(static_cast<f64>(x)); }
  constexpr auto operator"" _bp(unsigned long long x) { return buffer_pixel(static_cast<f64>(x)); }

  ///
  /// \brief Content scale.
  ///
  /// Represents scaling applied to content of window.
  ///
  using content_scale = typed_unit<f64, class _content_scale_tag>;

  ///
  /// \brief Buffer scale.
  ///
  /// Represents scaling applied to framebuffer.
  ///
  using buffer_scale = typed_unit<f64, class _buffer_scale_tag>;

  ///
  /// \brief Logical DPI.
  ///
  /// Represnets logical DPI.
  ///
  using logical_dpi = typed_unit<f64, class _logical_dpi_tag>;

  ///
  /// \brief Physical DPI.
  ///
  /// Represents physical DPI.
  ///
  using physical_dpi = typed_unit<f64, class _physical_dpi_tag>;

  // clang-format on

} // namespace y2::ui::unit

// Format specialization for typed_unit.
template <class T, class Tag>
struct fmt::formatter<y2::ui::unit::typed_unit<T, Tag>> : fmt::formatter<y2::f64>
{
  template <class FormatCtx>
  auto format(const y2::ui::unit::typed_unit<T, Tag>& a, FormatCtx& ctx)
  {
    return fmt::formatter<y2::f64>::format(static_cast<y2::f64>(a), ctx);
  }
};