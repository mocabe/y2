// SPDX-License-Identifier: MIT

#pragma once

#include <y2/ui/trackable_fwd.hpp>
#include <y2/ui/signal_fwd.hpp>
#include <y2/ui/connection.hpp>
#include <y2/ui/memory.hpp>
#include <y2/ui/dynamic_cast.hpp>

namespace y2::ui {

  ///
  /// \brief Trackable object.
  ///
  class trackable : public enable_dynamic_cast<trackable>,
                    public std::enable_shared_from_this<trackable>
  {
  public:
    virtual ~trackable() noexcept = default;
    virtual auto track() const -> weak<const trackable>;
    virtual auto track() -> weak<trackable>;

  protected:
    void disconnect_all() noexcept;

  private:
    template <class Signature, template <class> class Combiner>
    friend class signal;

    virtual void add_connection(connection c);

  private:
    std::vector<connection> m_connections;

    using std::enable_shared_from_this<trackable>::weak_from_this;
    using std::enable_shared_from_this<trackable>::shared_from_this;
  };

} // namespace y2::ui