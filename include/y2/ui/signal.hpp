// SPDX-License-Identifier: MIT

#pragma once

#include <y2/ui/signal_fwd.hpp>
#include <y2/ui/connection.hpp>
#include <y2/ui/memory.hpp>
#include <y2/ui/trackable.hpp>

#include <memory>
#include <mutex>
#include <concepts>

#include <boost/signals2.hpp>

namespace y2::ui {

  namespace detail {

    // clang-format off
    template <class T, class F, class R, class... Ts>
    concept signal_connectable_member = requires(T& t, F f, Ts&&... args)
    {
      std::derived_from<T, trackable>;
      { std::invoke(f, t, std::forward<Ts>(args)...) } -> std::same_as<R>;
    };

    template <class T, class F, class R, class... Ts>
    concept signal_connectable_free = requires(T& t, F f, Ts&&...args)
    {
      std::derived_from<T, trackable>;
      { std::invoke(f, std::forward<Ts>(args)...) } -> std::same_as<R>;
    };
    // clang-format on

  } // namespace detail

  ///
  /// \brief Signal class.
  ///
  /// This class wraps boost::signasl2 to provide signal-slot features for trackable objects.
  /// Signals are not guaranteed to be thread safe.
  ///
  template <template <class> class Combiner, class R, class... ArgTypes>
  class signal<R(ArgTypes...), Combiner>
  {
  public:
    using result_type        = R;
    using signature_type     = R(ArgTypes...);
    using slot_function_type = std::function<signature_type>;

  private:
    // clang-format off
    using combiner_type = boost::signals2::keywords::combiner_type<Combiner<result_type>>;
    using mutex_type    = boost::signals2::keywords::mutex_type<boost::signals2::dummy_mutex>;
    using signal_type   = boost::signals2::signal_type<signature_type, combiner_type, mutex_type>::type;
    // clang-format on

  private:
    signal_type m_signal;

    static void add_connection(trackable& t, connection&& c)
    {
      t.add_connection(std::move(c));
    }

    static auto make_connection(boost::signals2::connection c)
    {
      return connection(c);
    }

  public:
    signal()              = default;
    signal(const signal&) = delete;
    signal& operator=(const signal&) = delete;

    signal(signal&& other) noexcept
    {
      m_signal.swap(other.m_signal);
    }

    signal& operator=(signal&& other) noexcept
    {
      auto tmp = signal(std::move(other));
      swap(tmp);
      return *this;
    }

    void swap(signal& other) noexcept
    {
      m_signal.swap(other.m_signal);
    }

    void disconnect_all()
    {
      return m_signal.disconnect_all_slots();
    }

    bool empty() const
    {
      return m_signal.empty();
    }

    auto num_slots() const -> size_t
    {
      return m_signal.num_slots();
    }

    template <class... Args>
    auto operator()(Args&&... args)
    {
      return m_signal(std::forward<Args>(args)...);
    }

  public:
    ///
    /// Connect signal.
    ///
    /// \param sig Signal to connect.
    /// \param slot Slot function.
    ///
    friend void connect(signal<R(ArgTypes...), Combiner>& sig, slot_function_type slot)
    {
      sig.m_signal.connect(std::move(slot));
    }

    ///
    /// Connect signal by making scoped connection.
    ///
    /// \param sig Signal to connect.
    /// \param slot Slot function.
    /// \retval Scoped connection object.
    ///
    friend auto connect_scoped(signal<R(ArgTypes...), Combiner>& sig, slot_function_type slot)
      -> connection
    {
      return make_connection(sig.m_signal.connect(std::move(slot)));
    }

    ///
    /// Connect signal with lifetime tracking.
    ///
    /// \param sig Signal to connect.
    /// \param t Trackable object to bind connection.
    /// \param f Pointer to member slot function.
    ///
    template <class T, class F>
    requires detail::signal_connectable_member<T, F, R, ArgTypes...>
    friend void connect(signal<R(ArgTypes...), Combiner>& sig, T& t, F f)
    {
      add_connection(t, connect_scoped(sig, [f, &t](ArgTypes&&... args) {
                       return std::invoke(f, t, std::forward<ArgTypes>(args)...);
                     }));
    }

    ///
    /// Connect signal by making scoped connection.
    ///
    /// \param sig Signal to connect.
    /// \param t Trackable object to bind connection.
    /// \param f Pointer to member slot function.
    ///
    template <class T, class F>
    requires detail::signal_connectable_member<T, F, R, ArgTypes...>
    friend auto connect_scoped(signal<R(ArgTypes...), Combiner>& sig, T& t, F f) -> connection
    {
      return connect_scoped(sig, [f, &t](ArgTypes&&... args) {
        return std::invoke(f, t, std::forward<ArgTypes>(args)...);
      });
    }

    ///
    /// Connect signal with lifetime tracking.
    ///
    /// \param sig Signal to connect.
    /// \param t Trackable object to bind connection.
    /// \param f Slot function.
    ///
    template <class T, class F>
    requires detail::signal_connectable_free<T, F, R, ArgTypes...>
    friend void connect(signal<R(ArgTypes...), Combiner>& sig, T& t, F f)
    {
      add_connection(t, connect_scoped(sig, [f](ArgTypes&&... args) {
                       return std::invoke(f, std::forward<ArgTypes>(args)...);
                     }));
    }
  };

} // namespace y2::ui