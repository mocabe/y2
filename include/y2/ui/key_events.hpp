// SPDX-License-Identifier: MIT

#pragma once

#include <y2/ui/key.hpp>
#include <y2/ui/event.hpp>
#include <y2/ui/memory.hpp>

#include <string>

namespace y2::ui::events {

  /// Key event base.
  class key_event : public event
  {
  protected:
    key_event(event_phase phase, ui::key key);

  protected:
    ui::key m_key;
  };

  // propagate = yes
  class key_press final : public key_event
  {
    ui::key_action m_action;
    ui::key_mods m_mods;

  public:
    key_press(
      event_phase phase,
      ui::key key,
      ui::key_action action,
      ui::key_mods mods);

    /// get key
    auto key() const -> ui::key;
    /// get modifier flags
    auto modifiers() const -> ui::key_mods;
    /// get action
    auto action() const -> ui::key_action;
    /// auto repeat?
    bool is_repeat() const;

    // test modifiers
    bool test_modifiers(ui::key_mods mods) const;
    bool has_modifier() const;
    bool shift() const;
    bool control() const;
    bool alt() const;
    bool super() const;
    bool caps_lock() const;
    bool num_lock() const;
  };

  // propagete = yes
  class key_release final : public key_event
  {
  public:
    key_release(event_phase phase, ui::key key);

    /// get key
    auto key() const -> ui::key;
  };

  // propagate = yes
  class key_char final : public key_event
  {
    std::u8string m_str;

  public:
    key_char(event_phase phase, std::u8string str);

    /// get chars
    auto chars() const -> std::u8string_view;
  };

} // namespace y2::ui::events
