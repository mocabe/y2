// SPDX-License-Identifier: MIT

#pragma once

#include <y2/ui/os/event_queue_fwd.hpp>
#include <y2/ui/os/event_queue_events.hpp>
#include <y2/core/optional.hpp>
#include <y2/core/context.hpp>

#include <chrono>

namespace y2::ui {

  class os_event_queue_delegate
  {
  public:
    virtual ~os_event_queue_delegate() noexcept                        = default;
    virtual void process_event(os_event_queue_events::event_posted& e) = 0;
  };

  class os_timer_delegate
  {
  public:
    virtual ~os_timer_delegate() noexcept                               = default;
    virtual void process_event(os_event_queue_events::timer_timeout& e) = 0;
    virtual auto id() const noexcept -> s32                             = 0;
    virtual auto interval() const noexcept -> std::chrono::milliseconds = 0;
  };

  class os_event_queue
  {
    os_event_queue_delegate* m_queue_delegate = nullptr;
    std::unordered_map<u32, os_timer_delegate*> m_timers;

  protected:
    void send_posted_event();
    void send_timer_expiration(u32 id);

  public:
    virtual ~os_event_queue() noexcept = default;

    void set_delegate(os_event_queue_delegate& d);
    void remove_delegate();

    void add_timer_delegate(os_timer_delegate& d);
    void remove_timer_delegate(os_timer_delegate& d);

    static void thread_configure();
    static bool thread_has_context();
    static auto thread_local_context() -> y2::context<os_event_queue>;
    static auto next_timer_id() -> u32;

    virtual void run()  = 0; ///< Wait wait next event and process.
    virtual void wake() = 0; ///< Wake up event queue.
    virtual void stop() = 0; ///< Stop processing/waiting events immediately.
    virtual void post() = 0; ///< Post event to queue.

  protected:
    virtual void set_timer(u32 id, std::chrono::milliseconds interval) = 0;
    virtual void remove_timer(u32 id)                                  = 0;
  };

} // namespace y2::ui