// SPDX-License-Identifier: MIT

#include <y2/ui/os/application_events.hpp>
#include <y2/ui/memory.hpp>

#include <mutex>
#include <vector>

namespace y2::ui {

  ///
  /// \brief Posted event queue.
  ///
  class posted_event_queue final
  {
    using event_type = events::os_application_post_event;
    mutable std::mutex m_mtx;
    std::vector<unique<event_type>> m_queue;

    auto lock() const -> std::unique_lock<std::mutex>;

  public:
    posted_event_queue();

    bool empty() const;
    void push(event_receiver& r, unique<event> e);
    auto pop() -> unique<events::os_application_post_event>;
  };

} // namespace y2::ui