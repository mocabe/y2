// SPDX-License-Identifier: MIT

#pragma once

#include <y2/ui/os/screen_fwd.hpp>
#include <y2/ui/event.hpp>
#include <y2/ui/rect.hpp>
#include <y2/core/string.hpp>
#include <y2/core/optional.hpp>

namespace y2::ui::events {

  class os_screen_event : public ui::event
  {
  public:
    os_screen_event();
  };

  class os_screen_connect final : public os_screen_event
  {
    shared<os_screen> m_screen;

  public:
    os_screen_connect(shared<os_screen> m);

    auto screen() const -> shared<os_screen>;
  };

  class os_screen_disconnect final : public os_screen_event
  {
    shared<os_screen> m_screen;

  public:
    os_screen_disconnect(shared<os_screen> m);

    auto screen() const -> shared<os_screen>;
  };

  class os_primary_screen_change final : public os_screen_event
  {
    shared<os_screen> m_screen;

  public:
    os_primary_screen_change(shared<os_screen> m);

    auto screen() const -> shared<os_screen>;
  };

  /// Per-screen property update.
  class os_screen_update final : public os_screen_event
  {
    optional<ui::rect_px> m_geometry;
    optional<ui::rect_px> m_work_area;
    optional<unit::content_scale> m_content_scale;
    optional<unit::logical_dpi> m_logical_dpi;
    optional<unit::physical_dpi> m_physical_dpi;
    optional<string> m_name;

  public:
    os_screen_update();

    auto geometry() const -> optional<ui::rect_px>;
    auto work_area() const -> optional<ui::rect_px>;
    auto content_scale() const -> optional<unit::content_scale>;
    auto logical_dpi() const -> optional<unit::logical_dpi>;
    auto physical_dpi() const -> optional<unit::physical_dpi>;
    auto name() const -> optional<string>;

  public:
    void set_geometry(ui::rect_px geometry);
    void set_work_area(ui::rect_px work_area);
    void set_content_scale(unit::content_scale scale);
    void set_logical_dpi(unit::logical_dpi dpi);
    void set_physical_dpi(unit::physical_dpi dpi);
    void set_name(string name);
  };

} // namespace y2::ui::events