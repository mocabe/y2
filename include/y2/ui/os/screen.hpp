// SPDX-License-Identifier: MIT

#pragma once

#include <y2/ui/os/screen_fwd.hpp>
#include <y2/ui/os/screen_events.hpp>
#include <y2/ui/rect.hpp>
#include <y2/ui/memory.hpp>
#include <y2/ui/unit.hpp>
#include <y2/core/string.hpp>

#include <vector>
#include <string>

namespace y2::ui {

  class os_screen_delegate
  {
  public:
    virtual ~os_screen_delegate() noexcept                      = default;
    virtual void process_event(events::os_screen_update& e)     = 0;
    virtual void process_event(events::os_screen_disconnect& e) = 0;
  };

  class os_screen
  {
  protected:
    std::vector<os_screen_delegate*> m_delegates;

  public:
    virtual ~os_screen() noexcept = default;

    void add_delegate(os_screen_delegate& d);
    void remove_delegate(os_screen_delegate& d);

    virtual bool valid() const                                = 0;
    virtual auto geometry() const -> ui::rect_px              = 0;
    virtual auto work_area() const -> ui::rect_px             = 0;
    virtual auto content_scale() const -> unit::content_scale = 0;
    virtual auto logical_dpi() const -> unit::logical_dpi     = 0;
    virtual auto physical_dpi() const -> unit::physical_dpi   = 0;
    virtual auto name() const -> string                       = 0;
  };

} // namespace y2::ui