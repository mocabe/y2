// SPDX-License-Identifier: MIT

#pragma once

#include <y2/ui/os/screen_info_fwd.hpp>
#include <y2/ui/os/screen_events.hpp>
#include <y2/ui/rect.hpp>
#include <y2/ui/memory.hpp>
#include <y2/core/context.hpp>

namespace y2::ui {

  class os_screen_info_delegate
  {
  public:
    virtual ~os_screen_info_delegate() noexcept                     = default;
    virtual void process_event(events::os_screen_connect& e)        = 0;
    virtual void process_event(events::os_screen_disconnect& e)     = 0;
    virtual void process_event(events::os_primary_screen_change& e) = 0;
  };

  class os_screen_info
  {
  public:
    virtual ~os_screen_info() noexcept = default;

    static auto context() -> y2::context<os_screen_info>;

  public:
    void set_delegate(os_screen_info_delegate& d);
    void remove_delegate();

  public:
    virtual auto primary() -> shared<os_screen>              = 0;
    virtual auto screens() -> std::vector<shared<os_screen>> = 0;
    virtual auto default_dpi() -> unit::logical_dpi          = 0;

  protected:
    bool has_delegate() const;

    void send_event(auto& e)
    {
      if (m_delegate) {
        m_delegate->process_event(e);
      }
    }

  private:
    os_screen_info_delegate* m_delegate = nullptr;
  };

} // namespace y2::ui