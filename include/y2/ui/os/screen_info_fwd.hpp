// SPDX-License-Identifier: MIT

#pragma once

namespace y2::ui {

  class os_screen_info_delegate;
  class os_screen_info;

} // namespace y2::ui