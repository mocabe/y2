// SPDX-License-Identifier: MIT

#pragma once

#include <y2/ui/event.hpp>

namespace y2::ui::events {

  class os_event_queue_event : public event
  {
  public:
    os_event_queue_event();
  };
} // namespace y2::ui::events

namespace y2::ui::os_event_queue_events {

  class event_posted final : public events::os_event_queue_event
  {
  public:
    event_posted();
  };

  class timer_timeout final : public events::os_event_queue_event
  {
    u32 m_id = 0;

  public:
    timer_timeout(u32 id);

    auto id() const -> u32;
  };
} // namespace y2::ui::os_event_queue_events