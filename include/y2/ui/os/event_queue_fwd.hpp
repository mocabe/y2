// SPDX-License-Identifier: MIT

#pragma once

namespace y2::ui {

  class os_event_queue_delegate;
  class os_timer_delegate;
  class os_event_queue;

}