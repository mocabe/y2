// SPDX-License-Identifier: MIT

#pragma once

namespace y2::ui {

  class os_application_delegate;
  class os_application;

}