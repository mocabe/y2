// SPDX-License-Identifier: MIT

#pragma once

#include <y2/ui/event_receiver_fwd.hpp>
#include <y2/ui/event.hpp>
#include <y2/ui/memory.hpp>
#include <y2/ui/exit_code.hpp>
#include <y2/core/string.hpp>
#include <y2/core/uuid.hpp>

#include <vector>

namespace y2::ui::events {

  class os_application_event : public event
  {
  public:
    os_application_event();
  };

  class os_application_start : public os_application_event
  {
    string m_id;
    std::vector<string> m_args;

  public:
    os_application_start(string id, std::vector<string> args);

    auto id() const -> string;
    auto args() const -> std::vector<string>;
  };

  class os_application_exit : public os_application_event
  {
    exit_code m_code;

  public:
    os_application_exit(exit_code code);

    auto code() const -> exit_code;
  };

  class os_application_unhandled_exception : public os_application_event
  {
    std::exception_ptr m_exception = nullptr;

  public:
    os_application_unhandled_exception(std::exception_ptr e);
    auto exception() const -> std::exception_ptr;
  };

  class os_application_post_event : public os_application_event
  {
    weak<ui::event_receiver> m_receiver;
    unique<ui::event> m_event;

  public:
    os_application_post_event(ui::event_receiver& r, unique<ui::event> e);
    auto receiver() const -> weak<ui::event_receiver>;
    auto event() const -> ui::event&;
  };

} // namespace y2::ui::events
