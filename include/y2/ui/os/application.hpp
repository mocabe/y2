// SPDX-License-Identifier: MIT

#pragma once

#include <y2/ui/os/application_fwd.hpp>
#include <y2/ui/os/screen_info_fwd.hpp>
#include <y2/ui/os/application_events.hpp>
#include <y2/ui/os/screen_events.hpp>
#include <y2/ui/os/event_queue.hpp>
#include <y2/ui/os/posted_event_queue.hpp>
#include <y2/ui/memory.hpp>
#include <y2/ui/application_events.hpp>
#include <y2/ui/screen_events.hpp>
#include <y2/ui/exit_code.hpp>
#include <y2/core/uuid.hpp>
#include <y2/core/optional.hpp>
#include <y2/core/context.hpp>

#include <span>
#include <queue>

namespace y2::ui {

  class os_application_delegate
  {
  public:
    virtual ~os_application_delegate() noexcept                               = default;
    virtual void process_event(events::os_application_start& e)               = 0;
    virtual void process_event(events::os_application_exit& e)                = 0;
    virtual void process_event(events::os_application_unhandled_exception& e) = 0;
    virtual void process_event(events::os_screen_connect& e)                  = 0;
    virtual void process_event(events::os_screen_disconnect& e)               = 0;
    virtual void process_event(events::os_primary_screen_change& e)           = 0;
  };

  class os_application : protected os_event_queue_delegate
  {
  public:
    static void configure(string id, std::vector<string> args);
    static auto context() -> context<os_application>;

  public:
    os_application(string id, std::vector<string> args);
    virtual ~os_application() noexcept;

    void set_delegate(os_application_delegate& d);
    void remove_delegate();

    bool send_event(event_receiver& r, ui::event& e);
    void post_event(event_receiver& r, unique<ui::event> e);

    virtual auto run() -> exit_code   = 0;
    virtual void exit(exit_code code) = 0;

    auto id() const -> string;
    auto args() const -> std::vector<string>;

  protected:
    void process_event(os_event_queue_events::event_posted& e) override;
    void process_all_posted_events();

    void send_event(auto& e)
    {
      if (m_delegate) {
        m_delegate->process_event(e);
      }
    }

  private:
    struct params;

  private:
    /// Delegate.
    os_application_delegate* m_delegate = nullptr;
    /// Init params
    shared<params> m_params;
    /// Event queue context of main thread.
    y2::context<os_event_queue> m_event_queue;
    /// List of posted events.
    posted_event_queue m_post_events;
  };

} // namespace y2::ui