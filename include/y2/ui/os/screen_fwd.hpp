// SPDX-License-Identifier: MIT

#pragma once

namespace y2::ui {

  class os_screen_delegate;
  class os_screen;

}