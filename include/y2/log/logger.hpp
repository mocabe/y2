// SPDX-License-Identifier: MIT

#pragma once

#include <y2/core/prelude.hpp>
#include <y2/core/badge.hpp>

#include <string_view>
#include <memory>
#include <spdlog/fwd.h>
#include <fmt/format.h>

namespace y2::log {

  /// Log level
  enum class level
  {
    info,    //< Information
    warning, //< Warnings
    error,   //< Errors
  };

  /// \brief Logger interface
  class logger
  {
    std::shared_ptr<spdlog::logger> m_logger;
    level m_level;

  public:
    // internal
    logger(badge<logger>);

  public:
    /// Make new logger, or get existing logger from name.
    [[nodiscard]] static auto get_for_stderr(std::string_view sv)
      -> std::shared_ptr<logger>;

  public:
    /// Output log to default sink
    void log(level lvl, std::string_view msg);

    /// Set log output level
    void set_level(level lvl);

    /// Disable logger
    void disable();

    /// Enable logger
    void enable();
  };

  /// Log formatted message
  template <class... Args>
  void log_formatted(
    std::shared_ptr<logger> logger,
    level lvl,
    const char* msg,
    Args&&... args)
  {
    if (logger) {
      logger->log(lvl, fmt::format(msg, std::forward<Args>(args)...));
    }
  }

  /// info
  template <class... Args>
  void log_info(std::shared_ptr<logger> logger, const char* msg, Args&&... args)
  {
    log_formatted(logger, level::info, msg, std::forward<Args>(args)...);
  }

  /// warning
  template <class... Args>
  void log_warning(
    std::shared_ptr<logger> logger,
    const char* msg,
    Args&&... args)
  {
    log_formatted(logger, level::warning, msg, std::forward<Args>(args)...);
  }

  /// error
  template <class... Args>
  void log_error(
    std::shared_ptr<logger> logger,
    const char* msg,
    Args&&... args)
  {
    log_formatted(logger, level::error, msg, std::forward<Args>(args)...);
  }

} // namespace y2::log

/// Macro to create local logger definition
#define Y2_DECL_LOCAL_LOGGER(NAME)                                                            \
  namespace {                                                                                 \
    [[nodiscard]] auto local_logger()                                                         \
    {                                                                                         \
      static auto logger = ::y2::log::logger::get_for_stderr(#NAME);                          \
      return logger;                                                                          \
    }                                                                                         \
                                                                                              \
    template <class... Args>                                                                  \
    void log_info(const char* msg, Args&&... args)                                            \
    {                                                                                         \
      return ::y2::log::log_info(local_logger(), msg, std::forward<Args>(args)...);           \
    }                                                                                         \
                                                                                              \
    template <class... Args>                                                                  \
    void debug_log_info([[maybe_unused]] const char* msg, [[maybe_unused]] Args&&... args)    \
    {                                                                                         \
      if constexpr (::y2::is_debug)                                                           \
        log_info(msg, std::forward<Args>(args)...);                                           \
    }                                                                                         \
                                                                                              \
    template <class... Args>                                                                  \
    void log_warning(const char* msg, Args&&... args)                                         \
                                                                                              \
    {                                                                                         \
      return ::y2::log::log_warning(local_logger(), msg, std::forward<Args>(args)...);        \
    }                                                                                         \
                                                                                              \
    template <class... Args>                                                                  \
    void debug_log_warning([[maybe_unused]] const char* msg, [[maybe_unused]] Args&&... args) \
    {                                                                                         \
      if constexpr (::y2::is_debug)                                                           \
        log_warning(msg, std::forward<Args>(args)...);                                        \
    }                                                                                         \
                                                                                              \
    template <class... Args>                                                                  \
    void log_error(const char* msg, Args&&... args)                                           \
    {                                                                                         \
      ::y2::log::log_error(local_logger(), msg, std::forward<Args>(args)...);                 \
    }                                                                                         \
                                                                                              \
    template <class... Args>                                                                  \
    void debug_log_error([[maybe_unused]] const char* msg, [[maybe_unused]] Args&&... args)   \
    {                                                                                         \
      if constexpr (::y2::is_debug)                                                           \
        log_error(msg, std::forward<Args>(args)...);                                          \
    }                                                                                         \
  }
